<?php
include "doc_check.class.php";

//*****************************************************************************
class page extends doc_check {

    //**************************************************************************
    function __construct() {
        
        $this->page_title = "Daily document submission check - manual";
        parent::__construct();
    }
    
    //***************************************************************************
    function createForm() {

        $this->form = $this->getForm();

        $this->form->addText("dest_db", "Destination database", true)->value = $this->user->env;

        $this->form->addUpload("report_file", "Report file (XLSX or CSV)", false, true);

        $this->form_submitButtons($this->form, false, false, "Check");
        $this->form->getInputValues();
    }

    //***************************************************************************
    function check() {

        $this->checkMandatory($this->form);

        $ctrl = $this->form->inputControls["report_file"];
        if ($ctrl->getUploadError()) {
            $this->showMessage("Errore caricamento file", "Si è verificato l'errore " .
                    $ctrl->getUploadError() .
                    " durante il caricamento del documento $ctrl->name." .
                    " Si prega di avvertire l'assistenza tecnica.");
        }

        
        $reportName = $ctrl->inputValue;
        $extension = strtolower(pathinfo($reportName, PATHINFO_EXTENSION));
        if ($extension == "xlsx") {
            $rows = $this->getXLSXRows($ctrl->getTmpValue());
        }
        elseif ($extension == "csv") {
            $rows = $this->getCSVRows($ctrl->getTmpValue());
        }
        else {
            $this->showMessage("Errore formato file", "Il file $reportName non è in formato CSV o XLSX");
        }
        
        $rowNr = 0;
        $barcodeColNr = 0;
        $messages = [];
        $chunkLimit = 100;
        $chunk = [];
        foreach ($rows as $row) {
            $rowNr++;
            if ($rowNr == 1) {
                $barcodeColNr = $this->getBarcodeColNr($row);
                continue;
            }
            
            if (count($chunk) == $chunkLimit) {
                $messages = array_merge($messages, $this->checkChunk($barcodeColNr, $chunk));
                $chunk = [];
            }

            $row["my_row_nr"] = $rowNr;
            $chunk[] = $row;
        }
        if (count($chunk)) {
            $messages = array_merge($messages, $this->checkChunk($barcodeColNr, $chunk));
            $chunk = [];
        }
            
        if ($messages) {
            $this->showMessage("$reportName - Rilevati errori", "Si sono verificati i seguenti errori durante la lettura del file:<p>" .
                    implode("<br>", $messages), false, true);
        }
        else {
            $this->showMessage("$reportName - Nessun errore rilevato", "Nessun errore rilevato; processate " . ($rowNr - 1) . " righe", false, true);
        }
        
    }

//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
