<?php

	if (count($argv) != 4) {
		exit("usage: " . basename($argv[0]) . " [server] [utc_start_time] [utc_stop_time]\n\n" .
				"example: " . basename($argv[0]) . " apugbovnettuno1 2022-07-15T13:55:00.000 2022-07-15T16:00:00.000");
	}
	$server = $argv[1];
	$utc_start_time = $argv[2];
	$utc_stop_time = $argv[3];
	
	echo "parametri input = $server - $utc_start_time - $utc_stop_time\n\n";
	
	$uuid_list = [];
	$index = 0;

	$page = "http://$server.servizi.gr-u.it:25086/call/fax/infail";
		echo "leggo pagina $page\n";
	$page_content = url_get_contents($page);
	list($before, $interesting) = explode("<c:selection>", $page_content, 2);
	list($start_point, $after) = explode("</c:selection>", $interesting, 2);
	
	$loop = true;
	while($loop) {
		$objects = explode("<c:object>", $page_content);
		foreach ($objects as $object) {
			if (!strpos($object, "<c:uuid>")) {
				// non è un object
				continue;
			}
			list($before, $interesting) = explode("<TimeStart>", $object, 2);
			list($time_start, $after) = explode("</TimeStart>", $interesting, 2);
			if ($time_start > $utc_stop_time) {
				// non siamo ancora risaliti alla zona temporale da risottomettere
				continue;
			}
			elseif ($time_start < $utc_start_time) {
				// siamo risaliti fino alla fine della zona temporale da risottomettere
				$loop = false;
				break;
			}
			// siamo nella zona temporale da risottomettere
			list($before, $interesting) = explode("<Response>", $object, 2);
			list($response, $after) = explode("</Response>", $interesting, 2);
			if ($response === "The message could not be imported into Kofax Capture due to an unknown error.") {
				// elemento da risottomettere
				list($before, $interesting) = explode("<c:uuid>", $object, 2);
				list($uuid, $after) = explode("</c:uuid>", $interesting, 2);
				$uuid_list[] = $uuid;
				echo "risottometterò $uuid - $time_start\n";
			}
		}
			
		if (!$loop) {
			break;
		}
		
		$index += 50;
		$page = "http://$server.servizi.gr-u.it:25086/call/fax/infail?selection=$start_point&index=$index";
		echo "leggo pagina $page\n";
		$page_content = url_get_contents($page);
	}
	
	foreach ($uuid_list as $uuid) {
		$page = "http://$server.servizi.gr-u.it:25086/call/fax/ReRun?Id=$uuid";
		echo "risottometto $page \n";
		 $page_content = url_get_contents($page);
	}
	
	echo "\n\n-----------------------\nfine\n\n";


	//***************************************************************************

	/**
	 * 
	 * @return string
	 */
	function url_get_contents($url) {

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		$buffer = curl_exec($curl);
		curl_close($curl);

		return $buffer;
	}

