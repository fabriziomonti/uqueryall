<?php

error_reporting(E_ERROR | E_WARNING | E_COMPILE_ERROR | E_CORE_ERROR | E_PARSE);
//****************************************************************************************
if (PHP_MAJOR_VERSION >= 7) {
    set_error_handler(function ($errno, $errstr) {
        if (strpos($errstr, 'Constant APPL_PHP_CLI_EXE') === 0)
            return true;
        if (strpos($errstr, 'Attempt to read property') === 0)
            return true;
        if (strpos($errstr, 'Undefined array key') === 0)
            return true;
        if (strpos($errstr, 'Trying to access array offset') === 0)
            return true;
        if (strpos($errstr, 'Undefined variable') === 0)
            return true;
        if (strpos($errstr, 'Undefined property') === 0)
            return true;
        return strpos($errstr, 'Declaration of') === 0;
    }, E_WARNING);
}

//****************************************************************************************
include_once (__DIR__ . "/defines.inc.php");
include_once (__DIR__ . "/config.inc.php");
include_once (__DIR__ . "/walibs4/waapplication/waapplication.class.php");

//****************************************************************************************
class uqueryall extends waLibs\waApplication {

    var $directoryDoc;
    var $webDirectoryDoc;
    var $sessionData;
    // file di config di questa applicazione 
    var $fileConfigDB;
    // dati di editing da appiccicare a tutte le modifiche sulle tabelle del db
    var $formModId = "modified";
    var $formModIp = "ip_edit";

    /**
     * contiene i dati dell'operatore loggato alla sessione
     */
    var $user = array();
    var $userPreferences;
    var $startPage = 'home.php';
    var $loginPage = 'frm_login.php';
    // indica se stiamo facendo vedere qualcosa in una finestra figlia
    var $childWindow = false;
    // protocollo utlizzato (http/s)
    var $protocol = "https";

    public static $applicationInstance;

    //****************************************************************************************

    /**
     * costruttore
     *
     */
    function __construct($checkUser = true) {
        $this->useSession = true;
        $this->domain = APPL_DOMAIN;
        $this->httpwd = APPL_DIRECTORY;
        $this->directoryTmp = APPL_TMP_DIRECTORY;
        $this->name = APPL_NAME;
        $this->title = APPL_TITLE;
        $this->version = APPL_REL;
        $this->versionDate = APPL_REL_DATE;

        $this->directoryDoc = APPL_DOC_DIRECTORY;
//		$this->webDirectoryDoc = APPL_WEB_DOC_DIRECTORY;
        $this->protocol = $this->getProtocol();

        self::$applicationInstance = $this;

        $this->init();

        $this->sessionData = &$_SESSION[$this->name];
        $this->user = &$this->sessionData['user'];

        // impostazioni delle preferenze
        $this->userPreferences = unserialize(base64_decode($_COOKIE[urlencode($this->name . "_" . base64_encode($this->user->username) . "_prefs")]));
        $this->userPreferences["navigation_by_windows"] = isset($this->userPreferences["navigation_by_windows"]) ? $this->userPreferences["navigation_by_windows"] : false;
        $this->userPreferences["max_table_rows"] = $this->userPreferences["max_table_rows"] ? $this->userPreferences["max_table_rows"] : waLibs\waTable::LIST_MAX_REC;
        $this->userPreferences["table_actions"] = isset($this->userPreferences["table_actions"]) ? $this->userPreferences["table_actions"] : "watable_actions_context";
        $this->navigationMode = $this->userPreferences["navigation_by_windows"] ? waLibs\waApplication::NAV_WINDOW : waLibs\waApplication::NAV_INNER;

        // verifica validità dell'utent loggato
        $this->checkUser($checkUser);
    }

    //*****************************************************************************

    /**
     * @return \waLibs\waDBConnection
     */
    function getDBConnection($fileConfigDB = '') {
        return parent::getDBConnection($fileConfigDB ? $fileConfigDB : $this->fileConfigDB);
    }

    //*****************************************************************************
    function record2Array(waLibs\waRecord $record) {
        $retval = array();
        $rs = $record->recordset;
        for ($i = 0; $i < $rs->fieldNr(); $i++) {
//			if ($record->$i && $rs->fieldType($i) == \waLibs\waDB::DATE)
//				{
//				$retval[$rs->fieldName($i)] = date("Y-m-d", $record->$i);
//				}
//			elseif ($record->$i && $rs->fieldType($i) == \waLibs\waDB::DATETIME)
//				{
//				$retval[$rs->fieldName($i)] = date("Y-m-d H:i:s", $record->$i);
//				}
//			else
//				{
            $retval[$rs->fieldName($i)] = $record->$i;
//				}
        }

        return $retval;
    }

    //**************************************************************************
    // verifica che un form soddisfi i requisiti di obbligatorietà, 
    // altrimenti interrompe l'operazione di salvataggio con un messaggio
    function checkMandatory(waLibs\waForm $form) {
        if (!$form->checkMandatory()) {
            $this->showMandatoryError();
        }
    }

    //***************************************************************************

    /**
     * 
     * @return string
     */
    function url_get_contents($url) {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $buffer = curl_exec($curl);
        curl_close($curl);

        return $buffer;
    }

    //***************************************************************************

    /**
     * restituisce il protocllo di connessione (http/s)
     */
    function getProtocol() {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";

        return $protocol;
    }

    //***************************************************************************

    /**
     * @return void
     */
    function getMenu() {
        if ($this->childWindow) {
            return false;
        }

        $m = new waLibs\waMenu();
        $m->open();

        if ($this->user->env) {
            $m->openSection("Tables");
            if ($this->sessionData["tables"]) {
                $tables_nr = count($this->sessionData["tables"]);
                $max_in_section = 100;
                $sections_nr = intval($tables_nr / $max_in_section);
                $sections_nr += $tables_nr % $max_in_section == 0 ? 0 : 1;
                for ($sections_cntr = 0; $sections_cntr < $sections_nr; $sections_cntr++) {
                    $section_first_index = $sections_cntr * $max_in_section;
                    $section_first_table = $this->sessionData["tables"][$section_first_index]["table_name"];
                    $section_last_index = $sections_cntr * $max_in_section + $max_in_section - 1;
                    $section_last_index = $section_last_index > $tables_nr - 1 ? $tables_nr - 1 : $section_last_index;
                    $section_last_table = $this->sessionData["tables"][$section_last_index]["table_name"];
                    $m->openSection("$section_first_table - $section_last_table");
                    for ($index = $section_first_index; $index <= $section_last_index; $index++) {
                        $table_data = $this->sessionData["tables"][$index];
                        $m->addItem("$table_data[table_name] ($table_data[num_rows])", "tbl_crud.php?tbl_name=$table_data[table_name]");
                    }
                    $m->closeSection();
                }
            }

            $m->closeSection();

            $m->openSection("Views");
            $m->addItem("New", "javascript:document.waPage.openPage(\"frm_view.php\")");
            $m->addItem("");

            if ($this->sessionData["views"]) {
                foreach ($this->sessionData["views"] as $view_id => $view_title) {
                    $m->addItem($view_title, "tbl_view.php?view_id=$view_id");
                }
            }
            $m->closeSection();
        }

        if ($this->user->db_accounts) {
            $m->openSection("Databases");
            $redir = base64_encode($_SERVER["REQUEST_URI"]);
            ksort($this->user->db_accounts, SORT_STRING);
            foreach ($this->user->db_accounts as $env => $db_account) {
                $m->addItem(strtoupper($env), "change_environment.php?env=$env&redir=$redir");
            }
            $m->addItem("None", "change_environment.php");
            $m->closeSection();
        }

        if ($this->user->env && count($this->user->db_accounts) > 1 && $this->sessionData["tablenames"]["CC_CLAIM"]) {
            $m->openSection("LiquiDump");
                $m->addItem("Claim import", "javascript:document.waPage.openPage(\"frm_claim_import.php\")");
                $m->addItem("Base import", "javascript:document.waPage.openPage(\"frm_base_import.php\")");
                $m->addItem("Reset", "javascript:document.waPage.openPage(\"frm_db_reset.php\")");
            $m->closeSection();
            $m->openSection("Doc check");
                $m->openSection("Daily submission");
                    $m->addItem("Mailbox", "javascript:document.waPage.openPage(\"frm_doc_check_mailbox.php\")");
                    $m->addItem("Manual", "javascript:document.waPage.openPage(\"frm_doc_check.php\")");
                $m->closeSection();
                $m->openSection("EcmLoader results");
                    $m->addItem("PecNew", "javascript:document.waPage.openPage(\"frm_doc_check_pecnew.php\")");
                    $m->addItem("FDMPosta", "javascript:document.waPage.openPage(\"frm_doc_check_fdm_posta.php\")");
                    $m->addItem("FDMAgp", "javascript:document.waPage.openPage(\"frm_doc_check_fdm_agp.php\")");
                    $m->addItem("StornoAssegni", "javascript:document.waPage.openPage(\"frm_doc_check_storno_assegni.php\")");
                    $m->addItem("P2000", "javascript:document.waPage.openPage(\"frm_doc_check_p2000.php\")");
                    $m->addItem("All of above", "javascript:document.waPage.openPage(\"frm_doc_check_ecm_all.php\")");
                $m->closeSection();
                $m->addItem("Kofax FAX", "javascript:document.waPage.openPage(\"frm_doc_check_fax.php\")");
                $m->addItem("FileNet query", "javascript:document.waPage.openPage(\"frm_filenet_query.php\")");
            $m->closeSection();
        }        
        
        $m->openSection("Tools");
        $m->addItem("DB accounts", "tbl_db_account.php");
        if ($this->user->env) {
            $m->addItem("Flush " . strtoupper($this->user->env) . " tables cache", "javascript:document.waPage.openPage(\"flush_tables_cache.php\")");
        }
        $m->addItem("Preferences", "javascript:document.waPage.openPage(\"frm_config.php\")");
        $m->addItem("Account", "javascript:document.waPage.openPage(\"frm_account.php\")");
        $m->addItem("Update code", "javascript:document.waPage.openPage(\"update_code.php\")");
        $m->addItem("Logout", "logout.php");
        $m->closeSection();

        $m->close();

        include_once __DIR__ . "/ui/view/wamenu/wamenu.php";
        $m->view = new \uqueryall\wamenu_view();

        return $m;
    }

    //*****************************************************************************
    // verifica l'user loggato
    //*****************************************************************************

    /**
     * 
     * @param boolean $checkUser se l'user deve essere verificato
     */
    function checkUser($checkUser) {
        if (!$checkUser) {
            return;
        }
        if (!$this->user) {
            $this->redirect($this->loginPage);
        }

        $this->initTablseEnv();
    }

    //*****************************************************************************
    // crea il blocco dei bottoni standard di una form
    //*****************************************************************************
    function form_submitButtons(waLibs\waForm $form,
            $readOnly = false,
            $showDeleteButton = true,
            $cmdOkCaption = 'Save',
            $cmdCancelCaption = 'Cancel',
            $cmdDeleteCaption = 'Delete',
            $cmdCloseCaption = 'Close') {
        if (!$readOnly) {
            $okCtrl = new waLibs\waButton($form, 'cmd_submit', $cmdOkCaption);
        }
        if ($readOnly) {
            $cancelCtrl = new waLibs\waButton($form, 'cmd_cancel', $cmdCloseCaption);
        } else {
            $cancelCtrl = new waLibs\waButton($form, 'cmd_cancel', $cmdCancelCaption);
        }
        $cancelCtrl->cancel = true;
        $cancelCtrl->submit = false;

        if ($showDeleteButton && !$readOnly) {
            $ctrl = new waLibs\waButton($form, 'cmd_delete', $cmdDeleteCaption);
            $ctrl->delete = true;
        }

//        $ctrl = new waLibs\waButton($form, 'cmd_help', 'Help');
//        $ctrl->submit = false;
    }

    //***************************************************************************

    /**
     * -
     */
    function getPageTitle() {
        return $this->items["title"] ? $this->items["title"]->value : "";
    }

    //***************************************************************************

    /**
     * -
     * @return waLibs\waTable
     */
    function getTable($sqlOrArray, $fileConfigDB = null) {
        $fileConfigDB = $fileConfigDB ? $fileConfigDB : $this->fileConfigDB;
        $table = new waLibs\waTable($sqlOrArray, $fileConfigDB);
        $viewType = $this->userPreferences["table_actions"];

        include_once __DIR__ . "/ui/view/watable/$viewType.php";
        $class = "\\uqueryall\\$viewType" . "_view";
        $table->view = new $class();

        // determinazione del nr di righe massimo della table
        if (!is_array($sqlOrArray)) {
            if ($_GET['watable_no_pagination']) {
                // anche se chiedono di vedere tutto in  una sola pagina, limitiamo 
                // comunque la vista a 1000 righe, altrimenti rischiano di 
                // impallare il server
                $table->listMaxRec = 1000;
                $table->addAction("pagination", false, "Pagination");
            } else {
                $table->listMaxRec = $this->userPreferences["max_table_rows"] ?
                        $this->userPreferences["max_table_rows"] :
                        waLibs\waTable::LIST_MAX_REC;
                $table->addAction("no_pagination", false, "All");
            }
        }

        $table->title = $this->getPageTitle();
        if ($fileConfigDB != $this->fileConfigDB) {
            $table->removeAction("New");
            $table->removeAction("Edit");
            $table->removeAction("Delete");
        } else {
            $table->removeAction("Details");
        }

        // una specie di separatore
//		$table->addAction("____", true);

        if ($this->childWindow) {
            $table->addAction("Close", false);
            // portiamo il bottone "chiudi in prima posizione
            $this->moveTableActionToTop($table, "Close");
        }

        $table->pdfOrientation = "L";

        return $table;
    }

    //***************************************************************************

    /**
     * sposta un'azione prima delle altre
     * 
     */
    function moveTableActionToTop(waLibs\waTable $table, $actionName) {
        $swappo[$actionName] = $table->actions[$actionName];
        foreach ($table->actions as $k => $v) {
            if ($k != $actionName) {
                $swappo[$k] = $v;
            }
        }
        $table->actions = $swappo;
    }

    //***************************************************************************

    /**
     * -
     * @return waLibs\waForm
     */
    function getForm($destinationPage = null) {
        $form = new waLibs\waForm($destinationPage, $this);
        $form->loadExtensions(WAMODULO_EXTENSIONS_DIR);
        include_once __DIR__ . "/ui/view/waform/waform.php";
        $form->view = new \uqueryall\waFormView();
        $form->fieldNameModId = $this->formModId;

        return $form;
    }

    //***************************************************************************

    /**
     * manda in output la pagina
     * 
     */
    function show($return = false) {
        $version_data = (object) array("nr" => $this->version, "date" => date("Y-m-d", $this->versionDate));
        $this->addItem($version_data, "version_data");
        if ($this->user->env) {
            $this->addItem($this->user->env, "env");
        }

        if (!$this->view) {
            include_once __DIR__ . "/ui/view/waapplication.php";
            $this->view = new \uqueryall\waapplication_view();
        }

        waLibs\waApplication::show();

        if ($return) {
            return;
        }

        exit();
    }

    //*****************************************************************************
    function record2Object(waLibs\waRecord $record) {
        return (object) $this->record2Array($record);
    }

    //**************************************************************************
    // l'eliminazione standard di un record è solo logica, non fisica
    // non utilizzato
    function deleteRecord(waLibs\waRecord $record, $end_script = true) {
        if (!$record) {
            return;
        }

        $record->retired = $record->id;
        $this->saveRecordset($record->recordset);
        if ($end_script) {
            $this->response();
        }
    }

    //***************************************************************************

    /**
     * consolida su db le modifiche avvenute su un recordset waRecordset
     * 
     * in caso di errore, termina l'esecuzione dello script
     * invocando il metodo {@link showDBError}
     * 
     * @param waRecordset $recordset recordset da consolidare
     */
    function saveRecordset(waLibs\waRecordset $recordset) {
        foreach ($recordset->records as $record) {
            $this->setEditorData($record);
        }

        // salvataggio vero e proprio
        parent::saveRecordset($recordset);
    }

    //*****************************************************************************
    function setEditorData(waLibs\waRecord $record) {

        if ($record->isNew() || $record->isModified()) {
            $record->modified = time();
            $record->id_account_edit = $this->user->id;
            $record->ip_edit = $_SERVER['REMOTE_ADDR'];
        }
        if ($record->isNew() && !$record->registered_at) {
            $record->created = $record->edited_at;
        }
    }

    //*****************************************************************************
    function checkLockViolation(waLibs\waForm $form) {
        if ($form->record->value($form->fieldNameModId) != $form->getModId()) {
            $this->showMessage("Lock violation error", "Lock violation error");
        }
    }

    //***************************************************************************

    /**
     * -
     */
    function getTitleContext($table, $description_field_name = "name", $key_name = "", $dbconfig = null) {
        /**
         * @todo sistemare
         */
        $key_name = $key_name ? $key_name : "id_$table";
        $description_field_name = $description_field_name ? $description_field_name : "name";
        $key = $_GET[$key_name];
        if (!$key) {
            return "";
        }
        $dbconn = $dbconfig ? $this->getDBConnection($dbconfig) : $this->getMasterDBConnection();
        $sql = "select $description_field_name from $table where id=" . $dbconn->sqlString($key);
        $record = $this->getRecordset($sql, $dbconn, 1)->records[0];
        if (!$record) {
            return "";
        }

        return "\n" . htmlspecialchars($record->value(0));
    }

    //***************************************************************************

    /**
     * -
     */
    function setTableColumns(waLibs\waTable $table, waLibs\waDBConnection $dbconn, $sql, $aliases = [], $tbl_name = "") {
        $rs = $this->getRecordset($sql, $dbconn, 0);

        foreach ($rs->columns as $column) {
            if ($column["type"] != waLibs\waDB::CONTAINER) {
                // occhio: oracle non restituisce il nome tabella tra gli attributi della colonna...
                $col = $table->addColumn($column["name"], $column["name"]);
                if (!($_GET["watable_export_csv"] || $_GET["watable_export_pdf"] || $_GET["watable_export_xls"])) {
                    $col->aliasOf = $aliases[$column["name"]] ? $aliases[$column["name"]] : "$tbl_name." . $column["name"];
                    $fk_tbl_name = "";
                    $fk_fld_name = "ID";
                    // se è una tabella "crud" cerchiamo le fk
                    if ($fk_tbl_name = $this->getFK($tbl_name, $column["name"])) {
                    } elseif ($column["name"] == "CREATEUSERID" || $column["name"] == "UPDATEUSERID") {
                        $fk_tbl_name = "CC_USER";
                    } elseif ($this->sessionData["tablenames"]["CCTL_" . $column["name"]]) {
                        $fk_tbl_name = "CCTL_" . $column["name"];
                    } elseif ($this->sessionData["point_tos"]["$tbl_name.$column[name]"]) {
                        $fk_tbl_name = $this->sessionData["point_tos"]["$tbl_name.$column[name]"]["dest_table"];
                        $fk_fld_name = $this->sessionData["point_tos"]["$tbl_name.$column[name]"]["dest_field"];
                    }
                    if ($fk_tbl_name) {
                        $col->HTMLConversion = false;
                        $col->computeFunction = function (\waLibs\waTable $table) use ($fk_tbl_name, $fk_fld_name, $column) {
                            $value = $table->record->value($column["name"]);
                            if ($value) {
                                return "<a href='javascript:document.waPage.fkLink(\"$fk_tbl_name\", \"$fk_fld_name\", \"$value\");' onmouseover='document.waPage.fkLinkMouseOver(this, \"$fk_tbl_name\", \"$fk_fld_name\", \"$value\");' onmouseout='document.waPage.fkLinkMouseOut();'>$value</a>";
                            }
                            return "";
                        };
                    }
                }
            }
        }
    }

    //***************************************************************************

    /**
     * cerca una foreignKey tra le constraints
     */
    function getFK($table_name, $column_name) {
        if ($table_name && $this->sessionData["constraints"]["parents"][$table_name]) {
            foreach ($this->sessionData["constraints"]["parents"][$table_name] as $constraint) {
                if ($constraint->column === $column_name) {
                    return $constraint->parent;
                }
            }
        }
        
    }
    //***************************************************************************

    /**
     * -
     */
    function setFormFields($readOnly = false) {

        foreach ($this->form->recordset->columns as $column) {
            if ($column["type"] == waLibs\waDB::DATE) {
                $this->form->addDate($column["name"], $column["name"], $readOnly);
            } elseif ($column["type"] == waLibs\waDB::DATETIME) {
                $this->form->addDateTime($column["name"], $column["name"], $readOnly);
            } elseif ($column["type"] == waLibs\waDB::DECIMAL) {
                $this->form->addCurrency($column["name"], $column["name"], $readOnly);
            } elseif ($column["type"] == waLibs\waDB::INTEGER) {
                $this->form->addInteger($column["name"], $column["name"], $readOnly);
            } elseif ($column["type"] == waLibs\waDB::CONTAINER) {
                $first_char = $this->form->recordset->records[0] ? substr(trim($this->form->recordset->records[0]->value($column["name"])), 0, 1) : "";
                if ($first_char == "{" || $first_char == "[") {
                    $this->form->addGeneric("waJson", $column["name"], $column["name"], $readOnly);
                } else {
                    $this->form->addTextArea($column["name"], $column["name"], $readOnly);
                }
            } else {
                $this->form->addText($column["name"], $column["name"], $readOnly);
            }
        }
    }

    //***************************************************************************
    function initTablseEnv() {
//return;
//        phpinfo();exit();
        if (!$this->user->env) {
            return;
        }
        $env = $this->user->env;
        $this->sessionData["tables"] = [];
        $this->sessionData["tablenames"] = [];
        $this->sessionData["point_tos"] = [];
        $this->sessionData["constraints"] = [];
        $this->sessionData["primary_keys"] = [];
        
        $db_account = $this->user->db_accounts[$env];
        $this->fileConfigDB = new \waLibs\waDBPparams();
        $this->fileConfigDB->DBTYPE = $this->getDBType();
        $this->fileConfigDB->HOST = $db_account->HOST;
        $this->fileConfigDB->PORT = $db_account->PORT;
        $this->fileConfigDB->DBNAME = $this->getDBName($db_account->SID, $db_account->PDB);
        $this->fileConfigDB->USERNAME = $db_account->USERNAME;
        $this->fileConfigDB->PASSWORD = $db_account->PASSWORD;
        
        $this->setTables($env);
        $this->setConstraints($env);
        $this->setPrimaryKeys($env);
        $this->setPointTos();
        $this->setViews();
    }

    //***************************************************************************
    function setPointTos() {

        if (file_exists("$this->directoryDoc/tables.cache/point_tos")) {
            $this->sessionData["point_tos"] = unserialize(file_get_contents("$this->directoryDoc/tables.cache/point_tos"));
        }
        
    }

    //***************************************************************************
    function setViews() {
        
        if (!isset($this->sessionData["views"])) {
            $this->sessionData["views"] = [];
            $view_files = glob("$this->directoryDoc/views/*." . $this->user->username);
            if ($view_files) {
                foreach ($view_files as $view_file) {
                    $elems = explode(".", basename($view_file), 2);
                    $id = $elems[0];
                    $view = unserialize(file_get_contents($view_file));
                    $this->sessionData["views"][$id] = $view->view_title;
                }
                asort($this->sessionData["views"]);
            }
        }
    }

    //***************************************************************************
    function setTables($env) {
        
        $data_file = "$this->directoryDoc/tables.cache/". $this->user->username . "/$env";
        if (file_exists($data_file)) {
            $this->sessionData["tables"] = unserialize(file_get_contents($data_file));
        } else {
            $dbconn = $this->getDBConnection();
            $schema = $this->getCurrentSchema();
            $sql = "select table_name, num_rows" .
                    " from all_tables" .
                    " where owner='$schema'" .
                    " and not table_name like 'CCST_%'" .
                    " order by table_name";
            $rs = $this->getRecordset($sql, $dbconn);
            foreach ($rs->records as $record) {
                $this->sessionData["tables"][] = ["table_name" => $record->table_name, "num_rows" => $record->num_rows];
            }
            // sa il signore perchè, ma oracle fallisce la order by...
            usort($this->sessionData["tables"], function ($a, $b) {
                return strcmp(str_replace("_", "A", $a["table_name"]), str_replace("_", "A", $b["table_name"]));
            });
            @mkdir("$this->directoryDoc/tables.cache/". $this->user->username);
            file_put_contents($data_file, serialize($this->sessionData["tables"]));
        }
        foreach ($this->sessionData["tables"] as &$table) {
                $this->sessionData["tablenames"][$table["table_name"]] = $table["table_name"];
        }
    }

    //***************************************************************************
    function setConstraints($env) {
        
        $data_file = "$this->directoryDoc/tables.cache/". $this->user->username . "/$env.constraints";
        if (file_exists($data_file)) {
            $this->sessionData["constraints"] = unserialize(file_get_contents($data_file));
        } else {
            $dbconn = $this->getDBConnection();
            $schema = $this->getCurrentSchema();
            $sql = "SELECT a.table_name, a.column_name, c_pk.table_name parent_table_name, c.constraint_name, c.constraint_type, c.status"
                    . " FROM all_cons_columns a"
                    . " JOIN all_constraints c ON a.owner = c.owner AND a.constraint_name = c.constraint_name"
                    . " LEFT JOIN all_constraints c_pk ON c.r_owner = c_pk.owner AND c.r_constraint_name = c_pk.constraint_name"
                    . " WHERE a.owner='$schema' "
                    . " AND c.constraint_type='R'"
                    . " AND c_pk.table_name is not null"
                    . " ORDER BY a.table_name";
            $rs = $this->getRecordset($sql, $dbconn);
            foreach ($rs->records as $record) {
                $constraint = new stdClass();
		$constraint->table = $record->table_name;
		$constraint->column = $record->column_name;
		$constraint->parent = $record->parent_table_name;
		$constraint->name = $record->constraint_name;
		$constraint->type = $record->constraint_type;
		$constraint->enabled = strcasecmp($record->status, "ENABLED") === 0;
                $this->sessionData["constraints"]["parents"][$constraint->table][] = $constraint;
                $this->sessionData["constraints"]["children"][$constraint->parent][] = $constraint;
            }
            @mkdir("$this->directoryDoc/tables.cache/". $this->user->username);
            file_put_contents($data_file, serialize($this->sessionData["constraints"]));
        }
    }

    //***************************************************************************
    function setPrimaryKeys($env) {
        
        $data_file = "$this->directoryDoc/tables.cache/". $this->user->username . "/$env.primary_keys";
        if (file_exists($data_file)) {
            $this->sessionData["primary_keys"] = unserialize(file_get_contents($data_file));
        } else {
            $dbconn = $this->getDBConnection();
            $schema = $this->getCurrentSchema();
            
            $sql = "SELECT cc.table_name, cc.column_name, tc.data_type, c.status"
                    . " FROM all_constraints c"
                    . " JOIN all_cons_columns cc ON c.owner = cc.owner AND c.constraint_name = cc.constraint_name"
                    . " JOIN all_tab_columns tc ON cc.owner = tc.owner AND cc.table_name = tc.table_name AND cc.column_name=tc.column_name"
                    . " WHERE c.owner='$schema' "
                    . " AND c.constraint_type='P'"
                    . " ORDER BY cc.table_name";
            $rs = $this->getRecordset($sql, $dbconn);
            foreach ($rs->records as $record) {
                $pk = new stdClass();
                $pk->table = $record->table_name;
                $pk->column = $record->column_name;
                $pk->type = $record->data_type;
                $pk->enabled = strcasecmp($record->status, "ENABLED") === 0;
                $this->sessionData["primary_keys"][$pk->table] = $pk;
            }
            @mkdir("$this->directoryDoc/tables.cache/". $this->user->username);
            file_put_contents($data_file, serialize($this->sessionData["primary_keys"]));
        }
    }

    //***************************************************************************
    function getOrderedTableColumns(\waLibs\waDBConnection $dbconn, $tbl_name) {
//		$sql = "SELECT LISTAGG(CONCAT('$tbl_name.', column_name), ',') WITHIN GROUP (ORDER BY column_name) ordered_columns" .
//				" FROM ALL_TAB_COLUMNS" .
//				" WHERE TABLE_NAME = '$tbl_name'" .
//				" AND column_name!='ID'";
//		return "$tbl_name.ID," . $this->getRecordset($sql, $dbconn, 1)->records[0]->ordered_columns;
        $schema = $this->getCurrentSchema();
        $pkColumn = $this->getPrimaryKey($tbl_name);
        $sql = "SELECT column_name" .
                " FROM ALL_TAB_COLUMNS" .
                " WHERE OWNER='$schema'" .
                " AND TABLE_NAME = '$tbl_name'" .
                " AND DATA_TYPE != 'SDO_GEOMETRY'" .
                ($pkColumn ? " AND column_name!='$pkColumn'" : "") .
                " ORDER BY column_name";
        $rs = $this->getRecordset($sql, $dbconn);
        $retval = $comma = "";
        foreach ($rs->records as $record) {
            $retval .= "$comma$record->column_name";
            $comma = ",";
        }
        return ($pkColumn ? "$pkColumn," : "") . $retval;
    }

    //***************************************************************************
    function oneWayEncrypt($toEncrypt) {
        return hash(APPL_ONE_WAY_ENCRYPT_ALGO, $toEncrypt);
    }

    //***************************************************************************
    function twoWayEncrypt($toEncrypt, $key) {

        $iv = str_pad(substr($key, 0, APPL_TWO_WAY_ENCRYPT_IV_LEN), APPL_TWO_WAY_ENCRYPT_IV_LEN, "0");
        return openssl_encrypt($toEncrypt, APPL_TWO_WAY_ENCRYPT_METHOD, $key, false, $iv);
    }

    //***************************************************************************
    function twoWayDecrypt($toDecrypt, $key) {

        $iv = str_pad(substr($key, 0, APPL_TWO_WAY_ENCRYPT_IV_LEN), APPL_TWO_WAY_ENCRYPT_IV_LEN, "0");
        return openssl_decrypt($toDecrypt, APPL_TWO_WAY_ENCRYPT_METHOD, $key, false, $iv);
    }

    //***************************************************************************
    function saveUserData() {

        $user_data_encrypted = $this->twoWayEncrypt(serialize($this->user), $this->user->password_encrypted);
        file_put_contents("$this->directoryDoc/accounts/" . $this->user->username, $user_data_encrypted);
    }

    //***************************************************************************
    function getCurrentSchema() {

        return $this->user->db_accounts[$this->user->env]->SCHEMA;
    }

    //***************************************************************************
    function getPrimaryKey($table_name) {

        if ($this->sessionData["primary_keys"][$table_name]) {
            return $this->sessionData["primary_keys"][$table_name]->column;
        }

    }

    //***************************************************************************
    function getPrimaryKeySqlValue(waLibs\waDBConnection $dbconn, $table_name, $id) {

        if ($this->sessionData["primary_keys"][$table_name]) {
            if (strtoupper($this->sessionData["primary_keys"][$table_name]->type) == "NUMBER") {
                return $dbconn->sqlInteger($id);
            }
            return $dbconn->sqlString($id);
        }

    }

    //***************************************************************************
    function getDBType() {

        if (APPL_DB_DRIVER === "ORACLE") {
            return waLibs\waDB::DBTYPE_ORACLE;
        }
        return waLibs\waDB::DBTYPE_ASAS;
    }

    //***************************************************************************
    function getDBName($SID, $PDB) {

        if (APPL_DB_DRIVER === "ORACLE") {
            return $PDB ? $PDB : $SID;
        }
        return $PDB ? "/$PDB" : ":$SID";
    }

    //**************************************************************************
    function execute($cmd, $workdir = null) {

        $pwd = "";
        if (!is_null($workdir)) {
            $pwd = getcwd();
            chdir($workdir);
        }

        $somethingOut = false;
        $handle = popen("$cmd", "r");
        while ($row = fgets($handle)) {
            $this->log(str_replace("\n", "", $row));
            $somethingOut = true;
        }
        pclose($handle);

        if ($pwd) {
            chdir($pwd);
        }
        if (!$somethingOut) {
            $this->log("Error executing: $cmd", true);
        }
        
    }

    //***************************************************************************
    function logStart($title) {
        
        $this->timeStart = time();
        header( 'Content-type: text/html; charset=utf-8' );
        echo "<pre>$title\n\n";
        ob_flush();
        flush();

    }

    //***************************************************************************
    function log($toLog, $error = false) {
        
        echo "$toLog\n";
        ob_flush();
        flush();
        
        if ($error) {
            $this->logEnd();
        }

    }

    //***************************************************************************
    function logEnd() {
        
        $timing = time() - $this->timeStart;
        echo "\n\n-----------------------------------------------\n" .
                "finished in $timing seconds\n</pre>";

        exit();
    }

    //***************************************************************************
    function pathAdjust($path) {
        if (strpos($path, "\\") !== false) {
            $path = str_replace("/", "\\", $path);
        }
        return $path;
    }

    //***************************************************************************
    function setControlInvisible(waLibs\waControl $ctrl) {
        $ctrl->visible = false;
        $ctrl->form->labels[$ctrl->name]->visible = false;
    }

    //***************************************************************************
    function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }

    //***************************************************************************
    function remoteExec(phpseclib3\Net\SFTP $sftp, $command) {
        
        $this->log("Executing remote command: $command");
        
        $out = $sftp->exec($command);
        if ($out === false) {
            $this->log("Error while executing remote command: $command", true);
        }
        if ($out === true) {
            $out = "";
        }
        $this->log($out ? $out : "Remote command executed");
        
    }

    //***************************************************************************
    function remotePut(phpseclib3\Net\SFTP $sftp, $src, $dest) {
        
        $this->log("Copying " . basename($src) . " to remote server");
        $out = $sftp->put($dest, $src, phpseclib3\Net\SFTP::SOURCE_LOCAL_FILE);
        if ($out === false) {
            $this->log("Error while copying to remote server: " . basename($src), true);
        }
        
        $this->log(basename($src) . " copied to remote server");

    }

    //***************************************************************************
    function remoteGet(phpseclib3\Net\SFTP $sftp, $src, $dest) {
        
        $this->log("Copying " . basename($src) . " from remote server");
        $out = $sftp->get($src, $dest);
        if ($out === false) {
            $this->log("Error while copying from remote server: " . basename($src), true);
        }
        $this->log(basename($src) . " copied from remote server");

    }

    //***************************************************************************
    function cleanUpViewSql($inSql) {
        
        $sql = "";
        $lines = explode("\n", $inSql);
        foreach ($lines as $line) {
            $comment_start = strpos($line, "--");
            if ($comment_start !== false) {
                $line = substr($line, 0, $comment_start);
            }
            if (trim($line)) {
                $sql .= "$line\n";
            }
        }

        $sql = trim($sql);
        if (substr($sql, -1) == ";") {
            $sql = substr($sql, 0, -1);
        }
        return $sql;
    }


    //**************************************************************************
}

// fine classe uqueryall
	
//***************************************************************************

