<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

	//**************************************************************************
	function __construct() {
		parent::__construct();

                $base = "$this->directoryDoc/tables.cache/". $this->user->username . "/" . $this->user->env;
		unlink($base);
		unlink("$base.constraints");
		unlink("$base.primary_keys");
		$this->response();
	}

	//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
