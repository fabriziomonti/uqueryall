<?php

include "doc_check.class.php";

//*****************************************************************************
class page extends doc_check {


    //**************************************************************************
    function __construct() {
        
        $this->page_title = "Check FDMAgp EcmLoader results";
        $this->main_dir = "/InboundFlows/Filenet/Smistadoc/FDM/Agp";
        $this->log_name = "infoFDMAgp.log";
        
        parent::__construct();
    }
    
//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
