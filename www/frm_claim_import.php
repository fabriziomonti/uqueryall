<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

    /**
     *
     * @var waLibs\waForm
     */
    var $form;
    
    var $timeStart;

    //**************************************************************************
    function __construct() {
        parent::__construct();

        if (!$this->user->env || count($this->user->db_accounts) <= 1 || !$this->sessionData["tablenames"]["CC_CLAIM"]) {
            $this->showMessage("Not available", "Operation not available on current database", false, true);
        }

        $this->createForm();

        if ($this->form->isToUpdate()) {
            $this->import();
        } else {
            $this->showPage();
        }
    }

    //*****************************************************************************

    /**
     * mostra
     * 
     * costruisce la pagina contenente il form e la manda in output
     * @return void
     */
    function showPage() {
        $this->addItem("Claim import", "title");
        $this->addItem($this->form);
        $this->show();
    }

    //***************************************************************************
    function createForm() {

        $this->form = $this->getForm();

        $this->form->openTab("main", "Main");
            $this->form->addText("dest_db", "Destination database", true)->value = $this->user->env;
            $ctrl = $this->form->addSelect("source_db", "Source database", false, true);
            $ctrl->list = [];
            foreach ($this->user->db_accounts as $env => $db_account) {
                if ($env != $this->user->env) {
                    $ctrl->list[$env] = "$env ($db_account->SCHEMA)";
                }
            }
            $this->form->addTextArea("claim_list", "Claim list (1 ClaimNumber -> 1 row)", false, true);
            $this->form->addBoolean("remote", "Remote execution");
            $this->form->addBoolean("custom_params", "Custom parameters");
        $this->form->closeTab();
        
        $this->form->openTab("ssh", "SSH");
            $this->form->addText("remote_host", "Remote host")->value = $this->user->liquidump_remote_host;
            $this->form->addText("remote_username", "Remote username")->value = $this->user->liquidump_remote_username;
            $this->form->addPassword("remote_password", "Remote password")->value = $this->user->liquidump_remote_password;
            $this->form->addTextArea("remote_java_env", "Remote Java env")->value = $this->user->liquidump_remote_java_env;
        $this->form->closeTab();
            
        $this->form->openTab("custom", "Custom parameters");
            $this->form->addTextArea("dump_params", "Dump parameters")->value = file_get_contents(__DIR__ . "/liquidump_params/ClaimDump.conf");
            $this->form->addTextArea("restore_params", "Restore Parameters")->value = file_get_contents(__DIR__ . "/liquidump_params/ClaimStore.conf");
        $this->form->closeTab();

        $this->form_submitButtons($this->form, false, false);
        $this->form->getInputValues();
    }

    //***************************************************************************
    function import() {

        $this->checkMandatory($this->form);

        $process_id = date("Ymd_His");
        $claimdump_conf = "";

        $claim_list = explode("\n", $this->form->claim_list);
        foreach ($claim_list as $claim) {
            if ($claim = trim($claim)) {
                $claimdump_conf .= "claim=$claim\n";
            }
        }
        $liquidump_bin_path = dirname(APPL_LIQUIDUMP_CMD);

        $sourceAccount = $this->user->db_accounts[$this->form->source_db];
        $claimdump_conf .= "\ndbHost=$sourceAccount->HOST\n" .
                "dbPort=$sourceAccount->PORT\n" .
                "dbSID=$sourceAccount->SID\n" .
                "dbPDB=$sourceAccount->PDB\n" .
                "dbUser=$sourceAccount->USERNAME\n" .
                "dbPassword=$sourceAccount->PASSWORD\n" .
                "dbSchema=$sourceAccount->SCHEMA\n" .
                "workingDir=./$process_id\n\n" .
                $this->form->dump_params;
        file_put_contents($this->pathAdjust("$liquidump_bin_path/ClaimDump.conf"), $claimdump_conf);

        $destAccount = $this->user->db_accounts[$this->user->env];
        $claimstore_conf .= "\ndbHost=$destAccount->HOST\n" .
                "dbPort=$destAccount->PORT\n" .
                "dbSID=$destAccount->SID\n" .
                "dbPDB=$destAccount->PDB\n" .
                "dbUser=$destAccount->USERNAME\n" .
                "dbPassword=$destAccount->PASSWORD\n" .
                "dbSchema=$destAccount->SCHEMA\n" .
                "workingDir=./$process_id\n\n" .
                $this->form->restore_params;
        file_put_contents($this->pathAdjust("$liquidump_bin_path/ClaimStore.conf"), $claimstore_conf);

        set_time_limit(0);
        $this->logStart("Claim import");
        
        $this->log("ClaimDump");
        if ($this->form->remote) {
            require ('autoload.php');
            $loader = new \Composer\Autoload\ClassLoader();
            $this->log("Remote login");
            $sftp = new phpseclib3\Net\SFTP($this->form->remote_host);   // Domain or IP
            if (!$sftp->login($this->form->remote_username, $this->form->remote_password)) {
                $this->log("Remote login failed", true);
            }
            $sftp->mkdir("liquidump.$process_id");
            $sftp->chdir("liquidump.$process_id");
            $this->remotePut($sftp, $this->pathAdjust("$liquidump_bin_path/ClaimDump.conf"), "ClaimDump.conf");
            $this->remotePut($sftp, $this->pathAdjust("$liquidump_bin_path/liquidump.jar"), "liquidump.jar");
            $liquidump_remote_sh = $this->form->remote_java_env . "\n\njava -jar liquidump.jar ClaimDump\n";
            $liquidump_remote_sh = str_replace("\r", "", $liquidump_remote_sh);
            file_put_contents($this->pathAdjust("$liquidump_bin_path/liquidump_remote.sh"), $liquidump_remote_sh);
            $this->remotePut($sftp, $this->pathAdjust("$liquidump_bin_path/liquidump_remote.sh"), "liquidump.sh");
            $sftp->chmod(0777, "liquidump.sh");
            $this->log("ClaimDump start");
            $this->remoteExec($sftp, "cd ~/liquidump.$process_id;bash -c ./liquidump.sh");
            $this->remoteExec($sftp, "cd ~/liquidump.$process_id;zip -r $process_id.zip $process_id/");
            $this->remoteGet($sftp, "$process_id.zip", $this->pathAdjust("$liquidump_bin_path/$process_id.zip"));
            $this->remoteExec($sftp, "rm -rf ~/liquidump.$process_id");
            $this->log("Unzip $process_id.zip");
            $sftp->disconnect();
            $this->execute(APPL_UNZIP_CMD . " $process_id.zip", $liquidump_bin_path);
            unlink("$liquidump_bin_path/$process_id.zip");
        } 
        else {
            $this->execute(APPL_LIQUIDUMP_CMD . " ClaimDump", $liquidump_bin_path);
        }
        
        $this->log("ClaimStore");
        $this->execute(APPL_LIQUIDUMP_CMD . " ClaimStore", $liquidump_bin_path);

        $this->deleteDirectory($this->pathAdjust("$liquidump_bin_path/$process_id"));
        $this->logEnd();
    }

//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
