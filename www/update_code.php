<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

    //**************************************************************************
    function __construct() {
        parent::__construct();
        
        $this->logStart("Update code");
        $this->log("Update app");
        $this->execute('git pull');
        $this->log("Update framework");
        $this->execute('git pull', __DIR__ . "/walibs4");
        $this->logEnd();

    }

//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
