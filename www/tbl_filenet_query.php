<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

    var $username_placeholder = "__username_placeholder__";
    var $password_placeholder = "__password_placeholder__";
    var $objectstore_placeholder = "__objectstore_placeholder__";
    var $queryfilter_placeholder = "__queryfilter_placeholder__";
    var $resulsize_placeholder = "__resulsize_placeholder__";
    
    var $form;
    
    //**************************************************************************
    function __construct() {
        parent::__construct();

        $this->childWindow = true;
        $this->addItem("FileNet query", "title");
        $this->addItem($this->myGetTable());
        $this->show();
    }

    //***************************************************************************
    function myGetTable() {

        $this->form = unserialize(base64_decode($_GET["params"]));
        $request = $this->getRequest();
        $documents = $this->getResponse($request);
        if (!$documents) {
            $this->showMessage("Nessun risultato", "Nessun risultato", true, true);
        }
        
        $table = parent::getTable($documents);
        
        // actions
        $table->removeAction("New");
        $table->removeAction("Edit");
        $table->removeAction("Delete");
        $table->addAction("Back");
        
        // columns
        foreach ($documents[0] as $key => $val) {
            $table->addColumn($key, $key);
        }
        $table->loadRows();
        
        
        $this->addItem("FileNet query", "title");
        $this->addItem($table);
        $this->show();
        
        
    }

    //***************************************************************************
    function getResponse($request) {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->form->endpoint);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/xml"]);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["SoapAction: loadDocumentsByQuery"]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        $response = curl_exec($ch);
        curl_close($ch);
        $documents = $this->getDataAsArray($response);
        return $documents;
        
    }

    //***************************************************************************
    function getRequest() {

        $request = $this->getRequestTemplate();
        $request = str_replace($this->username_placeholder, $this->form->username, $request);
        $request = str_replace($this->password_placeholder, $this->form->password, $request);
        $request = str_replace($this->objectstore_placeholder, $this->form->object_store, $request);
        $request = str_replace($this->resulsize_placeholder, $this->form->max_result, $request);
        
        $query_filter = "";
        if (trim($this->form->query)) {
            $query_filter = $this->form->query;
        }
        else {
            $and = "";
            if ($this->form->CodiceDoc) {
                $query_filter = "$and CodiceDoc like '" . $this->form->CodiceDoc . "'";
                $and = " and";
            }
            if ($this->form->Barcode) {
                $query_filter .= "$and Barcode like '" . $this->form->Barcode . "'";
                $and = " and";
            }
            if ($this->form->DateCreated_from) {
                $query_filter .= "$and DateCreated &gt;= " . date("Ymd", $this->form->DateCreated_from) . "T000000Z";
                $and = " and";
            }
            if ($this->form->DateCreated_to) {
                $query_filter .= "$and DateCreated &lt; " . date("Ymd", $this->form->DateCreated_to) . "T235959Z";
                $and = " and";
            }
        }
        $request = str_replace($this->queryfilter_placeholder, $query_filter, $request);

        return $request;
    }

    //***************************************************************************
    function getDataAsArray($response) {
        $retval = [];
        
        $response = $this->mungXML($response);
        $passo = json_decode(json_encode(simplexml_load_string($response)),true);
        $ripasso = $passo["soapenv_Body"]["p526_loadDocumentsByQueryResponse"]["loadDocumentsByQueryReturn"]["EcmDocument"];
        if (!$ripasso) {
            return $retval;
        }
        
        $raw_documents_data = [];
        if ($ripasso["id"]) {
            // c'è un documento solo, quindi non abbiamo un array di documenti ma
            // un solo array di attributi del solo documento
            $raw_documents_data[] = $ripasso;
        }
        else {
            $raw_documents_data = $ripasso;
        }
        
        foreach ($raw_documents_data as $raw_document_data) {
            $document_data = [];
            foreach ($raw_document_data["attributes"]["item"] as $attribute) {
                $attribute_name = $attribute["key"];
                $raw_value = $attribute["value"]["value"]["value"];
                if (is_array($raw_value)) {
                    $attribute_value = implode( " | ", $raw_value);
                }
                else {
                    $attribute_value = $raw_value;
                }
                $document_data[$attribute_name] = $attribute_value;
            }
            ksort($document_data,SORT_STRING | SORT_FLAG_CASE);
            $retval[] = $document_data;
            
        }
        
        return $retval;
        
    }

    //***************************************************************************
    // FUNCTION TO MUNG THE XML SO WE DO NOT HAVE TO DEAL WITH NAMESPACE
    function mungXML($xml) {
        $obj = SimpleXML_Load_String($xml);
        if ($obj === FALSE) return $xml;

        // GET NAMESPACES, IF ANY
        $nss = $obj->getNamespaces(TRUE);
        if (empty($nss)) return $xml;

        // CHANGE ns: INTO ns_
        $nsm = array_keys($nss);
        foreach ($nsm as $key) {
            // A REGULAR EXPRESSION TO MUNG THE XML
            $rgx
            = '#'               // REGEX DELIMITER
            . '('               // GROUP PATTERN 1
            . '\<'              // LOCATE A LEFT WICKET
            . '/?'              // MAYBE FOLLOWED BY A SLASH
            . preg_quote($key)  // THE NAMESPACE
            . ')'               // END GROUP PATTERN
            . '('               // GROUP PATTERN 2
            . ':{1}'            // A COLON (EXACTLY ONE)
            . ')'               // END GROUP PATTERN
            . '#'               // REGEX DELIMITER
            ;
            // INSERT THE UNDERSCORE INTO THE TAG NAME
            $rep
            = '$1'          // BACKREFERENCE TO GROUP 1
            . '_'           // LITERAL UNDERSCORE IN PLACE OF GROUP 2
            ;
            // PERFORM THE REPLACEMENT
            $xml =  preg_replace($rgx, $rep, $xml);
        }

        return $xml;

    } // End :: mungXML()

    //***************************************************************************
    function getRequestTemplate() {
        return '
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:it="http://it.unipol.ecm.document">
   <soapenv:Header>
       <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
            <wsse:UsernameToken>
                <wsse:Username>' . $this->username_placeholder . '</wsse:Username>
                <wsse:Password>' . $this->password_placeholder . '</wsse:Password>
            </wsse:UsernameToken>
        </wsse:Security>
   </soapenv:Header>

   <soapenv:Body>
      <it:loadDocumentsByQuery>
         <documentSearch>
            <key>
               <company>' . $this->objectstore_placeholder . '</company>
               <project/>
               <application/>
               <category/>
               <entity/>
            </key>
            <id/>
            <code/>
            <type/>
            <title/>
            <level>0</level>
            <model></model>

            <attributes>
               <!--Zero or more repetitions:-->
            </attributes>
         </documentSearch>
         <documentQuery>
            <selectFieldList>
               <string>AgenziaDoc</string>
               <string>ApebAckEnabled</string>
               <string>Attivita</string>
               <string>Barcode</string>
               <string>Categoria</string>
               <string>ClassificationStatus</string>
               <string>CmIndexingFailureCode</string>
               <string>CmIsMarkedForDeletion</string>
               <string>CmRetentionDate</string>
               <string>Coda</string>
               <string>CodBusta</string>
               <string>CodDocOriginali</string>
               <!-- string>CodDocRielabOCR</string-->
               <string>CodiceDoc</string>
               <string>CodTrasm</string>
               <string>CommentoDoc</string>
               <string>ComponentBindingLabel</string>
               <string>CompoundDocumentState</string>
               <string>ContentElementsPresent</string>
               <string>ContentRetentionDate</string>
               <string>ContentSize</string>
               <string>Creator</string>
               <string>CurrentState</string>
               <string>DataPerv</string>
               <string>DataRicezione</string>
               <string>DataScansione</string>
               <string>DataSpedizione</string>
               <string>DataTimbroPostale</string>
               <string>DateCheckedIn</string>
               <string>DateContentLastAccessed</string>
               <string>DateCreated</string>
               <string>DateLastModified</string>
               <string>DescScarto</string>
               <string>DocGiaLavorato</string>
               <string>DocumentTitle</string>
               <string>EntryTemplateId</string>
               <string>EntryTemplateLaunchedWorkflowNumber</string>
               <string>EntryTemplateObjectStoreName</string>
               <string>FaxDestinatario</string>
               <string>FaxMittente</string>
               <!-- string>FlagElabOCR</string-->
               <string>FlagMigrato</string>
               <string>FolderDoc</string>
               <string>GpsIndirizzoNorm</string>
               <string>GpsLatitudine</string>
               <string>GpsLongitudine</string>string>
               <string>GpsTimestamp</string>
               <string>IDClaimant</string>
               <string>IDEvento</string>
               <string>IDExposure</string>
               <string>IDIncident</string>
               <string>IDMatter</string>
               <string>IDPagamento</string>
               <string>IDSinistro</string>
               <!-- string>IdWorkItem</string-->
               <string>IgnoreRedirect</string>
               <string>IndexationId</string>
               <!--string>IndicPagine</string-->
               <string>InfoAutoreFoto</string>
               <string>IsCurrentVersion</string>
               <string>IsFrozenVersion</string>
               <string>IsInExceptionState</string>
               <string>IsReserved</string>
               <string>IsVersioningEnabled</string>
               <string>KeyDocLogOptimo</string>
               <string>LastModifier</string>
               <string>LivelloDoc</string>
               <string>LockOwner</string>
               <string>LockTimeout</string>
               <string>LockToken</string>
               <string>MailDestinatario</string>
               <string>MailMittente</string>
               <string>MailSmistatore</string>
               <string>MailSubject</string>
               <string>MajorVersionNumber</string>
               <string>MimeType</string>
               <string>MinorVersionNumber</string>
               <string>Mittente</string>
               <string>Name</string>
               <string>NominativoCreazioneDoc</string>
               <string>NominativoModificaDoc</string>
               <string>NumDocBusta</string>
               <string>NumKfxBatchId</string>
               <string>NumKfxDocId</string>
               <string>NumPagine</string>
               <string>NumProgTrasm</string>
               <string>NumRaccomandata</string>
               <string>Owner</string>
               <string>Pacco</string>
               <string>ProgrBusta</string>
               <string>RisorsaDoc</string>
               <string>RuoloAutoreFoto</string>
               <string>RuoloUtenzaCreazioneDoc</string>
               <string>Scatola</string>
               <string>SistComp</string>
               <string>SistCompOriginale</string>
               <string>StatoDoc</string>
               <string>StorageLocation</string>
               <!--string>SXTipoIdentita</string-->
               <string>TipoCanale</string>
               <string>TipoDoc</string>
               <string>TipoIngresso</string>
               <string>TipoScarto</string>
               <string>UtenzaCreazioneDoc</string>
               <string>UtenzaModificaDoc</string>
               <string>VersionStatus</string>
            </selectFieldList>
            <queryFilter>' . $this->queryfilter_placeholder . '</queryFilter>
            <sortFieldList/>
            <resultSize>' . $this->resulsize_placeholder . '</resultSize>
         </documentQuery>
      </it:loadDocumentsByQuery>
   </soapenv:Body>
</soapenv:Envelope>        

        ';
    }

//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
        