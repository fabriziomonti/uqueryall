<?php

//******************************************************************************
include "uqueryall.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends uqueryall {


	//*****************************************************************************
	function __construct() {
		parent::__construct();

		$this->addItem($this->getMenu());
		$this->addItem("", "title");
		if (!$this->user->db_accounts) {
			$this->addItem("Confgura almeno un database per vedere tabelle e dati.", "testo_libero");
		}
		elseif (!$this->user->env) {
			$this->addItem("Seleziona un database per vedere tabelle e dati.", "testo_libero");
		}
		else {
			$this->addItem($this->myGetForm());
		}
		$this->show();
	}

	//*****************************************************************************

	/**
	 * @return waLibs\waForm
	 */
	function myGetForm() {
		
		$form = parent::getForm();
		$form->destinationPage = "tbl_crud.php";
		$ctrl = $form->addSelectTypeahead("typeahead_table_name", "Table quick search", false);
		$ctrl->list = $this->sessionData["tablenames"];
		
		$form->getInputValues();
		if ($form->isToUpdate()) {
			$this->redirect("tbl_crud.php?tbl_name=" . $form->typeahead_table_name);
		}
		
		return $form;
		
		
	}

	//*****************************************************************************
}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();
