<?php

//******************************************************************************
include "uqueryall.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends uqueryall {

    var $tbl_name;

    //*****************************************************************************
    function __construct() {
        parent::__construct();
        $this->tbl_name = $_POST["typeahead_table_name"] ? $_POST["typeahead_table_name"] : $_GET["tbl_name"];

        if (!$this->sessionData["tablenames"][$this->tbl_name]) {
		$this->redirect("home.php");
        }         
        
        $this->addItem($this->getMenu());
        $this->addItem("table $this->tbl_name - data", "title");
        $this->addItem($this->myGetForm());
        $this->addItem($this->myGetTable());
        $this->addItem($this->getFkTooltipSpace(), "fk_tooltip");
        $this->show();
    }

    //*****************************************************************************

    /**
     * @return waLibs\waTable
     */
    function myGetTable() {
        // creazione della tabella
        $dbconn = $this->getDBConnection();
        $schema = $this->getCurrentSchema();
        $sql = "SELECT " . $this->getOrderedTableColumns($dbconn, $this->tbl_name) . " FROM $schema.$this->tbl_name";
        $retirable = strpos($sql, ",RETIRED") !== false;
        if ($retirable && !$_GET["retiredtoo"]) {
            $sql .= " WHERE RETIRED=0";
        }
        $pkColumn = $this->getPrimaryKey($this->tbl_name);
        $sql .= $pkColumn ? " ORDER BY $this->tbl_name.$pkColumn DESC" : "";

        $table = parent::getTable($sql);
        $table->formPage = "frm_crud.php?tbl_name=$this->tbl_name";

        if (!$pkColumn) {
            $table->removeAction("Edit");
            $table->removeAction("Delete");
        }
        
        if ($retirable && $_GET["retiredtoo"]) {
            $table->addAction("NoRretired", false, "No retired");
        } elseif ($retirable) {
            $table->addAction("RetiredToo", false, "Retired");
        }
        $table->addAction("Columns");
        $table->addAction("Constraints");
        $table->addAction("Indexes");
        $table->addAction("Foreign_keys", false, "Foreign keys");
        $table->addAction("Ref_up", false, "Ref. up");
        $table->addAction("Ref_down", false, "Ref. down");
        $table->addAction("Import");

        if ($this->sessionData["constraints"]["children"][$this->tbl_name]) {
            $table->addAction("sep", true, "<hr>");
            $constraints = &$this->sessionData["constraints"]["children"][$this->tbl_name];
            usort($constraints, function ($a, $b) {
                return strcmp(str_replace("_", "A", $a->parent), str_replace("_", "A", $b->parent));
            });
            foreach ($constraints as $constraint) {
                $table->addAction("___child_table___$constraint->table.$constraint->column", true, $constraint->table);
            }
        }

        $this->setTableColumns($table, $dbconn, $sql, null, $this->tbl_name);
        // lettura dal database delle righe che andranno a popolare la tabella
        if (!$table->loadRows()) {
            $this->showDBError($table->recordset->dbConnection);
        }

        $dbconn->disconnect();
        return $table;
    }

    //*****************************************************************************

    /**
     * @return waLibs\waForm
     */
    function myGetForm() {

        $form = parent::getForm();

        $ctrl = $form->addSelectTypeahead("typeahead_table_name", "Table quick search", false);
        $ctrl->list = $this->sessionData["tablenames"];

        $form->getInputValues();
        if ($form->isToUpdate()) {
            $this->redirect("?tbl_name=" . $form->typeahead_table_name);
        }

        return $form;
    }

    //*****************************************************************************

    /**
     * @return waLibs\waForm
     */
    function getFkTooltipSpace() {

        return '';
    }

    //*****************************************************************************
}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();
