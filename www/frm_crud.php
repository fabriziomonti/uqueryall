<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

    /**
     *
     * @var waLibs\waForm
     */
    var $form;
    var $tbl_name;
    var $pkColumn;

    //**************************************************************************
    function __construct() {
        parent::__construct();
        $this->tbl_name = $_GET["tbl_name"];

        $this->createForm();

        if ($this->form->isToUpdate()) {
            $this->updateRecord();
        } elseif ($this->form->isToDelete()) {
            $this->myDeleteRecord();
        } else {
            $this->showPage();
        }
    }

    //*****************************************************************************

    /**
     * mostra
     * 
     * costruisce la pagina contenente il form e la manda in output
     * @return void
     */
    function showPage() {
        $this->addItem($this->tbl_name, "title");
        $this->addItem($this->form);
        $this->show();
    }

    //***************************************************************************
    function createForm() {
        $this->form = $this->getForm();
        $this->form->recordset = $this->getMyRecordset();
        $record = $this->form->recordset->records[0];

        $readonly = stripos($this->user->env, "local") !== 0;
        $this->setFormFields($readonly);

        $this->form_submitButtons($this->form, $readonly, $record ? !$readonly : false);
        $this->form->getInputValues();
    }

    //***************************************************************************

    /**
     * -
     *
     * @return waLibs\waRecordset
     */
    function getMyRecordset() {
        $dbconn = $this->getDBConnection();
        $this->pkColumn = $this->getPrimaryKey($this->tbl_name);
        $id = $_GET[$this->pkColumn];
        $schema = $this->getCurrentSchema();
        $sql = "SELECT " . $this->getOrderedTableColumns($dbconn, $this->tbl_name) .
                " FROM $schema.$this->tbl_name" .
                " WHERE $this->pkColumn=" . $this->getPrimaryKeySqlValue($dbconn, $this->tbl_name, $id);

        $recordset = $this->getRecordset($sql, $dbconn, $id ? 1 : 0);
        if ($id && !$recordset->records) {
            $this->showMessage("Record not found", "Record not found", false, true);
        }

        return $recordset;
    }

    //***************************************************************************
    function updateRecord() {
        // controlli obbligatorieta' e formali
        $this->checkMandatory($this->form);

        $record = $this->form->recordset->records[0];
        if (!$record) {
            $record = $this->form->recordset->add();
        } else {
            $this->checkLockViolation($this->form);
        }

        // maialata
        $this->doWhatOracleWantNotToDo();
        $this->form->save();
        $this->saveRecordset($record->recordset);

        $this->response();
    }

    //***************************************************************************
    function myDeleteRecord() {

        // maialata
        $this->doWhatOracleWantNotToDo();
        
        $record = $this->form->recordset->records[0];
        $record->retired = $record->value($this->pkColumn);
        $this->saveRecordset($record->recordset);
        $this->response();
        
    }

    //***************************************************************************
    function doWhatOracleWantNotToDo() {

        // maialata: poichè oracle non ci fornisce il nome delle tabelle dei campi
        // e la chiave primaria della tabella, forziamo il nome tabella di tutti 
        // i campi e aggiungiamo la chiave primaria ai metadati del record
        $schema = $this->getCurrentSchema();
        foreach ($this->form->recordset->columns as &$column) {
            $column["table"] = $this->tbl_name;
            $column["tableAbsName"] = "$schema.$this->tbl_name";
        }

        $this->form->recordset->columns[$this->pkColumn]["primaryKey"] = 1;
        
    }

    //*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
