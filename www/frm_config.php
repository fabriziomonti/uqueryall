<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;

	//**************************************************************************
	function __construct() {
		parent::__construct();

		$this->createForm();

		if ($this->form->isToUpdate()) {
			$this->updateRecord();
		} else {
			$this->showPage();
		}
	}

	//*****************************************************************************

	/**
	 * mostra
	 * 
	 * costruisce la pagina contenente il form e la manda in output
	 * @return void
	 */
	function showPage() {
		$this->addItem("Preferences", "title");
		$this->addItem($this->form);
		$this->show();
	}

	//***************************************************************************
	function createForm() {

		$this->form = $this->getForm();

		$ctrl = $this->form->addBoolean("navigation_by_windows", "Navigation by windows", false);
		$ctrl->dbBound = false;
		$ctrl->value = $this->userPreferences["navigation_by_windows"];

		$ctrl = $this->form->addSelect("max_table_rows", "Max table rows", false, true);
		$ctrl->dbBound = false;
		$ctrl->emptyRow = false;
		for ($i = 10; $i <= 100; $i += 10)
			$ctrl->list[$i] = $i;
		$ctrl->value = $this->userPreferences["max_table_rows"];

		$ctrl = $this->form->addSelect("table_actions", "Table actions", false, true);
		$ctrl->dbBound = false;
		$ctrl->emptyRow = false;
		$ctrl->list = array("watable_actions_left" => "left", "watable_actions_context" => "right click", "watable_actions_context_left_click" => "left click", "watable_actions_right" => "right");
		$ctrl->value = $this->userPreferences["table_actions"];

		$this->form_submitButtons($this->form, false, false);

		$this->form->getInputValues();
	}

	//***************************************************************************
	function updateRecord() {
		$this->checkMandatory($this->form);

		// blob della configurazione da mandare via cookie al browser
		$prefs["navigation_by_windows"] = $this->form->navigation_by_windows;
		$prefs["max_table_rows"] = $this->form->max_table_rows;
		$prefs["table_actions"] = $this->form->table_actions;
		$cookieName = urlencode($this->name . "_" . base64_encode($this->user->username) . "_prefs");
		$cookieVal = base64_encode(serialize($prefs));
                list($domain, $port) = explode(":", $this->domain);
		setcookie($cookieName, $cookieVal, mktime(0, 0, 0, date('n'), date('j'), date('Y') + 10), "$this->httpwd", $domain);

		$this->response();
	}

	//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
