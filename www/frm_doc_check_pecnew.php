<?php

include "doc_check.class.php";

//*****************************************************************************
class page extends doc_check {


    //**************************************************************************
    function __construct() {
        
        $this->page_title = "Check PecNew EcmLoader results";
        $this->main_dir = "/InboundFlows/Filenet/Smistadoc/ErroriLogOptimo/PecNew";
        
        parent::__construct();
    }
    
//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
