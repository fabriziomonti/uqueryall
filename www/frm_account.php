<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

    /**
     *
     * @var waLibs\waForm
     */
    var $form;

    //**************************************************************************
    function __construct() {
        parent::__construct();

        $this->createForm();

        if ($this->form->isToUpdate()) {
            $this->updateRecord();
        } elseif ($this->form->isToDelete()) {
            $this->deleteAccount();
        } else {
            $this->showPage();
        }
    }

    //*****************************************************************************

    /**
     * mostra
     * 
     * costruisce la pagina contenente il form e la manda in output
     * @return void
     */
    function showPage() {
        $this->addItem("Account", "title");
        $this->addItem($this->form);
        $this->show();
    }

    //***************************************************************************
    function createForm() {

        $this->form = $this->getForm();

        $this->form->openTab("personal", "Personal");
            $this->form->addText("username", "Username", true)->value = $this->user->username;
            $this->form->addPassword("new_password", "New password");
            $this->form->addPassword("new_password_confirm", "New password confirm");
        $this->form->closeTab();
        
        $this->form->openTab("doc_check_params", "Doc check params");
            $this->form->openFrame("doc_check_ssh", "SSH");
                $this->form->addText("doc_check_remote_host", "Host")->value = $this->user->doc_check_remote_host;
                $this->form->addText("doc_check_remote_username", "Username")->value = $this->user->doc_check_remote_username;
                $this->form->addPassword("doc_check_remote_password", "Password")->value = $this->user->doc_check_remote_password;
            $this->form->closeFrame();
            $this->form->openFrame("doc_check_mailbox", "Mailbox");
                $this->form->addText("doc_check_mailbox_host", "Host")->value = $this->user->doc_check_mailbox_host;
                $this->form->addText("doc_check_mailbox_username", "Username")->value = $this->user->doc_check_mailbox_username;
                $this->form->addPassword("doc_check_mailbox_password", "Password")->value = $this->user->doc_check_mailbox_password;
            $this->form->closeFrame();
            $this->form->openFrame("doc_check_kofax", "Kofax");
                $this->form->addText("doc_check_kofax_host", "Host")->value = $this->user->doc_check_kofax_host;
                $this->form->addText("doc_check_kofax_username", "Username")->value = $this->user->doc_check_kofax_username;
                $this->form->addPassword("doc_check_kofax_password", "Password")->value = $this->user->doc_check_kofax_password;
            $this->form->closeFrame();
        $this->form->closeTab();
            
        $this->form->openTab("filenet_params", "FileNet params");
            $this->form->openFrame("filenet_prod_params", "PROD");
                $this->form->addText("filenet_prod_endpoint", "Endpoint")->value = $this->user->filenet_prod_endpoint;
                $this->form->addText("filenet_prod_username", "Username")->value = $this->user->filenet_prod_username;
                $this->form->addPassword("filenet_prod_password", "Password")->value = $this->user->filenet_prod_password;
            $this->form->closeFrame();
            $this->form->openFrame("filenet_coll_params", "COLL");
                $this->form->addText("filenet_coll_endpoint", "Endpoint")->value = $this->user->filenet_coll_endpoint;
                $this->form->addText("filenet_coll_username", "Username")->value = $this->user->filenet_coll_username;
                $this->form->addPassword("filenet_coll_password", "Password")->value = $this->user->filenet_coll_password;
            $this->form->closeFrame();
        $this->form->closeTab();
            
        $this->form->openTab("liquidump_params", "LiquiDump params");
            $this->form->addText("liquidump_remote_host", "Host")->value = $this->user->liquidump_remote_host;
            $this->form->addText("liquidump_remote_username", "Username")->value = $this->user->liquidump_remote_username;
            $this->form->addPassword("liquidump_remote_password", "Password")->value = $this->user->liquidump_remote_password;
            $this->form->addTextArea("liquidump_remote_java_env", "Java env")->value = $this->user->liquidump_remote_java_env;
        $this->form->closeTab();
            
        $this->form_submitButtons($this->form, false, false);
        $this->form->getInputValues();
    }

    //***************************************************************************
    function updateRecord() {
        $this->checkMandatory($this->form);

        if ($this->form->new_password) {
            if ($this->form->new_password != $this->form->new_password_confirm) {
                $this->showMessage("Passwords don't match", "Passwords don't match");
            }
            $this->user->password_encrypted = $this->oneWayEncrypt($this->form->new_password);
        }

        $this->user->doc_check_remote_host = $this->form->doc_check_remote_host;
        $this->user->doc_check_remote_username = $this->form->doc_check_remote_username;
        $this->user->doc_check_remote_password = $this->form->doc_check_remote_password;
        $this->user->doc_check_mailbox_host = $this->form->doc_check_mailbox_host;
        $this->user->doc_check_mailbox_username = $this->form->doc_check_mailbox_username;
        $this->user->doc_check_mailbox_password = $this->form->doc_check_mailbox_password;
        $this->user->doc_check_kofax_host = $this->form->doc_check_kofax_host;
        $this->user->doc_check_kofax_username = $this->form->doc_check_kofax_username;
        $this->user->doc_check_kofax_password = $this->form->doc_check_kofax_password;
        $this->user->filenet_prod_endpoint = $this->form->filenet_prod_endpoint;
        $this->user->filenet_prod_username = $this->form->filenet_prod_username;
        $this->user->filenet_prod_password = $this->form->filenet_prod_password;
        $this->user->filenet_coll_endpoint = $this->form->filenet_coll_endpoint;
        $this->user->filenet_coll_username = $this->form->filenet_coll_username;
        $this->user->filenet_coll_password = $this->form->filenet_coll_password;
        $this->user->liquidump_remote_host = $this->form->liquidump_remote_host;
        $this->user->liquidump_remote_username = $this->form->liquidump_remote_username;
        $this->user->liquidump_remote_password = $this->form->liquidump_remote_password;
        $this->user->liquidump_remote_java_env = $this->form->liquidump_remote_java_env;
        
        $this->saveUserData();
        
        $this->response();
    }

    //***************************************************************************
    function deleteAccount() {

        unlink("$this->directoryDoc/accounts/" . $this->user->username);
        $this->sessionData = array();
        $this->response();
    }

    //*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
