<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

    /**
     *
     * @var waLibs\waForm
     */
    var $form;

    //**************************************************************************
    function __construct() {
        parent::__construct(false);

        $this->createForm();

        if ($this->form->isToUpdate()) {
            $this->doLogin();
        } else {
            $this->showPage();
        }
    }

    //*****************************************************************************

    /**
     * showPage
     * 
     * costruisce la pagina contenente il form e la manda in output
     * @return void
     */
    function showPage() {
        $this->addItem("$this->title Login", "title");
        $this->addItem($this->form);
        $this->show();
    }

    //***************************************************************************
    function createForm() {
        $this->form = $this->getForm();
        $this->form->addText("username", "Username", false, true);
        $this->form->addPassword("password", "Password", false, true);
        $this->form->addBoolean("registration", "Nuovo utente");
        $this->form->addPassword("password_confirm", "Conferma Password", false, false)->visible = false;
        $this->form->labels["password_confirm"]->visible = false;

        new waLibs\waButton($this->form, 'cmd_submit', ' Login ');

        $this->form->getInputValues();
    }

    //***************************************************************************
    function doLogin() {
        $this->checkMandatory($this->form);

        if ($this->form->registration) {
            $this->doRegistration();
        }

        if (!file_exists("$this->directoryDoc/accounts/" . $this->form->username)) {
            $this->showMessage("Utente non riconosciuto", "Login non permesso: utente non riconosciuto");
        }
        $user_data_encrypted = file_get_contents("$this->directoryDoc/accounts/" . $this->form->username);
        $password_encrypted = $this->oneWayEncrypt($this->form->password);
        $user_data_serialized = $this->twoWayDecrypt($user_data_encrypted, $password_encrypted);
        if (!$user_data_serialized) {
            $this->showMessage("Utente non riconosciuto", "Login non permesso: utente non riconosciuto");
        }
        $user_data = unserialize($user_data_serialized);
        if ($user_data->password_encrypted !== $password_encrypted) {
            // probabilmente è inutile, ma finchè siamo qui...
            $this->showMessage("Utente non riconosciuto", "Login non permesso: utente non riconosciuto");
        }
        $this->user = $user_data;

        $this->redirect($this->startPage);
    }

    //***************************************************************************
    function doRegistration() {

        if ($this->form->password != $this->form->password_confirm) {
            $this->showMessage("Password non corrispondenti", "Password non corrispondenti");
        }
        if (file_exists("$this->directoryDoc/accounts/" . $this->form->username)) {
            $this->showMessage("Account già esistente", "Account già esistente");
        }
//		if (!$this->isPasswordStrong($this->form->password)) {
//			$this->showMessage("Password troppo debole", "Password troppo debole: min 8 caratteri, una maiuscola, una minuscola, un numero, un carattere speciale");
//		}

        $user_data = new stdClass();
        $user_data->username = $this->form->username;
        $user_data->password_encrypted = $this->oneWayEncrypt($this->form->password);
        $user_data->db_accounts = [];
        $user_data_encrypted = $this->twoWayEncrypt(serialize($user_data), $user_data->password_encrypted);
        file_put_contents("$this->directoryDoc/accounts/" . $this->form->username, $user_data_encrypted);
    }

    //***************************************************************************
    // verifica che una password sia strong (non utilizzato)
    //***************************************************************************
    function isPasswordStrong($password) {

        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number = preg_match('@[0-9]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);

        return $uppercase && $lowercase && $number && $specialChars && strlen($password) >= 8;
    }

//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
$page = new page();
