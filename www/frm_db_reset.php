<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

    /**
     *
     * @var waLibs\waForm
     */
    var $form;
    
    var $timeStart;

    //**************************************************************************
    function __construct() {
        parent::__construct();

        if (!$this->user->env || count($this->user->db_accounts) <= 1 || !$this->sessionData["tablenames"]["CC_CLAIM"]) {
            $this->showMessage("Not available", "Operation not available on current database", false, true);
        }

        $this->createForm();

        if ($this->form->isToUpdate()) {
            $this->reset();
        } else {
            $this->showPage();
        }
    }

    //*****************************************************************************

    /**
     * mostra
     * 
     * costruisce la pagina contenente il form e la manda in output
     * @return void
     */
    function showPage() {
        $this->addItem("DB reset", "title");
        $msg = "<p>This operation will completely destroy your database <b>" . $this->user->env .  "</b>!</p>\n" .
                "<p>Are you <b>absolutely sure</b>???</p>\n" .
                "<p>If you do, after the reset you must" .
                "<ul>\n" .
                "<li>start the server and wait for the <i>ClaimCenter Ready</i> notification\n" .
                "<li>stop the server\n" .
                "<li>execute a base import from any of exisiting databases\n" .
                "</ul></p>\n";
        $this->addItem($msg, "testo_libero");
        $this->addItem($this->form);
        $this->show();
    }

    //***************************************************************************
    function createForm() {

        $this->form = $this->getForm();

        $this->form->addText("dest_db", "Database to reset", true)->value = $this->user->env;

        $this->form_submitButtons($this->form, false, false, "Yes, I want to reset the db " . $this->user->env .  " !");
        $this->form->getInputValues();
    }

    //***************************************************************************
    function reset() {

        $liquidump_bin_path = dirname(APPL_LIQUIDUMP_CMD);

        $destAccount = $this->user->db_accounts[$this->user->env];
        $liquireset_conf .= "\ndbHost=$destAccount->HOST\n" .
                "dbPort=$destAccount->PORT\n" .
                "dbSID=$destAccount->SID\n" .
                "dbPDB=$destAccount->PDB\n" .
                "dbUser=$destAccount->USERNAME\n" .
                "dbPassword=$destAccount->PASSWORD\n" .
                "dbSchema=$destAccount->SCHEMA\n" .
                "dryRun=false\n";
        file_put_contents($this->pathAdjust("$liquidump_bin_path/LiquiReset.conf"), $liquireset_conf);

        set_time_limit(0);
        $this->logStart("Reset of database " . $this->user->env);
        
        $this->execute(APPL_LIQUIDUMP_CMD . " LiquiReset", $liquidump_bin_path);

        $this->logEnd();
    }

//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
