<?php

if (!defined('__CONFIG_VARS')) {
    define('__CONFIG_VARS', 1);

    $__DIR__ = __DIR__;

    // file contenente i veri valori di configurazione, che non devono finire
    // su git, mentre il presente file rimane per documentazione
    @include "$__DIR__/myconfig.inc.php";

    // file contenente i parametri della versione (che cambiano e devono essere
    // inviati al server, a differenza di questi)
    include "$__DIR__/versionconfig.inc.php";

    @define('APPL_DOMAIN', $_SERVER['HTTP_HOST']);
    @define('APPL_DIRECTORY', '');
    @define('APPL_TMP_DIRECTORY', "$__DIR__/web_files/tmp");
    @define('APPL_DOC_DIRECTORY', "$__DIR__/web_files");
    @define('APPL_NAME', 'uqueryall');
    @define('APPL_TITLE', 'uQueryAll');

    @define("WAMODULO_EXTENSIONS_DIR", "$__DIR__/wamodulo_ext"); // directory estensioni classe modulo

    @define("APPL_ONE_WAY_ENCRYPT_ALGO", "sha512"); // metodo encryption distruttivo
    @define("APPL_TWO_WAY_ENCRYPT_METHOD", "AES-256-CBC"); // metodo encryption non distruttivo
    @define("APPL_TWO_WAY_ENCRYPT_IV_LEN", 16); // lunghezza dell'initialization-vector per encryption non distruttiva

    @define("APPL_DB_DRIVER", "ASAS"); // driver da usare per l'accesso al db (ASAS / ORACLE [non va xche Galetti...])
    @define("APPL_LIQUIDUMP_CMD", "$__DIR__/liquidump_bin/liquidump.sh"); // il cmd/sh per lanciare liquidump
    @define("APPL_UNZIP_CMD", "unzip"); // comando per unzippare
    
    @define("APPL_VENDOR_DIR", "$__DIR__/vendor");

    set_include_path(get_include_path() . PATH_SEPARATOR . APPL_VENDOR_DIR);
} //  if (!defined('__CONFIG_VARS'))
