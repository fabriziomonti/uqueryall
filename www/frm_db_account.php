<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

    /**
     *
     * @var waLibs\waForm
     */
    var $form;
    var $env;

    //**************************************************************************
    function __construct() {
        parent::__construct();

        $this->env = str_replace("___blank_placeholder__", " ", $_GET["ENV_ID"]);

        $this->createForm();

        if ($this->form->isToUpdate()) {
            $this->updateDbAccount();
        } elseif ($this->form->isToDelete()) {
            $this->deleteDbAccount();
        } else {
            $this->showPage();
        }
    }

    //*****************************************************************************

    /**
     * mostra
     * 
     * costruisce la pagina contenente il form e la manda in output
     * @return void
     */
    function showPage() {
        $this->addItem("DB Account", "title");
        $this->addItem($this->form);
        $this->show();
    }

    //***************************************************************************
    function createForm() {

        $this->form = $this->getForm();

        $db_account = $this->user->db_accounts[$this->env];
        $this->form->addText("ENV", "Name", false, true)->value = $this->env;
        $this->form->addText("HOST", "Host", false, true)->value = $db_account->HOST;
        $this->form->addText("PORT", "Port", false, true)->value = $db_account->PORT;
        $this->form->addText("SID", "SID")->value = $db_account->SID;
        $this->form->addText("PDB", "PDB")->value = $db_account->PDB;
        $this->form->addText("SCHEMA", "Schema", false, true)->value = $db_account->SCHEMA;
        $this->form->addText("USERNAME", "Username", false, true)->value = $db_account->USERNAME;
        $this->form->addPassword("PASSWORD", "Password", false, true)->value = $db_account->PASSWORD;

        $canDelete = $this->user->db_accounts[$this->env] && $this->env != $this->user->env;
        $this->form_submitButtons($this->form, false, $canDelete);
        $this->form->getInputValues();
    }

    //***************************************************************************
    function updateDbAccount() {
        $this->checkMandatory($this->form);

        $dbParams = new \waLibs\waDBPparams();
        $dbParams->DBTYPE = $this->getDBType();
        $dbParams->HOST = $this->form->HOST;
        $dbParams->PORT = $this->form->PORT;
        $dbParams->DBNAME = $this->getDBName($this->form->SID, $this->form->PDB);
        $dbParams->USERNAME = $this->form->USERNAME;
        $dbParams->PASSWORD = $this->form->PASSWORD;
        $this->getDBConnection($dbParams);
        $dbParams = (object) (array) $dbParams;
        $dbParams->SID = $this->form->SID;
        $dbParams->PDB = $this->form->PDB;
        $dbParams->SCHEMA = $this->form->SCHEMA;
        $this->user->db_accounts[$this->form->ENV] = $dbParams;
        $this->saveUserData();

        $this->response();
    }

    //***************************************************************************
    function deleteDbAccount() {

        $canDelete = $this->user->db_accounts[$this->env] && $this->env != $this->user->env;
        if (!$canDelete) {
            $this->showMessage("Database in uso", "$this->env : database in uso, impossibile eliminarlo.");
        }

        unset($this->user->db_accounts[$this->env]);
        $this->saveUserData();

        $this->response();
    }

    //*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
