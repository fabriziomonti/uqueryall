<?php
if (!defined('__VERSIONCONFIG_VARS'))
{
	define('__VERSIONCONFIG_VARS',1);
	
	define('APPL_REL', 							'3.2.10');
	define('APPL_REL_DATE', 					mktime(0,0,0, 10, 25, 2024));

	
} //  if (!defined('__VERSIONCONFIG_VARS'))
