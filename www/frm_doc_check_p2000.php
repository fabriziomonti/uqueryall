<?php

include "doc_check.class.php";

//*****************************************************************************
class page extends doc_check {


    //**************************************************************************
    function __construct() {
        
        $this->page_title = "Check P2000 EcmLoader results";
        $this->main_dir = "/InboundFlows/Filenet/Smistadoc/ErroriLogOptimo/P2000";
        $this->log_name = "infoP2000.log";
        
        parent::__construct();
    }
    
//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
