<?php

//******************************************************************************
include "uqueryall.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends uqueryall {

    var $view_id;
    var $view_data;

    //*****************************************************************************
    function __construct() {
        parent::__construct();
        $this->view_id = $_GET["view_id"];

        $file_name = "$this->directoryDoc/views/$this->view_id." . $this->user->username;
        if (!file_exists($file_name)) {
            $this->redirect($this->startPage);
        }

        $this->view_data = unserialize(file_get_contents("$this->directoryDoc/views/$this->view_id." . $this->user->username));

        $this->addItem($this->getMenu());
        $this->addItem("view " . $this->view_data->view_title, "title");
        $this->addItem($this->myGetTable());
        $this->show();
    }

    //*****************************************************************************

    /**
     * @return waLibs\waTable
     */
    function myGetTable() {

        // creazione della tabella
        $dbconn = $this->getDBConnection();
        $sql = $this->flattenSql($this->view_data->view_sql);

        $table = parent::getTable($sql);
        $table->formPage = "frm_crud.php?tbl_name=$this->view_id";

        $table->removeAction("New");
        $table->removeAction("Edit");
        $table->removeAction("Delete");

        $table->addAction("Edit_view", false, "Edit");
        $table->addAction("Delete_view", false, "Delete");

        $rs = $this->getRecordset($sql, $dbconn, 0);
        $this->setTableColumns($table, $dbconn, $sql);

        // cerchiamo di stabilire il nome della tabella del db di ogni colonna; se non 
        // ci riusciamo la colonna non avrà ordinamento e filtro
        $this->setColumnDbTable($table, $dbconn, $sql);

        // lettura dal database delle righe che andranno a popolare la tabella
        if (!$table->loadRows()) {
            $this->showDBError($table->recordset->dbConnection);
        }

        $dbconn->disconnect();
        return $table;
    }

    //*****************************************************************************
    function setColumnDbTable(waLibs\waTable $table, waLibs\waDBConnection $dbconn, $sql) {

        $sql_elems = $dbconn->splitSql($sql);
        $select_elems = explode(",", $sql_elems->select);
        foreach ($select_elems as &$select_elem) {
            if (stripos(strtoupper($select_elem), "SELECT ") === 0) {
                $select_elem = substr($select_elem, strlen("SELECT "));
            }
            $select_elem = trim($select_elem);
        }

        foreach ($table->columns as $column) {
            $found_idx = false;
            foreach ($select_elems as $idx => $select_elem_2) {
                if (stripos($select_elem_2, " AS " . $column->name)) {
                    list($dbtable_and_dbfield, $rest) = explode(" AS ", strtoupper($select_elem_2), 2);
                    $column->aliasOf = $dbtable_and_dbfield;
                    $found_idx = $idx;
                    break;
                } elseif (stripos($select_elem_2, "." . $column->name)) {
                    $column->aliasOf = $select_elem_2;
                    $found_idx = $idx;
                    break;
                }
            }
            if ($found_idx !== false) {
                array_splice($select_elems, $found_idx, 1);
            } else {
                $column->sort = $column->filter = false;
            }
        }
    }

    //*****************************************************************************
    function flattenSql($sql) {

        $sql = $this->cleanUpViewSql($this->view_data->view_sql);
        $sql = trim($sql);
        $sql = str_replace("\r", "", $sql);
        $sql = str_replace("\n", " ", $sql);
        $sql = str_replace("\t", " ", $sql);
        while (strpos($sql, "  ")) {
            $sql = str_replace("  ", " ", $sql);
        }

        return $sql;
    }

    //*****************************************************************************
}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();
