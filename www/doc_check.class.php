<?php

include "uqueryall.inc.php";

//*****************************************************************************
class doc_check extends uqueryall {

    /**
     *
     * @var waLibs\waForm
     */
    var $form;

    var $page_title = "";
    var $main_dir = "";
    var $log_name = "";
    
    //**************************************************************************
    function __construct() {

        parent::__construct();

        if (!$this->user->env || count($this->user->db_accounts) <= 1 || !$this->sessionData["tablenames"]["CC_CLAIM"]) {
            $this->showMessage("Not available", "Operation not available on current database", false, true);
        }

        $this->createForm();

        if ($this->form->isToUpdate()) {
            set_time_limit(0);
            $this->check();
        } else {
            $this->showPage();
        }
    }

    //*****************************************************************************

    /**
     * mostra
     * 
     * costruisce la pagina contenente il form e la manda in output
     * @return void
     */
    function showPage() {
        $this->addItem($this->page_title, "title");
        $this->addItem($this->form);
        $this->show();
    }


    //***************************************************************************
    function createForm() {

        $this->form = $this->getForm();

        $this->form->openTab("main", "Main");
            $this->form->addText("dest_db", "Destination database", true)->value = $this->user->env;

            $this->form->openFrame("depth", "Depth");
                $this->form->addInteger("depth_days", "Days")->value = 1;
                $this->form->addInteger("depth_hours", "Hours")->value = 0;
                $this->form->addInteger("depth_minutes", "Minutes")->value = 0;
            $this->form->closeFrame();
        $this->form->closeTab();

        $this->form->openTab("ssh", "SSH");
            $this->form->addText("remote_host", "Remote host", false, true)->value = $this->user->doc_check_remote_host;
            $this->form->addText("remote_username", "Remote username", false, true)->value = $this->user->doc_check_remote_username;
            $this->form->addPassword("remote_password", "Remote password", false, true)->value = $this->user->doc_check_remote_password;
        $this->form->closeTab();
        
        $this->form_submitButtons($this->form, false, false, "Check");
        $this->form->getInputValues();
    }

    //***************************************************************************
    function check() {

        if (stripos($this->main_dir, "PecNew")) {
            return $this->checkPecNew();
        }
        
        $this->checkMandatory($this->form);

        $sftp = $this->getRemoteConnection();
        $dirs = $this->getRelevantResultDirs($sftp);

        $messages = [];
        $chunkLimit = 100;
        foreach ($dirs as $dir) {
            if (!$dir) {
                continue;
            }
            $timestamp = substr(basename($dir), 0, 17);
            $messages[] = "<hr>Elaborazione file " . basename($dir) . "/$this->log_name<br/><br/>";
            $lapMessages = [];
            $dest = APPL_TMP_DIRECTORY . "/$timestamp.$this->log_name";
            $sftp->get("$this->main_dir/$dir/$this->log_name", $dest);
            $rowNr = 0;
            $chunk = [];
            $fp = fopen($dest, "rb");
            while ($row = fgets($fp)) {
                if (count($chunk) == $chunkLimit) {
                    $lapMessages = array_merge($lapMessages, $this->checkChunk(0, $chunk));
                    $chunk = [];
                }
                $elems = explode("Barcode: '", $row, 2);
                if (count($elems) == 2) {
                    $rowNr++;
                    list ($barcode, $rest) = explode("'", $elems[1], 2);
                    $chunk[] = [$barcode];
                }
            }
            if (count($chunk)) {
                $lapMessages = array_merge($lapMessages, $this->checkChunk(0, $chunk));
            }
            if ($lapMessages) {
                $messages = array_merge($messages, $lapMessages);
            } else {
                $messages[] = "Nessun errore rilevato; processate $rowNr righe";
            }
            fclose($fp);
            unlink($dest);
        }

        $sftp->disconnect();
        $this->showMessage("$this->page_title - Esito rilevamento", implode("<br/>", $messages), false, true);
        
    }

    //***************************************************************************
    function checkPecNew() {

        $this->checkMandatory($this->form);

        $sftp = $this->getRemoteConnection();
        $dirs = $this->getRelevantResultDirs($sftp);

        $messages = [];
        $chunkLimit = 100;
        foreach ($dirs as $dir) {
            if (!$dir) {
                continue;
            }
            $dir = basename($dir);
            $messages[] = "<hr>Elaborazione directory $dir<br/><br/>";
            $lapMessages = [];

            $command = "cd '$this->main_dir/$dir'; ls -1 *.xml";
            $out = $sftp->exec($command);
            $xmls = explode("\n", $out);
            $rowNr = 0;
            $chunk = [];
            foreach ($xmls as $xml) {
                if (!$xml || stripos($xml, "no such file")) {
                    continue;
                }
                if (count($chunk) == $chunkLimit) {
                    $lapMessages = array_merge($lapMessages, $this->checkChunk(0, $chunk));
                    $chunk = [];
                }
                $barcode = str_replace("-", "", str_replace(".xml", "", $xml));
                $rowNr++;
                $chunk[] = [$barcode];
            }
            if (count($chunk)) {
                $lapMessages = array_merge($lapMessages, $this->checkChunk(0, $chunk));
            }
            if ($lapMessages) {
                $messages = array_merge($messages, $lapMessages);
            } else {
                $messages[] = "Nessun errore rilevato; processate $rowNr righe";
            }
        }

        $sftp->disconnect();
        $this->showMessage("$this->page_title - Esito rilevamento", implode("<br/>", $messages), false, true);
        
    }

    //***************************************************************************
    /**
     * @return phpseclib3\Net\SFTP
     */
    function getRemoteConnection() {

        require ('autoload.php');
        $loader = new \Composer\Autoload\ClassLoader();
        $sftp = new phpseclib3\Net\SFTP($this->form->remote_host);   // Domain or IP
        if (!$sftp->login($this->form->remote_username, $this->form->remote_password)) {
            $this->showMessage("Remote login failed", "Remote login failed", false, true);
        }

        $sftp->setTimeout(120);
        return $sftp;
    }

    //***************************************************************************
    function getRelevantResultDirs(phpseclib3\Net\SFTP $sftp) {

        $time_depth = ($this->form->depth_days * 24 * 60) + ($this->form->depth_hours * 60) + $this->form->depth_minutes;
        $command = "cd $this->main_dir; find . -mindepth 1 -type d -mmin -$time_depth";
        $out = $sftp->exec($command);
        if (!$out) {
            if ($sftp->isTimeout()) {
                $this->showMessage("Timeout", "Timeout esecuzione - check non valido", false, true);
            }
            $this->showMessage("Nessun log rilevato", "Nessun log rilevato", false, true);
        }

        $dirs = explode("\n", $out);
        return $dirs;
    }

    //***************************************************************************
    function getXLSXRows($fileToRead) {

        $files = glob(APPL_VENDOR_DIR . "/xlsx-reader/lib/*.php");
        foreach ($files as $file) {
            include_once $file;
        }

        $ret = [];
        $reader = new Aspera\Spreadsheet\XLSX\Reader();
        $reader->open($fileToRead);
        foreach ($reader as $row) {
            if (!is_array($row) || !$row[0]) {
                break;
            }
            $ret[] = $row;
        }

        $reader->close();
        return $ret;
    }

    //***************************************************************************
    function getCSVRows($fileToRead) {

        ini_set("auto_detect_line_endings", true);
        $fp = fopen($fileToRead, "rb");
        if (!$fp) {
            $this->showMessage("Errore in apertura file CSV", "Si è verificato un errore durante l'apertura del file CSV.");
        }

        $ret = [];
        while ($row = fgetcsv($fp)) {
            if (!is_array($row) || !$row[0]) {
                break;
            }
            $ret[] = $row;
        }

        fclose($fp);

        return $ret;
    }

    //***************************************************************************
    function getBarcodeColNr($row) {

        $barcodeColNr = 0;
        foreach ($row as $cellValue) {
            if ($cellValue == "BARCODE") {
                break;
            }
            $barcodeColNr++;
        }
        return $barcodeColNr;
    }

    //***************************************************************************
    function checkChunk($barcodeColNr, $chunk) {

        static $dbconn;
        static $schema;
        
        if (!$dbconn) {
            $dbconn = $this->getDBConnection();
            $schema = $this->getCurrentSchema();
        }        
        
        $messages = [];
        $map = [];
        $inClause = "";
        $comma = "";
        foreach ($chunk as $row) {
            $barcode = $row[$barcodeColNr];
            $map[$barcode] = $row;
            $inClause .= "$comma '$row[$barcodeColNr]'";
            $comma = ",";
        }

        $sql = "select value as barcode"
                . " from $schema.ccx_documentmetadataitem_ext"
                . " where key='Barcode'"
                . " and value in ($inClause)";
        $recordset = $this->getRecordset($sql, $dbconn);

        if (count($recordset->records) != count($chunk)) {
            // andiamo a cercare quelli che mancano
            foreach ($recordset->records as $record) {
                unset($map[$record->barcode]);
            }
            foreach ($map as $barcode => $row) {
                $messages[] = ($row["my_row_nr"] ? "riga $row[my_row_nr]: " : "") . "il barcode $barcode non è presente nel db Liquido.";
            }
        }

        return $messages;
    }

//*****************************************************************************
}

