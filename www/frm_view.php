<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	var $view_id;
	var $view_data;

	//**************************************************************************
	function __construct() {
		parent::__construct();

		if ($_GET["view_id"]) {
			$this->view_id = $_GET["view_id"];
			$this->view_data = unserialize(file_get_contents("$this->directoryDoc/views/$this->view_id." . $this->user->username));
		}
		
		$this->createForm();

		if ($this->form->isToUpdate()) {
			$this->updateRecord();
		} 
		elseif ($this->form->isToDelete()) {
			$this->myDeleteRecord();
		} 
		else {
			$this->showPage();
		}
	}

	//*****************************************************************************

	/**
	 * mostra
	 * 
	 * costruisce la pagina contenente il form e la manda in output
	 * @return void
	 */
	function showPage() {
		$this->addItem("View", "title");
		$this->addItem($this->form);
		$this->show();
	}

	//***************************************************************************
	function createForm() {

		$this->form = $this->getForm();

		$this->form->addText("view_title", "Title", false, true)->value = $this->view_data->view_title;
		$this->form->addTextArea("view_sql", "SQL", false, true)->value = $this->view_data->view_sql;
		
		$this->form_submitButtons($this->form, false, $this->view_id);
		$this->form->getInputValues();
	}

	//***************************************************************************
	function updateRecord() {
		$this->checkMandatory($this->form);

		// test della query
                $sql = $this->cleanUpViewSql($this->form->view_sql);
		$rs = $this->getRecordset($sql, null, 0);
		
		$out = new stdClass();
		$out->view_title = $this->form->view_title;
		$out->view_sql = $this->form->view_sql;
				
		$view_id = $this->view_id ? $this->view_id : uniqid();
		file_put_contents("$this->directoryDoc/views/$view_id." . $this->user->username, serialize($out));
		$this->sessionData["views"][$view_id] = $out->view_title;
		asort($this->sessionData["views"]);

		$this->response();
	}

	//***************************************************************************
	function myDeleteRecord() {
		
		unlink("$this->directoryDoc/views/$this->view_id." . $this->user->username);
		unset($this->sessionData["views"][$this->view_id]);
		$this->response();
	}

	//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page(); 
