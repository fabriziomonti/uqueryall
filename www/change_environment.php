<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

	//**************************************************************************
	function __construct() {
		parent::__construct();

		if ($_GET["env"]) {
			if (!$this->user->db_accounts[$_GET["env"]]) {
				$this->showMessage("Database non configurato", "$_GET[env] : database non configurato");
			}
			$this->user->env = $_GET["env"];
			$this->saveUserData();
			$this->redirect(base64_decode($_GET["redir"]));
		}

		unset($this->user->env);
		$this->saveUserData();
		$this->sessionData["tables"] = [];
		$this->sessionData["tablenames"] = [];
		$this->redirect("home.php");
	}

	//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
