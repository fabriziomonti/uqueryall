<?php

include "doc_check.class.php";

//*****************************************************************************
class page extends doc_check {


    //**************************************************************************
    function __construct() {
        
        $this->page_title = "Check StornoAssegni EcmLoader results";
        $this->main_dir = "/InboundFlows/Filenet/Smistadoc/ErroriLogOptimo/StornoAssegni";
        $this->log_name = "infoStornoAssegno.log";
        
        parent::__construct();
    }
    
//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
