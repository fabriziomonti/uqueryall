<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

    /**
     *
     * @var waLibs\waForm
     */
    var $form;
    var $tbl_name;

    //**************************************************************************
    function __construct() {
        parent::__construct();
        $this->tbl_name = $_GET["tbl_name"];

        $this->createForm();

        if ($this->form->isToUpdate()) {
            $this->updateRecord();
        } else {
            $this->showPage();
        }
    }

    //*****************************************************************************

    /**
     * mostra
     * 
     * costruisce la pagina contenente il form e la manda in output
     * @return void
     */
    function showPage() {
        $this->addItem("Import $this->tbl_name to " . $this->user->env, "title");
        $this->addItem($this->form);
        $this->show();
    }

    //***************************************************************************
    function createForm() {
        $this->form = $this->getForm();
        $readonly = false;

        $ctrl = $this->form->addSelect("source_db", "Source database", false, true);
        $ctrl->list = [];
        foreach ($this->user->db_accounts as $env => $db_account) {
            if ($env != $this->user->env) {
                $ctrl->list[$env] = "$env ($db_account->SCHEMA)";
            }
        }

        $this->form_submitButtons($this->form, false, false);
        $this->form->getInputValues();
    }

    //***************************************************************************
    function updateRecord() {
        // controlli obbligatorieta' e formali
        $this->checkMandatory($this->form);

        
        $source_db_account = $this->user->db_accounts[$this->form->source_db];
        $source_db_params = new \waLibs\waDBPparams();
        $source_db_params->DBTYPE = $this->getDBType();
        $source_db_params->HOST = $source_db_account->HOST;
        $source_db_params->PORT = $source_db_account->PORT;
        $source_db_params->DBNAME = $this->getDBName($source_db_account->SID, $source_db_account->PDB);
        $source_db_params->USERNAME = $source_db_account->USERNAME;
        $source_db_params->PASSWORD = $source_db_account->PASSWORD;
        $source_dbConn = $this->getDBConnection($source_db_params);
        $sql = "select * from " .  $this->user->db_accounts[$this->form->source_db]->SCHEMA . ".$this->tbl_name where 1=1";
        // hacking: prendiamo gli stessi filtri usati da watable
        if (is_array($_GET["watable_ff"]["waTable"])) {
            foreach($_GET["watable_ff"]["waTable"] as $idx => $fieldName) {
                $sql .= $this->getFilter($source_dbConn, $fieldName, $_GET["watable_fm"]["waTable"][$idx], $_GET["watable_fv"]["waTable"][$idx]);
            }
        }
        
        $source_rs = $this->getRecordset($sql, $source_dbConn);

        $dbconn = $this->getDBConnection();
        $schema = $this->getCurrentSchema();
        $sql = "select * from $schema.$this->tbl_name";
        $dest_rs = $this->getRecordset($sql, $dbconn, 0);
        
        // maialata: poichè oracle non ci fornisce il nome delle tabelle dei campi
        // forziamo il nome tabella di tutti i campi
        foreach ($dest_rs->columns as &$column) {
            $column["table"] = $this->tbl_name;
            $column["tableAbsName"] = "$schema.$this->tbl_name";
        }
        foreach($source_rs->records as $source_record ) {
            $dest_record = $dest_rs->add();
            foreach ($dest_rs->columns as $acolumn) {
                $dest_record->insertValue($acolumn['name'], $source_record->value($acolumn['name']));
            }
        }

        $this->saveRecordset($dest_rs);
        $this->response();
    }

    //***************************************************************************
    function getFilter(waLibs\waDBConnection $dbconn, $field, $operator, $value) {
    
        $retval = "";
        
        if (!$value) {
        }
        elseif ($operator == "lt") {
            $retval = "and $field < " . $dbconn->sqlString($value);
        }
        elseif ($operator == "le") {
            $retval = "and $field <= " . $dbconn->sqlString($value);
        }
        elseif ($operator == "eq") {
            $retval = "and $field = " . $dbconn->sqlString($value);
        }
        elseif ($operator == "ge") {
            $retval = "and $field >= " . $dbconn->sqlString($value);
        }
        elseif ($operator == "gt") {
            $retval = "and $field > " . $dbconn->sqlString($value);
        }
        elseif ($operator == "ne") {
            $retval = "and $field <> " . $dbconn->sqlString($value);
        }
        elseif ($operator == "sw") {
            $retval = "and $field like " . $dbconn->sqlString("$value%");
        }
        elseif ($operator == "like") {
            $retval = "and $field like " . $dbconn->sqlString("%$value%");
        }
        
        return $retval;
    }
    
    //*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
