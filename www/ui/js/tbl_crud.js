//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class({
    //-------------------------------------------------------------------------
    // extends
    Extends: uqueryall,

    //-------------------------------------------------------------------------
    // proprieta'

    //-------------------------------------------------------------------------
    //initialization  
    // la fa il parent

    //-------------------------------------------------------------------------
    event_onchange_waForm_typeahead_table_name: function () {
        this.form.submit();
    },

    //-------------------------------------------------------------------------
    action_waTable_Columns: function () {
        location.href = "tbl_table_info.php?tbl_name=" + this.getQSParam("tbl_name") + "&columns=1";
    },

    //-------------------------------------------------------------------------
    action_waTable_Constraints: function () {
        location.href = "tbl_table_info.php?tbl_name=" + this.getQSParam("tbl_name") + "&constraints=1";
    },

    //-------------------------------------------------------------------------
    action_waTable_Indexes: function () {
        location.href = "tbl_table_info.php?tbl_name=" + this.getQSParam("tbl_name") + "&indexes=1";
    },

    //-------------------------------------------------------------------------
    action_waTable_Foreign_keys: function () {
        location.href = "tbl_table_info.php?tbl_name=" + this.getQSParam("tbl_name") + "&foreignkeys=1";
    },

    //-------------------------------------------------------------------------
    action_waTable_Ref_up: function () {
        this.openPage("ref_up.php?tbl_name=" + this.getQSParam("tbl_name"));
    },

    //-------------------------------------------------------------------------
    action_waTable_Ref_down: function () {
        this.openPage("ref_down.php?tbl_name=" + this.getQSParam("tbl_name"));
    },

    //-------------------------------------------------------------------------
    action_waTable_RetiredToo: function () {
        location.href = this.removeQSParam("retiredtoo") + "&retiredtoo=1";
    },

    //-------------------------------------------------------------------------
    action_waTable_NoRretired: function () {
        location.href = this.removeQSParam("retiredtoo");
    },

    //-------------------------------------------------------------------------
    action_waTable_Import: function () {
        this.openPage("frm_import.php" + document.location.search);
    },

    //-------------------------------------------------------------------------
    fkLink: function (tbl_name, idName, idValue) {
        window.open("tbl_crud.php?tbl_name=" + tbl_name + "&watable_ff[waTable][0]=" + idName + "&watable_fm[waTable][0]=eq&watable_fv[waTable][0]=" + idValue);
//		this.openPage("frm_crud.php?tbl_name=" + tbl_name + "&id=" + id);
    },

    //-------------------------------------------------------------------------
    fkLinkMouseOver: function (a, tbl_name, idName, idValue) {
        try {

            jQuery.ajax({
                url: "ws_fk_description.php?tbl_name=" + tbl_name + "&id_name=" + idName + "&id_value=" + idValue,
                type: 'GET',
                dataType: 'json',
                timeout: 20000,
                success: function (response, textStatus, jqXHR) {
                    if (response.result) {
                        jQuery(".waapplication_fk_tooltip").html(response.result);
                        jQuery(".waapplication_fk_tooltip").css("top", jQuery(a).position().top + 10);
                        jQuery(".waapplication_fk_tooltip").css("left", jQuery(a).position().left + 10);
                        jQuery(".waapplication_fk_tooltip").show();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                }
            });

        } catch (e) {
        }
    },

    //-------------------------------------------------------------------------
    fkLinkMouseOut: function () {
        jQuery(".waapplication_fk_tooltip").hide();
    }

});

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
