//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class({
//-------------------------------------------------------------------------
// extends
    Extends: uqueryall,
    //-------------------------------------------------------------------------
    // proprieta'

    //-------------------------------------------------------------------------
    //initialization
    initialize: function () {
        this.parent();
        
        var tabs = jQuery("ul#waForm_tabs_declaration li");
        jQuery(tabs[1]).hide();
        jQuery(tabs[2]).hide();

    },

    //-------------------------------------------------------------------------
    event_onclick_waForm_remote: function () {

        var visible = this.form.controls.remote.get();
        var tabs = jQuery("ul#waForm_tabs_declaration li");
        if (visible) {
            jQuery(tabs[1]).show();
        }
        else {
            jQuery(tabs[1]).hide();
        }
    },

    //-------------------------------------------------------------------------
    event_onclick_waForm_custom_params: function () {

        var visible = this.form.controls.custom_params.get();
        var tabs = jQuery("ul#waForm_tabs_declaration li");
        if (visible) {
            jQuery(tabs[2]).show();
        }
        else {
            jQuery(tabs[2]).hide();
        }
    }

});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
