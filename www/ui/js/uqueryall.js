//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var uqueryall = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waApplication,

	//-------------------------------------------------------------------------
	// proprieta'
	iHaveAWindowOpen: false,
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function()
		{
		this.parent();
				
		// trapping di ESC per chiusura pagina e nuovo record
		document.onkeyup = function (event) {document.waPage.trapKBShortcuts(event);}
		
		// overload del metodo di submit dei form
		var proxy = this;
	    for (var li in this.forms)
			{
	    	this.forms[li].submit = function() {proxy.formSubmit(this);};
	    	this.forms[li].cancel = function() {proxy.formCancel(this);};
			}
			
		},
	
	//-------------------------------------------------------------------------
	// ridefiniamo la apri page per usare jquery e fancybox
	openPage: function (page)
	    {
		this.iHaveAWindowOpen = true;
		
		if (this.navigationMode == this.navigationModeWindow)
			return this.parent(page, window.innerWidth, window.innerHeight);
		
		jQuery("a.iframe").fancybox(
								{
								href : page,
								type : 'iframe',
								hideOnOverlayClick: false,
								width: '96%',
								height: '96%',
								fitToView   : false,
								autoSize    : false,
//								showCloseButton: false,
								speedIn	:	0, 
								speedOut :	0,
								padding: 0,
								margin: 15,
								onCleanup: function() 
											{
											if (self.frames[0] && 
													self.frames[0].document.waPage &&
													self.frames[0].document.waPage.closePage)
												return self.frames[0].document.waPage.closePage();
											}
								}
								);
	
			jQuery("a.iframe").click();

		},
	    
	//-------------------------------------------------------------------------
	// ridefiniamo la chiudi page per usare jquery e fancybox
	closePage: function ()
	    {
		w = opener ? opener : parent;
		w.document.waPage.iHaveAWindowOpen = false;
		if (this.navigationMode == this.navigationModeWindow)
			return this.parent();

		parent.jQuery.fancybox.close();
		return true;
		},
	    
	//-------------------------------------------------------------------------
	trapKBShortcuts: function (event)
	    {
		if (this.iHaveAWindowOpen)
			return;
	
		event = event ? event : window.event;
		
		if (event.keyCode == 78 && event.altKey)
			{
			// lasciamo la gestione alla finestra figlia
			var button = document.getElementById("New");
			if (button)
				button.click();
			}
		if (event.keyCode == 27)
			{
			if (this.form && this.form.controls && this.form.controls.cmd_cancel)
				this.form.controls.cmd_cancel.obj.click();
			if (this.table && document.forms[this.table.nome + "_page_actions"].Close)
				document.forms[this.table.name + "_page_actions"].Close.click();
				
			}
		},
		
	//-------------------------------------------------------------------------
	// azione di chiusura delle pagine figlie con tabelle
	action_waTable_Close: function ()
	    {
	    this.closePage();
	    },
	    
	//-------------------------------------------------------------------------
	// mostra la page contenente la table senza pagezione
	action_waTable_no_pagination: function ()
	    {
		var toGo = this.removeQSParam("watable_no_pagination");
		var qoe = toGo.indexOf("?") != -1 ? "&" : "?";
		location.href = toGo + qoe + "watable_no_pagination=1";
	    },
	    
	//-------------------------------------------------------------------------
	// mostra la page contenente la table con pagezione
	action_waTable_pagination: function ()
	    {
		var toGo = this.removeQSParam("watable_no_pagination");
		var qoe = toGo.indexOf("?") != -1 ? "&" : "?";
		location.href = toGo + qoe + "watable_no_pagination=0";
	    },
	    
	//-------------------------------------------------------------------------
	// mostra i record children di un record
	showChildren: function (table, fkColumn, id)
	    {
            window.open("tbl_crud.php?tbl_name=" + table + "&watable_ff[waTable][0]=" + fkColumn + "&watable_fm[waTable][0]=eq&watable_fv[waTable][0]=" + id);
	    },
	    
	//-------------------------------------------------------------------------
	// ovviamente questo metodo viene richiamato solo in situazioni standard
	// ossia quando c'e' un form che si chiama waForm e un button di
	// annullamento dell'editing che si chiama cmd_cancel; se siete in una
	// altra situzione dovete implementare la vostra gestione dell'evento
	event_onclick_waForm_cmd_cancel: function (event)
	    {
	    this.closePage();
	    },
	    
	//-------------------------------------------------------------------------
	// ovviamente questo metodo viene richiamato solo in situazioni standard
	// ossia quando c'e' un form che si chiama waForm e un button di help
	// cmd_help; se siete in una altra situzione dovete implementare la vostra 
	// gestione dell'evento
	event_onclick_waForm_cmd_help: function (event)
	    {
		var page = encodeBase64(location.pathname);
		var pageTitle = encodeBase64(jQuery("div.waapplication_title").text());
	    this.openPage("help.php?p=" + page + "&pt=" + pageTitle);
	    },
	    
	//-------------------------------------------------------------------------
	enable_submit_buttons: function (form, enable)
		{
		if (form.controls.cmd_submit)
			form.controls.cmd_submit.enable(enable);
		if (form.controls.cmd_submit_close)
			form.controls.cmd_submit_close.enable(enable);
		if (form.controls.cmd_cancel)
			form.controls.cmd_cancel.enable(enable);
		if (form.controls.cmd_delete)
			form.controls.cmd_delete.enable(enable);

		},
		
	//-------------------------------------------------------------------------
	formCancel: function (form)
		{
	    this.closePage();
		},
		
	//-------------------------------------------------------------------------
	// valida form di default: disenable i bottoni di submit
	formSubmit: function (form)
		{
		this.enable_submit_buttons(form, false);
		
		var violations = form.getFormatViolations();
		if (violations.length)
			{
			var msg = '';
			for (var i = 0; i < violations.length; i++)
				{
				msg += "Invalid value in " +  violations[i] + ".\n";
				}
			this.enable_submit_buttons(form, true);
			return this.alert(msg);
			}
			
		violations = form.getMandatoryViolations();
		if (violations.length)
			{
			var msg = '';
			for (var i = 0; i < violations.length; i++)
				{
				msg += violations[i] + " is required.\n";
				}
			this.enable_submit_buttons(form, true);
			return this.alert(msg);
			}
			
		form.gotSubmitConfirm();
		}
		
		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

