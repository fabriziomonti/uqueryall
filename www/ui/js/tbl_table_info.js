//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: uqueryall,

	//-------------------------------------------------------------------------
	// proprieta'

	//-------------------------------------------------------------------------
	//initialization  
	// la fa il parent
	
	//-------------------------------------------------------------------------
	event_onchange_waForm_typeahead_table_name: function () {
		this.form.submit();
	},
			
	//-------------------------------------------------------------------------
	action_waTable_Data: function()
		{
		location.href = "tbl_crud.php?tbl_name=" + this.getQSParam("tbl_name");
		},
		
	//-------------------------------------------------------------------------
	action_waTable_Columns: function()
		{
		location.href = "tbl_table_info.php?tbl_name=" + this.getQSParam("tbl_name") + "&columns=1";
		},
		
	//-------------------------------------------------------------------------
	action_waTable_Constraints: function()
		{
		location.href = "tbl_table_info.php?tbl_name=" + this.getQSParam("tbl_name") + "&constraints=1";
		},
		
	//-------------------------------------------------------------------------
	action_waTable_Indexes: function()
		{
		location.href = "tbl_table_info.php?tbl_name=" + this.getQSParam("tbl_name") + "&indexes=1";
		},
		
	//-------------------------------------------------------------------------
	action_waTable_Foreign_keys: function()
		{
		location.href = "tbl_table_info.php?tbl_name=" + this.getQSParam("tbl_name") + "&foreignkeys=1";
		},
		
	//-------------------------------------------------------------------------
	action_waTable_Point_to: function(id)
		{
		this.openPage("frm_point_to.php?field_name=" + this.getQSParam("tbl_name") + "." + this.table.rows[id].fields.COLUMN_NAME);
		}
		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
