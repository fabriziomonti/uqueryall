//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class({
//-------------------------------------------------------------------------
// extends
    Extends: uqueryall,
    //-------------------------------------------------------------------------
    // proprieta'

    //-------------------------------------------------------------------------
    //initialization  
    // la fa il parent

    //-------------------------------------------------------------------------
    event_onclick_waForm_registration: function () {

        var confirmVisible = this.form.controls.registration.get();
        this.form.controls.password_confirm.show(confirmVisible, true);
        this.form.controls.password_confirm.setMandatory(confirmVisible, true);
    },

    //-------------------------------------------------------------------------
    formSubmit: function (form) {

        if (this.form.controls.registration.get()) {
            if (this.form.controls.password_confirm.get() !== this.form.controls.password.get()) {
                return this.alert("Password non corrispondenti");
            }
        }

        return this.parent(form);

    },

    //-------------------------------------------------------------------------
    // al momento non usato
    isPasswordStrong: function (password) {

        var uppercase = password.search(/[A-Z]/) >= 0;
        var lowercase = password.search(/[a-z]/) >= 0;
        var number = password.search(/[0-9]/) >= 0;
        var specialChars = password.search(/[^\w]/) >= 0;
        return uppercase && lowercase && number && specialChars && password.length >= 8;
    }

});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
