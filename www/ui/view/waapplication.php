<?php
namespace uqueryall;

//******************************************************************************
class waapplication_view implements \waLibs\i_waApplicationView
	{

	/**
	 * dati in input
	 * @var \waLibs\waApplicationData 
	 */
	protected $data = null;

	//**************************************************************************
	public function transform(\waLibs\waApplicationData  $data)
		{
		$this->data = $data;
		if (strpos($_SERVER["SERVER_SOFTWARE"], "nginx") !== false)
			{
			// per non cambiare la url-rewrite in configurazione nginx
			$this->data->waApplicationPath = "/uqueryall/walibs4/waapplication";
			$this->data->workingDirectory = "/uqueryall";
			}
		
		?>

		<!DOCTYPE html>
		<html>
			<head>
				<!-- Bootstrap core CSS -->
				<link href='//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet'>

				<link href='<?= $this->data->waApplicationPath ?>/uis/waapplication_default/css/waapplication.css' rel='stylesheet'/>

				<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/mootools/1.6.0/mootools-core.min.js'></script>
				<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
				<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js'></script>

				<!--siccome usiamo sia mootols che jquery, jquery deve essere chiamato per esteso-->
				<script type='text/javascript'>
					jQuery.noConflict();
				</script>

				<!--fancybox-->
				<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" />
				<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
				<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js"></script>

				<script type='text/javascript' src='<?= $this->data->waApplicationPath ?>/uis/waapplication_default/js/strmanage.js'></script>
				<script type='text/javascript' src='<?= $this->data->waApplicationPath ?>/uis/waapplication_default/js/waapplication.js'></script>

				<title>
		<?= $this->data->page->items["title"]->value ?>
					|
		<?= $this->data->title ?>
				</title>
			</head>
			<body >
				<noscript>
				<hr />
				<div style='text-align: center'>
					<b>
						Questa applicazione usa Javascript, ma il tuo browser ha questa funzione
						disabilitata. Sei pregato di abilitare Javascript per il dominio <?= $this->data->domain ?>
						e ricaricare la pagina.
					</b>
				</div>
				<hr />
				</noscript>

				<!-- se lavoriamo con navigazione interna creiamo anche l'iframe destinato a contenere la finestra figlia-->
				<?php if ($this->data->navigationMode == \waLibs\waApplication::NAV_INNER) : ?>
					<a class="iframe" id="fb_iframe" href="" style="display:none;"></a>
				<?php endif; ?>

				<!-- creazione degli itemi costitutivi della pagina (titolo, tabelle, moduli, testo libero, ecc.-->
				<?php
				foreach ($this->data->page->items as $item) :
					if ($item->name == "version_data") :
						$this->setVersiondata($item);
					elseif ($item->name == "title") :
						$this->setTitle($item);
					elseif ($item->name == "waapplication_action_back") :
						$this->setBackButton();
					elseif ($item->name == "waapplication_action_close") :
						$this->setClosePageButton();
					elseif ($item->name == "google_maps_api_key") :
						// già fatto
					elseif ($item->name == "env") :
						// già fatto
					else :
						$this->setItem($item);
					endif;
				endforeach;
				?>

				<!-- tentativi euristici: qui l'xsl tenta sempre di caricare:-->
				<!-- - un css dell'applicazione (directory_di_lavoro/ui/css/nome_applicazione.css)-->
				<!-- - un css della pagina  (directory_di_lavoro/ui/css/nome_pagina.css)-->
				<!-- - un js dell'applicazione (directory_di_lavoro/ui/js/nome_applicazione.js)-->
				<!-- - un js della pagina  (directory_di_lavoro/ui/js/nome_pagina.js)-->
				<!-- i js della pagina sono sempre gli ultimi a dover essere caricati, altrimenti non vedono le strutture altrui... -->
				<link href='<?= $this->data->workingDirectory ?>/ui/css/<?= $this->data->name ?>.css' rel='stylesheet'/>
				<link href='<?= $this->data->workingDirectory ?>/ui/css/<?= $this->data->page->name ?>.css' rel='stylesheet'/>
				<script type='text/javascript' src='<?= $this->data->workingDirectory ?>/ui/js/<?= $this->data->name ?>.js'></script>
				<script type='text/javascript' src='<?= $this->data->workingDirectory ?>/ui/js/<?= $this->data->page->name ?>.js'></script>

				<!-- se non esiste il file js relativo alla pagina, creiamo un oggetto pagina che ha le proprieta' -->
				<!-- e i metodi di default dell'applicazione. -->
				<!-- In ogni caso diciamo all'applicazione/pagina in che modalita' si dovra' navigare -->
				<!-- e se la pagina deve allineare la mamma e/o eventualmente chiudersi -->
				<script type='text/javascript'>
					if (!document.waPage)
						document.waPage = new uqueryall();
					document.waPage.navigationMode = '<?= $this->data->navigationMode ?>';
					<?php if ($this->data->page->returnValues) : ?>
									document.waPage.updateParent('<?= $this->data->page->returnValues ?>');
						<?php if ($this->data->page->close) : ?>
										document.waPage.closePage();
						<?php endif; ?>
					<?php endif; ?>
				</script>

				<!--modal per alert/confirm-->
				<div  id="waapplication_alert" class="modal fade">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title"><?=$data->title?></h4>
							</div>
							<div class="modal-body">
							</div>
							<div class="modal-footer">
								<button id="waapplication_alert_close" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
								<button id="waapplication_confirm_cancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
								<button id="waapplication_confirm_confirm" type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->				

			</body>
		</html>

		<?php
		}

	//**************************************************************************
	protected function setItem(\waLibs\waApplicationDataPageItem $item)
		{
		?>
		<div class="waapplication_<?= $item->name ?>">
		<?= $item->value ?>
		</div>
		<?php
		}

	//**************************************************************************
	protected
			function setVersionData(\waLibs\waApplicationDataPageItem $item)
		{
		?>
		<div class="waapplication_<?= $item->name ?>">
		<?= $this->data->title ?> versione <?= $item->value->nr ?> - <?= $item->value->date ?>
		</div>
		<?php
		}

	//**************************************************************************
	protected function setTitle(\waLibs\waApplicationDataPageItem $item)
		{
		// ambiente
		if ($this->data->page->items["env"]) {
			?>
			<div class="waapplication_env">
				<?= $this->data->page->items["env"]->value ?>
			</div>
			<?php
			
		}
		
		// definizione pagina di help
		$page = base64_encode($_SERVER["SCRIPT_NAME"]);
		$pageTitle = base64_encode($item->value);
		?>
		<!-- div class="waapplication_<?= $item->name ?>" onclick="document.waPage.openPage('help.php?p=<?=$page?>&pt=<?=$pageTitle?>')">
		<?= nl2br($item->value) ?>
		</div -->
		<div class="waapplication_<?= $item->name ?>">
		<?= nl2br($item->value) ?>
		</div>
		<?php
		}

	//**************************************************************************
	protected
			function setBackButton()
		{
		?>
		<div class="waapplication_back_button">
			<form><input type='button' class='btn' value='&lt;&lt; Back' onclick='history.back()' /></form>
		</div>
		<?php
		}

	//**************************************************************************
	protected
			function setClosePageButton()
		{
		?>
		<div class="waapplication_close_page_button">
			<form><input type='button' class='btn' value='Close' onclick='document.waPage.closePage()' /></form>
		</div>
		<?php
		}

	//**************************************************************************
	}

//******************************************************************************


