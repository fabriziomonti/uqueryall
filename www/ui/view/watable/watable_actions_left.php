<?php 
namespace uqueryall;

//******************************************************************************
class watable_actions_left_view implements \waLibs\i_waTableView
	{
	
	/**
	 * dati in input
	 * @var \waLibs\waTableData
	 */
	protected $data = null;
        
	//**************************************************************************
	public function transform(\waLibs\waTableData $data)
		{
		$this->data = $data;

		if (strpos($_SERVER["SERVER_SOFTWARE"], "nginx") !== false)
			{
			// per non cambiare la url-rewrite in configurazione nginx
			$this->data->waTablePath = "/uqueryall/walibs4/watable";
			}
		
		$this->setCssLink();
		$this->setJavascriptLink();
		$this->setSortOrderFrame();
		$this->setPageActions();
		$this->setNavbar();
		$this->setTable();
		$this->setNavbar();
		$this->setJavascriptObjects();
		
		}
		
	//**************************************************************************
	protected function setCssLink()
		{
		?>

		<!-- Bootstrap core CSS -->

		<link href='<?=$this->data->waTablePath?>/uis/watable_default/css/watable.css' rel='stylesheet'/>

		<?php
		}
		
	//**************************************************************************
	protected function setJavascriptLink()
		{
		?>
		<!-- Bootstrap core JavaScript
		================================================== -->
		
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_default/js/strmanage.js'></script>
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_default/js/wacolumn.js'></script>
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_default/js/warow.js'></script>
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_default/js/watable.js'></script>
		
		<?php
		}
		
	//**************************************************************************
	protected function hasSortFilterOptions() {

            static $has_sort_filter_options = null;

            if ($has_sort_filter_options === null) {
                $has_sort_filter_options = false;
                foreach ($this->data->columnHeaders as $hdr) {
                    if ($hdr->sort || $hdr->filter) {
                        $has_sort_filter_options = true;
                        break;
                    }
                }
            }

            return $has_sort_filter_options;
        }

	//**************************************************************************
	protected function setSortOptions()
		{
		$my_mode_names = array("Ascending" => "Ascendente", "Descending" => "Discendente");
		for ($idx = 0; $idx < 3; $idx++)
			{
			?>
			<div class='row'>
				<div class='col-sm-6' style='text-align: center;'>
					<select class='form-control' name='watable_of[<?=$this->data->name?>][<?=$idx?>]'>
						<option value=''></option>
						<?php
						foreach ($this->data->columnHeaders as $hdr) :
							if ($hdr->sort) :
								?>
								<option  value='<?=$hdr->name?>' <?=$this->data->orders[$idx]->field == $hdr->name ? "selected" : "" ?> >
									<?=$hdr->label?>
								</option>
								<?php 
							endif;
						endforeach;
						?>
					</select>
				</div>
				
				<div class='col-sm-6' style='text-align: center;'>
					<select class='form-control' name='watable_om[<?=$this->data->name?>][<?=$idx?>]'>
						<option value=''></option>
						<?php foreach ($this->data->orderModes as $mode) :?>
							<option  value='<?=$mode->value?>' <?=$this->data->order[$idx]->mode == $mode->value ? "selected" : "" ?> >
								<?=$mode->name?>
							</option>
							<?php endforeach;?>
					</select>
				</div>
			
			</div>
			<?php
			}
			
		}
		
	//**************************************************************************
	protected function setFilterOptions()
		{
		$my_mode_names = array("lower" => "minore", 
								"lower-equal" => "minore-uguale",
								"equal" => "uguale",
								"greater-equal" => "maggiore-uguale",
								"greater" => "maggiore",
								"not equal" => "diverso",
								"contains" => "contiene"
								);

		for ($idx = 0; $idx < 5; $idx++)
			{
			?>

			<div class='row'>
				<div class='col-sm-4' style='text-align: center;'>
					<select class='form-control' name='watable_ff[<?=$this->data->name?>][<?=$idx?>]'>
						<option value=''></option>
						
						<?php
						foreach ($this->data->columnHeaders as $hdr) :
							if ($hdr->filter) :
								?>
								<option  value='<?=$hdr->name?>' <?=$this->data->filters[$idx]->field == $hdr->name ? "selected" : "" ?> >
									<?=$hdr->label?>
								</option>
								<?php 
							endif;
						endforeach;
						?>
					</select>
				</div>			
				
				<div class='col-sm-4' style='text-align: center;'>
					<select class='form-control' name='watable_fm[<?=$this->data->name?>][<?=$idx?>]'>
						<option value=''></option>

						<?php foreach ($this->data->filterModes as $mode) :?>
							<option  value='<?=$mode->value?>' <?=$this->data->filters[$idx]->mode == $mode->value ? "selected" : "" ?> >
								<?=$mode->name?>
							</option>
						<?php endforeach;?>
					</select>
				</div>			

				<div class='col-sm-4' style='text-align: center;'>
					<?php
					$fieldType = $this->getColType($this->data->filters[$idx]->field);
					$value = htmlspecialchars($this->data->filters[$idx]->value, ENT_QUOTES | ENT_HTML5);
					if ($fieldType == \waLibs\waDB::DATE)
						{
						$value = substr($value, 8, 2) . "/" .substr($value, 5, 2) . "/" . substr($value, 0, 4);
						}
					elseif ($fieldType == \waLibs\waDB::DATETIME)
						{
						$value = substr($value, 8, 2) . "/" .substr($value, 5, 2) . "/" . substr($value, 0, 4) . " " . substr($value, 11, 2) . ":" . substr($value, 14, 2);
						}
					elseif ($fieldType == \waLibs\waDB::TIME)
						{
						$value = substr($value, 0, 2) . ":" . substr($value, 3, 2);
						}
					?>
					<input 
						class='form-control'
						name='watable_fv[<?=$this->data->name?>][<?=$idx?>]' 
						value='<?=$value?>' 
					>
				</div>			

			</div>			
			<?php
			}
			
		}
		
	//**************************************************************************
	protected function setSortOrderFrame()
		{
                if (!$this->hasSortFilterOptions()) {
                    return;
                }
                
		?>

		<div id='<?=$this->data->name?>_order_filter_frame' class='watable_order_filter_frame' style='visibility: hidden'>
			
			<p>Ordina/Filtra <?=$this->data->title?></p>

			<form  action='<?=$this->data->uri?>' id='<?=$this->data->name?>_order_filter_form' onsubmit='return document.<?=$this->data->name?>.filter(this)'>

				<div class='row watable_sortfilter_header'>
					<div class='col-sm-6' style='text-align: center;'>
						Order
					</div>
					<div class='col-sm-6' style='text-align: center;'>
						Mode
					</div>
				</div>
				<?php
				$this->setSortOptions();
				?>

				<div class='row watable_sortfilter_header'>
					<div class='col-sm-4' style='text-align: center;'>
						Filter
					</div>
					<div class='col-sm-4' style='text-align: center;'>
						Mode
					</div>
					<div class='col-sm-4' style='text-align: center;'>
						Value
					</div>
				</div>
				<?php
				$this->setFilterOptions();
				?>

				<div class='row watable_sortfilter_header'>
					<div class='col-sm-6' style='text-align: center;'>
						<input 
							type='submit' 
							class='btn btn-primary'
							value='Sort/Filter' 
						>
					</div>
					<div class='col-sm-6' style='text-align: center;'>
						<input 
							type='button' 
							class='btn'
							value='Cancel' 
							onclick='document.<?=$this->data->name?>.closeOrderFilter()' 
						>
					</div>
				</div>
				
			</form>
		</div>

		
		<?php
		}
		
	//**************************************************************************
	protected function setPageActions()
		{
		?>
			
		<form action='<?=$this->data->uri?>' id='<?=$this->data->name?>_page_actions' class='watable' onsubmit='return document.<?=$this->data->name?>.quickSearch()'>
			<div class="row">
				
				<div class='col-sm-12'>
					<div class='btn-group'>
						<?php
						foreach ($this->data->pageActions as $action)
							{
                                                        if ($action->name == "Filter" && !$this->hasSortFilterOptions()) {
                                                            continue;
                                                        }
							?>
							<button 
								type='button' 
								class='btn'
								name='<?=$action->name?>' 
								id='<?=$action->name?>' 
								title='<?=$action->label?>' 
								value='<?=$action->label?>' 
								onclick='document.<?=$this->data->name?>.action_<?=$this->data->name?>_<?=$action->name?>()'
							>
								<?=$action->label?>
							</button>
							<?php
							}

						// bottoni di esportazione (csv, xls, pdf)
						$this->setExportActions();
							
						// definizione pagina di help
						$page = base64_encode($_SERVER["SCRIPT_NAME"]);
						$pageTitle = base64_encode($this->data->title);
							
						?>
						<!-- button 
							type='button' 
							class='btn btn-info'
							title='Help' 
							value='Help' 
							onclick='document.waPage.openPage("help.php?p=<?=$page?>&pt=<?=$pageTitle?>")'
						>
							Help
						</button -->
						
					</div>
				</div>

				<?php
				// controlli per l'immissione testo di ricerca rapida
				$this->setQuickSearchControls();
				?>
				
			</div>
		</form>
			
		
		<?php
		}
		
	//**************************************************************************
	protected function setQuickSearchControls()
		{
                if (!$this->hasSortFilterOptions()) {
                    return;
                }
                
		$value = htmlspecialchars($this->data->quickSearch, ENT_QUOTES | ENT_HTML5);
		
		?>
		<div class='col-sm-12 watable_quick_search'>
			<div class='input-group'>
				<input 
					class='form-control'
					placeholder="text to search..."
					name='watable_qs[<?=$this->data->name?>]' value='<?=$value?>'
				>
				 <div class='input-group-btn'>
					<button 
						type='submit' 
						title='Search' 
						value='Search' 							
						class='btn btn-primary'
					>
						Search
					</button>
				</div>
			</div>
		</div>
		<?php
		}
		
	//**************************************************************************
	protected function setExportActions()
		{
		$qoe = strpos($this->data->uri, "?") === false ? "?" : "&";
		?>
		<button 
			type='button' 
			class='btn'
			title='CSV' 
			value='CSV' 
			onclick='document.location.href="<?=$this->data->uri . $qoe . "watable_export_csv[" . $this->data->name . "]=1"?>"'
		>
			CSV
		</button>
<!--		<button 
			type='button' 
			class='btn'
			title='XLS' 
			value='XLS' 
			onclick='document.location.href="<?=$this->data->uri . $qoe . "watable_export_xls[" . $this->data->name . "]=1"?>"'
		>
			XLS
		</button>-->
		<?php
                // se abbiamo più di 30 colonne non mostriamo il bottone pdf, 
                // altrimenti fpdf si pianta
                if (count($this->data->columnHeaders) <= 30) {
                    ?>
                    <button 
                            type='button' 
                            class='btn'
                            title='PDF' 
                            value='PDF' 
                            onclick='var w = window.open("<?=$this->data->uri . $qoe . "watable_export_pdf[" . $this->data->name . "]=1"?>")'
                    >
                            PDF
                    </button>

                    <?php
                    }
		?>

		<?php
		}
		
	//**************************************************************************
	protected function setNavbar()
		{
		if ($this->data->navbar->totalPageNr <= 1) 
			{
			return;
			}
			
		$tblName = $this->data->name;
		
		$first = $this->data->navbar->currentPageNr - 2 < 0 ? 0 : $this->data->navbar->currentPageNr - 2;
		$last = $first + 4 < $this->data->navbar->totalPageNr ? $first + 4 : $this->data->navbar->totalPageNr - 1;
		
		?>
		<div class='row watable_navbar'>

			<ul class="pagination">

				<?php
				$class = $this->data->navbar->currentPageNr == 0 ? "class='disabled'" : "";
				$href = $this->data->navbar->currentPageNr == 0 ? 
							"void(0)" : 
							"document.$tblName.goToPage(\"" . ($this->data->navbar->currentPageNr - 1) . '")';
				?>
				<li <?=$class?>>
					<a href='javascript:document.<?=$tblName?>.goToPage("0")'>&laquo;&laquo;</a>
				</li>
				<li <?=$class?>>
					<a href='javascript:<?=$href?>'>&laquo;</a>
				</li>

				<?php
				for ($li = $first; $li <= $last; $li++)
					{
					?>
					<li <?=$li == $this->data->navbar->currentPageNr ? "class='active'" : ""?>>
						<a href='javascript:document.<?=$this->data->name?>.goToPage("<?= $li ?>")'><?=$li + 1?></a></li>
					<?php
					}
					
				$class = $this->data->navbar->currentPageNr >= $this->data->navbar->totalPageNr - 1 ? "class='disabled'" : "";
				$href = $this->data->navbar->currentPageNr == $this->data->navbar->totalPageNr - 1 ? 
							"void(0)" : 
							"document." . $this->data->name . '.goToPage("' . ($this->data->navbar->currentPageNr + 1) . '")';
				?>
				<li <?=$class?>>
					<a href='javascript:<?=$href?>'>&raquo;</a>
				</li>
				<li <?=$class?>>
					<a href='javascript:document.<?=$tblName?>.goToPage("<?=$this->data->navbar->totalPageNr - 1?>")'>&raquo;&raquo;</a>
				</li>
			</ul>	
			
			<div class='watable_navbar_text'>
				rec <?=$this->data->navbar->firstRecord?>-<?=$this->data->navbar->lastRecord?> of <?=$this->data->navbar->totalRecordNr?>
			</div>
			
		</div>
		<?php
		}
		
	//**************************************************************************
	protected function setTable()
		{
		?>
		<form id='<?=$this->data->name?>' action='' method='post' class='watable'>
			<div class="table-responsive"> 
				<table class='table table-bordered table-striped table-hover'>
					<?php
					if ($this->data->title) 
						{
						?>
						<!--<caption><?=htmlspecialchars($this->data->title)?></caption>-->
						<?php
						}
					$this->setHeaders();
					$this->setTotalsRow();
					$this->setRows();
					?>
				</table>
			</div>
		</form>
			
		<?php
		}
		
	//**************************************************************************
	protected function setRowActions(\waLibs\waTableDataRow $row)
		{
		$tblName = $this->data->name;

		?>
		<!--colonna delle azioni su record--> 
		<td style='width: 1%'>
                    <div class='group-btn'>
                    <?php
                    foreach ($row->enableableActions as $idx => $enabled) {
                        if (!$enabled) {
                            continue;
                        }
                        $action = $this->data->recordActions[$idx]->name;
                        $on_click = "document.$tblName.action_$tblName" . "_$action(\"$row->id\")";
                        if (strpos($action, "___child_table___") === 0) {
                            list($dont_care, $params) = explode("___child_table___", $action);
                            list( $table, $fkColumn) = explode(".", $params);
                            $on_click = "document.waPage.showChildren(\"$table\", \"$fkColumn\", \"$row->id\")";
                        }
                        ?>
                        <button type='button' class='btn' onclick='<?=$on_click?>'>
                            <?=$this->data->recordActions[$idx]->label?>
                        </button>
                        <?php
                    }
                    ?>
                    </div>
		</td>
		<?php
		}
		
	//**************************************************************************
	protected function setRows()
		{
		$tblName = $this->data->name;

		?>
		<tbody>
			<?php
			foreach ($this->data->rows as $row)
				{
				?>
				<tr id='row_<?=$tblName?>_<?=$row->id?>' onclick='<?="document.$tblName.rows[\"$row->id\"]"?>.changeStatus()'>
					
					<?php
					$this->setRowActions($row);
					$this->setRowData($row);
					?>
					
				</tr>
				<?php
				}
			?>
		</tbody>
		<?php
		}
		
	//**************************************************************************
	protected function setRowData(\waLibs\waTableDataRow $row)
		{

		foreach ($row->cells as $idx => $cellValue)
			{
			$this->setCell($row, $cellValue, $idx);
			}
			
		}
		
	//**************************************************************************
	protected function setCell(\waLibs\waTableDataRow $row, $cellValue, $idx)
		{
		$tblName = $this->data->name;

		$hdr = $this->data->columnHeaders[$idx];
		if (!$hdr->show)
			{
			return;
			}

		?>
		<td style='text-align: <?=$this->getAlignment($hdr)?>' class='<?=$idx == 0 ? "id_cell" : "" ?>'>
			<?php

			if ($hdr->link)
				{
				?>
				<a href='javascript:document.<?=$tblName?>.link_<?=$tblName?>_<?=$hdr->name?>("<?=$row->id?>")'>
					<?=htmlspecialchars($cellValue)?>
				</a>
				<?php
				}

			elseif (!$hdr->HTMLConversion)
				{
				echo $cellValue;
				}

			elseif ($hdr->fieldType == \waLibs\waDB::DATETIME && $hdr->format == \waLibs\waTable::FMT_TIME)
				{
				if (strlen($cellValue))
					{
					echo substr($cellValue, 0, 5);
					}
				}

			elseif ($hdr->format == \waLibs\waTable::FMT_DATE)
				{
				if (strlen($cellValue))
					{
					echo substr($cellValue, 8, 2) . "/" .
					substr($cellValue, 5, 2) . "/" .
					substr($cellValue, 0, 4);
					}
				}

			elseif ($hdr->fieldType == \waLibs\waDB::DATE)
				{
				if (strlen($cellValue))
					{
					echo substr($cellValue, 8, 2) . "/" .
					substr($cellValue, 5, 2) . "/" .
					substr($cellValue, 0, 4);
					}
				}

			elseif ($hdr->fieldType == \waLibs\waDB::DATETIME)
				{
				if (strlen($cellValue))
					{
					echo substr($cellValue, 8, 2) . "/" .
					substr($cellValue, 5, 2) . "/" .
					substr($cellValue, 0, 4) . " " .
					substr($cellValue, 11, 8);
					}
				}

			elseif ($hdr->fieldType == \waLibs\waDB::TIME)
				{
				if (strlen($cellValue))
					{
					echo substr($cellValue, 0, 2) . ":" .
					substr($cellValue, 3, 2);
					}
				}

			elseif ($hdr->fieldType == \waLibs\waDB::DECIMAL)
				{
				if (strlen($cellValue))
					{
					echo number_format($cellValue, 2, ",", ".");
					}
				}

//			elseif ($hdr->fieldType == \waLibs\waDB::INTEGER)
//				{
//				if (strlen($cellValue))
//					echo number_format ($cellValue, 0, ",", ".");
//				}

		else
			{
			echo nl2br(htmlspecialchars($cellValue));
			}
			
			?>
		</td>
		<?php

		}
		
	//**************************************************************************
	protected function getAlignment(\waLibs\waTableDataColumnHeader $hdr)
		{
		$alignment = "left";
		$alignment = $hdr->fieldType == \waLibs\waDB::DATE ? "center" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::DATETIME ? "center" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::TIME ? "center" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::INTEGER ? "right" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::DECIMAL ? "right" : $alignment;
		$alignment = $hdr->alignment == \waLibs\waTable::ALIGN_R ? "right" : $alignment;
		$alignment = $hdr->alignment == \waLibs\waTable::ALIGN_C ? "center" : $alignment;

		return $alignment;
		}
		
	//**************************************************************************
	protected function setTotalsRow()
		{
		if (!$this->data->totalsRow)
			return;
		
		?>
		<tfoot>
			<tr>
				
				<?php
				$this->setRowActionsHeaders();
				$this->setTotalsRowData();
				?>
				
			</tr>
		</tfoot>
		<?php
		}
		
	//**************************************************************************
	protected function setTotalsRowData()
		{

		foreach ($this->data->totalsRow->cells as $idx => $cellValue)
			{
			$hdr = $this->data->columnHeaders[$idx];
			if (!$hdr->show) continue;
			?>
			<th style='text-align: <?=$this->getAlignment($hdr)?>'>
				<?php

				if ($hdr->fieldType == \waLibs\waDB::DECIMAL)
					{
					if (strlen($cellValue))
						{
						echo number_format($cellValue, 2, ",", ".");
						}
					}
				else
					{
					echo $cellValue;
					}
				?>	
			</th>
			
			<?php
			}
			
		}
		
	//**************************************************************************
	protected function setHeaders()
		{
		
		?>
			
		<thead>
			<tr id='<?=$this->data->name?>_headers'>

				<?php
				$this->setRowActionsHeaders();
				$this->setHeadersData();
				?>

			</tr>
		</thead>
			
		<?php
		}
		
	//**************************************************************************
	protected function setHeadersData()
		{
		
		$qoe = strpos($this->data->uri, "?") === false ? "?" : "&";
		$tblName = $this->data->name;
			
		foreach ($this->data->columnHeaders as $idx => $hdr)
			{
			if (!$hdr->show)
				{
				continue;
				}
			?>
			<th 
				style='text-align: <?=$this->getAlignment($hdr)?>' 
				id='<?=$this->data->name?>_<?=$hdr->name?>'
				class='<?=$idx == 0 ? "id_cell" : "" ?>'
			>
				<?php
				if ($hdr->sort)
					{
					$sortMode = $hdr->quickSort == "asc" ? "desc" : "asc";
					echo "<a href='" . $this->data->uri . "$qoe" . "watable_qo[$tblName]=$hdr->name&watable_qom[$tblName]=$sortMode'>\n";
					echo $hdr->label;
					if ($hdr->quickSort != "no")
						{
						?>
						<img src='<?=$this->data->waTablePath?>/uis/watable_default/img/<?=$hdr->quickSort?>_order.gif' border='0'/>
						<?php
						}
					echo "</a>\n";
					}
				else
					{
					echo $hdr->label;
					}
				?>
			</th>
			<?php
			}
		}
		
	//**************************************************************************
	protected function setRowActionsHeaders()
		{
		?>
		<th></th>
		<?php
		}
		
	//**************************************************************************
	protected function setJavascriptObjects()
		{
		$tblName = $this->data->name;

		?>
		
		<script type='text/javascript'>
			// inizializzazione parametri tabella <?=$tblName?>
			
			document.<?=$tblName?> = new waTable('<?=$tblName?>', '<?=$this->data->columnHeaders[0]->name?>', '<?=$this->data->exclusiveSelection ? 1 : 0 ?>', '<?=$this->data->formPage?>');

			<?php
			foreach ($this->data->rows as $row)
				{
				?>
				new waRow(document.<?=$tblName?>, '<?=$row->id?>', <?=$this->row2json($row)?>);
				<?php
				}
				
			// inizializzazione delle proprieta' delle colonne
			foreach ($this->data->columnHeaders as $idx => $hdr)
				{
				if (!$hdr->show) continue;
				?>
				new waColumn(document.<?=$tblName?>, "<?=$hdr->name?>", "<?=$hdr->label?>", "<?=$hdr->fieldType?>");
				<?php
				}
			?>

		</script>		
		
		<?php
		}
		
	//**************************************************************************
	protected function row2json(\waLibs\waTableDataRow $row)
		{
		$retval = array();
		
		foreach($row->cells as $idx => $cellValue)
			{
			$retval[$this->data->columnHeaders[$idx]->name] = $cellValue;
			}

		return json_encode($retval);
		}
		
	//**************************************************************************
	protected function getColType($col_name)
		{
		if ($idx = $this->getIdxFromName($this->data->columnHeaders, $col_name))
			{
			return $this->data->columnHeaders[$idx]->fieldType;
			}
			
		}
		
	//**************************************************************************
	// dato l'attributo name di un oggetto contenuto in un array, restituisce
	// l'indice dell'emento dell'array in cui il name è contenuto
	protected function getIdxFromName($array, $name, $name_property = "name")
		{
		foreach ($array as $idx => $item)
			{
			if ($item->$name_property == $name)
				{
				return $idx;
				}
			}
			
		return false;
		}
		
	//**************************************************************************
	}
//******************************************************************************


