<?php 
namespace uqueryall;
include_once __DIR__ . "/watable_actions_left.php";
	
//******************************************************************************
class watable_actions_context_view extends watable_actions_left_view
	{
	
	//**************************************************************************
	protected function setJavascriptLink()
		{
		parent::setJavascriptLink();
		?>
		<script type='text/javascript' src='<?=$this->data->waTablePath?>/uis/watable_actions_context/js/watable_actions_context.js'></script>
		
		<?php
		}
		
	//**************************************************************************
	protected function setRowActionsHeaders()
		{
		}
		
	//**************************************************************************
	protected function setRowActions($row)
		{
		}
		
	//**************************************************************************
	protected function setJavascriptObjects()
		{
		parent::setJavascriptObjects();
		
		$tblName = $this->data->name;

		// costruzione del menu contestuale per ogni riga
		foreach ($this->data->rows as $row)
			{
			?>
		
			<ul id="<?=$tblName?>_contextMenu_<?=$row->id?>" class="dropdown-menu" role="menu" style="display:none" >
				<?php
				foreach ($row->enableableActions as $idx => $enabled)
                                    {
                                    if ($enabled)
                                        {
                                        $action = $this->data->recordActions[$idx]->name;
                                        $on_click = "document.$tblName.action_$tblName" . "_$action(\"$row->id\")";
                                        if (strpos($action, "___child_table___") === 0) {
                                            list($dont_care, $params) = explode("___child_table___", $action);
                                            list( $table, $fkColumn) = explode(".", $params);
                                            $on_click = "document.waPage.showChildren(\"$table\", \"$fkColumn\", \"$row->id\")";
                                        }
                                        ?>
                                        <li>
                                            <a href='javascript:<?=$on_click?>'>
                                                <?=$this->data->recordActions[$idx]->label?>
                                            </a>
                                        </li>
                                        <?php
                                        }
                                    }
				?>
			</ul>

			<script type='text/javascript'>
				jQuery("#row_<?=$tblName?>_<?=$row->id?> td").contextMenu({
					menuSelector: "#<?=$tblName?>_contextMenu_<?=$row->id?>",
					menuSelected: function (invokedOn, selectedMenu) {}
				});

			</script>		
		
			<?php
			}
			
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


