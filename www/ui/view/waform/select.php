<?php 
namespace uqueryall;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waSelectView extends waControlView 
	{
	
	//**************************************************************************
	public function transform($data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>

			<select 
				id='<?=$this->form->name?>_<?=$this->name?>' 
				name='<?=$this->name?>' 
				<?=$this->getControlAttributes()?> 
				style='<?=$this->getControlStyle()?>'
				class='form-control <?=$this->getControlClass()?>'
			>

				<?php
				if ($this->emptyRow)
					{
					?>
					<option value=''></option>
					<?php
					}

				foreach ($this->list as $key => $val)
					{
					list($realKey, $rest) = explode("|", $key);
					$selected = $realKey == $this->value ? "selected='selected'" : "";
					?>
					<option value='<?=$key?>' <?=$selected?>><?=$val?></option>
					<?php
					}
				?>

			</select>
		</div>

		<?php
		
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


