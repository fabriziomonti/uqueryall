<?php 
namespace uqueryall;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waTextAreaView extends waControlView 
	{
	
	//**************************************************************************
	public function transform($data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<textarea 
				id='<?=$this->form->name?>_<?=$this->name?>' 
				name='<?=$this->name?>' 
				cols='<?=$this->columns?>' 
				rows='<?=$this->rows?>' 
				<?=$this->getControlAttributes()?> 
				style='<?=$this->getControlStyle()?>'
				class='form-control' <?=$this->getControlClass()?>><?=$this->value?></textarea>
		</div>

		<?php
		
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


