<?php 
namespace uqueryall;
	
require_once __DIR__ . "/i_waFormContainerView.interface.php";
require_once __DIR__ . "/control.php";

//******************************************************************************
class waTabView extends waControlView implements i_waFormContainerView
	{
	public $data;
	
//**************************************************************************
	public function open(\waLibs\waFormDataControlContainer $data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->form->tabs_inited)
			{
			?>
			<ul class="nav nav-tabs" id="<?=$this->form->name?>_tabs_declaration">
			<?php
			// andiamo a cercare tutti i tab per la dichiarazione
			$first = true;
			foreach ($this->form->controls as $control)
				{
				if ($control->type == "tab")
					{
					?>
					<li class="<?= $first ? 'active' : ''?>">
						<a data-toggle="tab" href="#<?=$this->form->name . "_$control->name"?>">
							<?=$control->value?>
						</a>
					</li>
					<?php
					$first = false;
					$this->form->lastTab = $control;
					}
				}
			
			?>
			</ul>
			<div class="tab-content">
			<?php
			}

		// apertura del contenuto del tab
		?>
		<div 
			id='<?=$this->form->name?>_<?=$this->name?>' 
			<?=$this->getControlAttributes()?>
			style='<?=$this->getControlStyle()?>'
			class='tab-pane fade <?= !$this->form->tabs_inited ? 'in active' : ''?> watab <?=$this->getControlClass()?>'
		>

		<?php
		$this->form->tabs_inited = true;
		
		// stuff starts here...
		
		}
		
	//**************************************************************************
	public function close()
		{
		//... stuff ends here
		?>
		</div>
		<?php
		
		// se è l'ultimo chiude la porta (il div "tabcontent")
		if ($this->name == $this->form->lastTab->name)
			{
			?>
			</div>
			<?php
			}
		}
		
	//**************************************************************************
	}
//******************************************************************************


