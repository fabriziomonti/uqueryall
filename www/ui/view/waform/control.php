<?php 
namespace uqueryall;
	
//******************************************************************************
class waControlView 
	{
	
	/**
	 * dati della form
	 * @var stdClass
	 */
	protected $form = null;

	/**
	 * dati del controllo
	 * @var stdClass
	 */
	protected $data = null;

	//**************************************************************************
	public function __construct($form)
		{
		$this->form = $form;
		}
		
	//**************************************************************************
	public function transform($data)
		{
		$this->data = $data;

		// i parametri sono anche trasformati in proprietà locali
		foreach ($data as $key => $val)
			{
			$this->$key = $val;
			}
		}
		
	//**************************************************************************
	public function transformInput($data)
		{
		return $_POST[$data->name];
		}
		
	//**************************************************************************
	protected function setControlHeader()
		{
		?>
		<!-- <?=$this->type . " - " . $this->name?> -->
		<?php
		}
		
	//**************************************************************************
	protected function getControlAttributes()
		{
		
		$retval = "";
		$retval .= $this->readOnly ? " disabled='disabled'" : "";
		return $retval;
		}
		
	//**************************************************************************
	protected function getControlClass()
		{
		
		$retval = "";
		$retval .= $this->readOnly ? " disabled" : "";
		$retval .= $this->mandatory ? " required" : "";
		return $retval;
		}
		
	//**************************************************************************
	protected function getControlStyle()
		{
		$retval = "";
		$retval .= !$this->visible ? " display: none;" : "";
		return $retval;
		}
		
	//**************************************************************************
	/**
	 * verifica che un controllo sia associato a una label o una label
	 * associata a un controllo
	 * 
	 * @return boolean
	 */
	protected function controlHaveLabel()
		{
		if ($this->type == "label")
			{
			return false;
			}
			
		foreach ($this->form->controls as $control)
			{
			if ($control->name == $this->name && $control->type == "label")
				{
				return true;
				}
			}
		
		return false;
		}
		
	//**************************************************************************
	/**
	 * verifica che una label sia associata a un controllo
	 * 
	 * @return boolean
	 */
	protected function labelHaveControl()
		{
		if ($this->type != "label")
			{
			return false;
			}

		foreach ($this->form->controls as $control)
			{
			if ($control->name == $this->name && $control->type != "label")
				{
				return true;
				}
			}
		
		return false;
		}
		
	}
//******************************************************************************


