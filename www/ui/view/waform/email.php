<?php 
namespace uqueryall;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waEmailView extends waControlView 
	{
	
	//**************************************************************************
	public function transform($data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		$html_size = $this->maxChars > 50 ? 50 : $this->maxChars;
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<input 
				type='email'
				id='<?=$this->form->name?>_<?=$this->name?>' 
				name='<?=$this->name?>' 
				value='<?=htmlspecialchars($this->value, ENT_QUOTES | ENT_HTML5)?>'
				maxlength='<?=$this->maxChars?>' 
				size='<?=$html_size?>' 
				<?=$this->getControlAttributes()?> 
				style='<?=$this->getControlStyle()?>'
				class='form-control <?=$this->getControlClass()?>'
			/>
		</div>

		<?php
		
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


