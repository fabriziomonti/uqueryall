<?php 
namespace uqueryall;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waMultiSelectView extends waControlView 
	{
	
	//**************************************************************************
	public function transform($data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<div 
				id='<?=$this->form->name?>_<?=$this->name?>' 
				class='form-control waform_multiselect_checkbox'
			>

				<?php
				foreach ($this->list as $key => $val)
					{
					list($realKey, $rest) = explode("|", $key);
					$checked = in_array($realKey, $this->value) ? "checked" : "";
					$disabled = in_array($realKey, $this->notSelectableList) ? "disabled" : "";
					?>
					<div>
						<input 
							type='checkbox' 
							id='<?=$this->form->name?>_<?=$this->name?><?=$key?>' 
							name='<?=$this->name?>[<?=$key?>]' 
							<?=$checked?>
							<?=$disabled?>
						>
						<div>
							<label 
								for='<?=$this->form->name?>_<?=$this->name?><?=$key?>'
								class='<?=$disabled?>'
							>
								<?=$val?>
							</label>
						</div>
					</div>
					<?php
					}
				?>

			</div>
			
		</div>

		<?php
		
		}
		
	//**************************************************************************
	public function transformInput($data)
		{
		if (!is_array($_POST[$data->name]))
			{
			return null;
			}

		return array_keys($_POST[$data->name]);
		}
		
	//**************************************************************************
	}
//******************************************************************************


