<?php 
namespace uqueryall;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waLabelView extends waControlView 
	{
	
	//**************************************************************************
	public function transform($data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if ($this->labelHaveControl())
			{
			?>
			<div 
				class='form-group'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
			>
			<?php
			}
			
		// definizione pagina di help
		$page = base64_encode($_SERVER["SCRIPT_NAME"]);
		$field = base64_encode($this->name);
		$fieldLabel = base64_encode($this->value);
			
		?>
		<label 
			id='lbl_<?=$this->form->name?>_<?=$this->name?>' 
			<?=$this->getControlAttributes()?> 
			style='<?=$this->getControlStyle()?>'
			class='<?=$this->getControlClass()?>'
		><?=$this->value?></label>

		<?php
		
		}
		
	//**************************************************************************
	public function transformInput($data)
		{
		return null;
		}
		
	//**************************************************************************
	}
//******************************************************************************


