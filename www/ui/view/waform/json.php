<?php 
namespace uqueryall;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waJsonView extends waControlView 
	{
	
	//**************************************************************************
	public function transform($data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
		
		// mascheriamo eventuali escape escapandoli
		$this->value = str_replace("\\", "\\\\", $this->value);
			
		?>
			
			<input 
				id='<?=$this->form->name?>_<?=$this->name?>' 
				name='<?=$this->name?>' 
				value='<?=htmlspecialchars($this->value, ENT_QUOTES | ENT_HTML5)?>'
				style='display: none;'
			/>
				
			<div 
				class='wajson <?=$this->getControlClass()?>'
				id='<?=$this->form->name?>_<?=$this->name?>_editor' 
				<?=$this->getControlAttributes()?> 
				style='<?=$this->getControlStyle()?>'
			/>

		</div>

			<script type="text/javascript">
				jQuery(document).ready
					(
					function () 
						{
						var container = document.getElementById('<?=$this->form->name?>_<?=$this->name?>_editor');
						var options = {
										"name" : "<?=$this->name?>",
										"onChange" : function() 
											{
											jQuery("#<?=$this->form->name?>_<?=$this->name?>").val(<?=$this->form->name?>_<?=$this->name?>.getText());
											}
										};
						// non sono riuscito senza usare una globale...
						<?=$this->form->name?>_<?=$this->name?> = new JSONEditor(container, options);
						<?=$this->form->name?>_<?=$this->name?>.set(JSON.parse('<?=$this->value?>'));
						<?=$this->form->name?>_<?=$this->name?>.collapseAll();
						}
					);
			</script>
				
		<?php
		
		}
		
	//**************************************************************************
	public function transformInput($data)
		{
		$retval = $_POST[$data->name];
		
		// occorre descapare ciò che abbiamo escapato all'andata
		$retval = str_replace("\\\\", "\\", $retval);
		return $retval;
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


