<?php 
namespace uqueryall;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waDateTimeView extends waControlView 
	{
	
	//**************************************************************************
	public function transform($data)
		{
		parent::transform($data);
		$control_id = $this->form->name . "_" . $this->name . "_datetimepicker";
		$value = strlen($data->value) ? 
						substr($data->value, 8, 2) . "/" .
						substr($data->value, 5, 2) . "/" .
						substr($data->value, 0, 4) . " " .
						substr($data->value, 11, 2) . ":" .
						substr($data->value, 14, 2) . ":" .
						substr($data->value, 17, 2)
					: "" ;
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		?>
			<div class='input-group date col-xs-12 col-sm-4 col-md-3 col-lg-3' id='<?=$control_id?>'>
				<input 
					type='text' 
					class='form-control <?=$this->getControlClass()?>'
					id='<?=$this->form->name?>_<?=$this->name?>' 
					name='<?=$this->name?>' 
					value='<?=$value?>' 
					<?=$this->getControlAttributes()?> 
					style='text-align: center; <?=$this->getControlStyle()?>'
				>
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>		
			</div>

			<script type="text/javascript">
				jQuery(function () 
					{
					jQuery('#<?=$control_id?>').datetimepicker
						(
							{
							format: 'DD/MM/YYYY HH:mm:SS',
							widgetPositioning : {horizontal: 'left', vertical: 'bottom'}
							}
						);
					}
				);

			</script>
		
		</div>		
		
		<?php
		}
		
	//**************************************************************************
	public function transformInput($data)
		{
		
		$value = trim($_POST[$data->name]);
		if (!$value)
			{
			return null;
			}
			
		$retval = substr($value, 6, 4) . "-" .
					substr($value, 3, 2) . "-" .
					substr($value, 0, 2) . " " .
					substr($value, 11, 2) . ":" .
					substr($value, 14, 2) . ":" .
					substr($value, 17, 2);
		
		return $retval;
		}
		
	//**************************************************************************
	}
//******************************************************************************


