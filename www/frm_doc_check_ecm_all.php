<?php

include "doc_check.class.php";

//*****************************************************************************
class page extends doc_check {

    var $all_meesages = [];
    
    //**************************************************************************
    function __construct() {
        
        $this->page_title = "Check all EcmLoader results";
        $this->main_dir = "/InboundFlows/Filenet/Smistadoc/ErroriLogOptimo/P2000";
        $this->log_name = "infoP2000.log";
        
        parent::__construct();
    }

    //***************************************************************************
    function check() {

        $this->checkMandatory($this->form);
        
        $this->all_meesages[] = "<hr><b>PecNew</b><br/><br/>";
        $this->main_dir = "/InboundFlows/Filenet/Smistadoc/ErroriLogOptimo/PecNew";
        parent::check();
        
        $this->all_meesages[] = "<br/><br/><hr><b>FDMPosta</b><br/><br/>";
        $this->main_dir = "/InboundFlows/Filenet/Smistadoc/FDM/Posta";
        $this->log_name = "infoFDMPosta.log";
        parent::check();
        
        $this->all_meesages[] = "<br/><br/><hr><b>FDMAgp</b><br/><br/>";
        $this->main_dir = "/InboundFlows/Filenet/Smistadoc/FDM/Agp";
        $this->log_name = "infoFDMAgp.log";
        parent::check();
        
        $this->all_meesages[] = "<br/><br/><hr><b>StornoAssegni</b><br/><br/>";
        $this->main_dir = "/InboundFlows/Filenet/Smistadoc/ErroriLogOptimo/StornoAssegni";
        $this->log_name = "infoStornoAssegno.log";
        parent::check();
        
        $this->all_meesages[] = "<br/><br/><hr><b>P2000</b><br/><br/>";
        $this->main_dir = "/InboundFlows/Filenet/Smistadoc/ErroriLogOptimo/P2000";
        $this->log_name = "infoP2000.log";
        parent::check();
        
        parent::showMessage("Esito rilevamento", implode("\n", $this->all_meesages), false, true);
        
    }
    
    //**************************************************************************
    function showMessage($title, $message, $back = true, $close = false) {
        $this->all_meesages[] = $message;
    }
    
    
//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
