<?php

//******************************************************************************
include "uqueryall.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends uqueryall {

    //*****************************************************************************
    function __construct() {
        parent::__construct();
        $this->addItem($this->getMenu());
        $this->addItem("DB Accounts", "title");
        $this->addItem($this->myGetTable());
        $this->show();
    }

    //*****************************************************************************
    /**
     * @return waLibs\waTable
     */
    function myGetTable() {

        // creazione della tabella
        $arr = [];
        foreach ($this->user->db_accounts as $env => $db_account) {
            $env_id = str_replace(" ", "___blank_placeholder__", $env);
            $arr[$env] = array_merge(["ENV_ID" => $env_id, "ENV" => $env], json_decode(json_encode($db_account), TRUE));
        }
        ksort($arr);
        $table = parent::getTable($arr);
        $table->formPage = "frm_db_account.php";

        $table->addColumn("ENV_ID", "ENV_ID", false);
        $table->addColumn("ENV", "Name");
        $table->addColumn("HOST", "Host");
        $table->addColumn("PORT", "Port");
        $table->addColumn("SID", "SID");
        $table->addColumn("PDB", "PDB");
        $table->addColumn("SCHEMA", "Schema");
        $table->addColumn("USERNAME", "Username");
        $table->loadRows();

        return $table;
    }

}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();
