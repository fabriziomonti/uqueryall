<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

    /**
     *
     * @var waLibs\waForm
     */
    var $form;
    
    var $username_placeholder = "__username_placeholder__";
    var $password_placeholder = "__password_placeholder__";
    var $objectstore_placeholder = "__objectstore_placeholder__";
    var $queryfilter_placeholder = "__queryfilter_placeholder__";
    var $resulsize_placeholder = "__resulsize_placeholder__";
    
    //**************************************************************************
    function __construct() {
        parent::__construct();

        $this->childWindow = true;
        $this->createForm();

        if ($this->form->isToUpdate()) {
            $this->go();
        } else {
            $this->showPage();
        }
    }

    //*****************************************************************************

    /**
     * mostra
     * 
     * costruisce la pagina contenente il form e la manda in output
     * @return void
     */
    function showPage() {
        $this->addItem("FileNet query", "title");
        $this->addItem($this->form);
        $this->show();
    }

    //***************************************************************************
    function createForm() {

        $this->form = $this->getForm();

        $this->form->openTab("main", "Main");
            $ctrl = $this->form->addOption("env", "Env", false, true);
                $ctrl->list = ["PROD" => "PROD", "COLL" => "COLL"];
                $ctrl->value = "PROD";
            $ctrl = $this->form->addOption("object_store", "Object-store", false, true);
                $ctrl->list = ["SX" => "SINISTRI", "SXACQ" => "SINISTRI_ACQUISIZIONE"];
                $ctrl->value = "SX";
            $this->form->addText("CodiceDoc", "CodiceDoc");
            $this->form->addText("Barcode", "Barcode");
            $this->form->addDate("DateCreated_from", "DateCreated from (GMT)");
            $this->form->addDate("DateCreated_to", "DateCreated to (GMT)");
            $this->form->addInteger("max_result", "Max result nr")->value = 10;
        $this->form->closeTab();
        $this->form->openTab("tab_query", "Query");
            $ctrl = $this->form->addTextArea("query", "Query");
        $this->form->closeTab();
        $this->form->openTab("endpoint", "Endpoint");
            $this->form->openFrame("filenet_prod_params", "PROD");
                $this->form->addText("PROD_endpoint", "Endpoint")->value = $this->user->filenet_prod_endpoint;
                $this->form->addText("PROD_username", "Username")->value = $this->user->filenet_prod_username;
                $this->form->addPassword("PROD_password", "Password")->value = $this->user->filenet_prod_password;
            $this->form->closeFrame();
            $this->form->openFrame("filenet_coll_params", "COLL");
                $this->form->addText("COLL_endpoint", "Endpoint")->value = $this->user->filenet_coll_endpoint;
                $this->form->addText("COLL_username", "Username")->value = $this->user->filenet_coll_username;
                $this->form->addPassword("COLL_password", "Password")->value = $this->user->filenet_coll_password;
            $this->form->closeFrame();
        $this->form->closeTab();

        $this->form_submitButtons($this->form, false, false, "Go");
        $this->form->getInputValues();
    }

    //***************************************************************************
    function go() {

        $this->checkMandatory($this->form);
        
        $params = new stdClass();
        $params->env = $this->form->env;
        $params->object_store = $this->form->object_store;
        $params->CodiceDoc = $this->form->CodiceDoc;
        $params->Barcode = $this->form->Barcode;
        $params->DateCreated_from = $this->form->DateCreated_from;
        $params->DateCreated_to = $this->form->DateCreated_to;
        $params->max_result = $this->form->max_result;
        $params->query = $this->form->query;
        $endpoint_field = $this->form->env . "_endpoint";
        $params->endpoint = $this->form->$endpoint_field;
        $username_field = $this->form->env . "_username";
        $params->username = $this->form->$username_field;
        $password_field = $this->form->env . "_password";
        $params->password = $this->form->$password_field;
        $this->redirect("tbl_filenet_query.php?params=" . base64_encode(serialize($params)));
        
    }

//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
        