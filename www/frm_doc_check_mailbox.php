<?php

include "doc_check.class.php";

//*****************************************************************************
class page extends doc_check {

    //**************************************************************************
    function __construct() {
        
        $this->page_title = "Daily document submission check - mailbox";
        parent::__construct();
    }
    
    //***************************************************************************
    function createForm() {

        $this->form = $this->getForm();

        $this->form->openTab("main", "Main");
            $this->form->addText("dest_db", "Destination database", true)->value = $this->user->env;
        $this->form->closeTab();
        $this->form->openTab("mailbox", "Mailbox");
            $this->form->addText("mailbox_host", "Mailbox", false, true)->value = $this->user->doc_check_mailbox_host;
            $this->form->addText("mailbox_username", "Username", false, true)->value = $this->user->doc_check_mailbox_username;
            $this->form->addPassword("mailbox_password", "Password", false, true)->value = $this->user->doc_check_mailbox_password;
        $this->form->closeTab();

        $this->form_submitButtons($this->form, false, false, "Check");
        $this->form->getInputValues();
    }

    //***************************************************************************
    function check() {

        $this->checkMandatory($this->form);
        
        $messages = [];
        while($reportName = $this->getNextReport()) {
        
            list ($date, $time, $originalReportName) = explode(".", basename($reportName), 3);
            $messages[] = "<hr>Elaborazione file $originalReportName ($date $time)<br/><br/>";
            $lapMessages = [];
            $rows = $this->getXLSXRows($reportName);
            $rowNr = 0;
            $barcodeColNr = 0;
            $chunkLimit = 100;
            $chunk = [];
            foreach ($rows as $row) {
                $rowNr++;
                if ($rowNr == 1) {
                    $barcodeColNr = $this->getBarcodeColNr($row);
                    continue;
                }

                if (count($chunk) == $chunkLimit) {
                    $lapMessages = array_merge($lapMessages, $this->checkChunk($barcodeColNr, $chunk));
                    $chunk = [];
                }

                $row["my_row_nr"] = $rowNr;
                $chunk[] = $row;
            }
            if (count($chunk)) {
                $lapMessages = array_merge($lapMessages, $this->checkChunk($barcodeColNr, $chunk));
                $chunk = [];
            }

            if ($lapMessages) {
                $messages = array_merge($messages, $lapMessages);
            } else {
                $messages[] = "Nessun errore rilevato; processate " . ($rowNr - 1) . " righe";
            }
            
            unlink($reportName);
        }

        if ($messages) {
            $this->showMessage("Esito rilevamento mailbox", implode("<br/>", $messages), false, true);
        } else {
            $this->showMessage("Nessun report rilevato", "Nessun report rilevato in mailbox", false, true);
        }

    }

    //***************************************************************************
    function getNextReport() {
        
        $inbox = imap_open($this->form->mailbox_host, $this->form->mailbox_username, $this->form->mailbox_password);
        $mailboxes = imap_list($inbox, $this->form->mailbox_host, '*');
        $archive = $this->getArchiveMailboxName($mailboxes);
        $trash = $this->getTrashMailboxName($mailboxes);
        
        while (imap_num_msg($inbox)) {
            if ($this->isSubjectValid($inbox)) {
                $body = imap_fetchstructure($inbox, 1);
                $partsNr = count($body->parts);
                if ($partsNr > 1) {
                    for ($partIdx = 0; $partIdx < $partsNr; $partIdx++) {
                        if (strtolower($body->parts[$partIdx]->disposition) == 'attachment') {
                            $filename = $this->getFilename($body->parts[$partIdx]);
                            $extension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
                            if ($extension == "xlsx") {
                                $timestamp = $this->getTimestamp($body->parts[$partIdx]);
                                $filename = APPL_TMP_DIRECTORY . "/$timestamp.$filename";
                                file_put_contents($filename, imap_base64(imap_fetchbody($inbox, 1, $partIdx + 1)));
        			imap_mail_move($inbox, 1, $archive);
                                imap_expunge($inbox);
                                imap_close($inbox);
                                return $filename;
                            }
                        }
                    }
                }
            }

            imap_mail_move($inbox, 1, $trash);
            imap_expunge($inbox);
        }

        imap_close($inbox);
    }

    //***************************************************************************
    function isSubjectValid($inbox) {
    
        static $patterns;
        
        if (!$patterns) {
            $patterns = explode(";", APPL_MAILBOX_Q_VALID_SUBJECT_PATTERNS);
        }
        
        $subject = strtolower(imap_fetch_overview($inbox, 1, 0)[0]->subject);
        foreach ($patterns as $pattern) {
            if (stripos($subject, $pattern) !== false) {
                return $subject;
            }
        }
        
        return false;
    }
    
    //***************************************************************************
    function getFilename($part) {
    
        foreach ($part->dparameters as $dparameter) {
            if (strtolower($dparameter->attribute) == "filename") {
                return $dparameter->value;
            }
        }
    }
    
    //***************************************************************************
    function getTimestamp($part) {
    
        foreach ($part->dparameters as $dparameter) {
            if (strtolower($dparameter->attribute) == "creation-date") {
                return date("Ymd.His", strtotime($dparameter->value));
            }
        }
    }
    
    //***************************************************************************
    function getArchiveMailboxName($mailboxes) {
    
        foreach ($mailboxes as $mailbox) {
            if (stripos($mailbox, ".Archiv") !== false) {
                return substr($mailbox, strlen($this->form->mailbox_host));
            }
        }
    }
    
    //***************************************************************************
    function getTrashMailboxName($mailboxes) {
    
        foreach ($mailboxes as $mailbox) {
            if (stripos($mailbox, ".Trash") !== false) {
                return substr($mailbox, strlen($this->form->mailbox_host));
            }
            if (stripos($mailbox, ".Cestino") !== false) {
                return substr($mailbox, strlen($this->form->mailbox_host));
            }
        }
    }
    
//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
        