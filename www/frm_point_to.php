<?php

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	var $field_name;

	//**************************************************************************
	function __construct() {
		parent::__construct();
		$this->field_name = $_GET["field_name"];

		$this->createForm();

		if ($this->form->isToUpdate()) {
			$this->updateRecord();
		} elseif ($this->form->isToDelete()) {
			$this->myDeleteRecord();
		} else {
			$this->showPage();
		}
	}

	//*****************************************************************************

	/**
	 * mostra
	 * 
	 * costruisce la pagina contenente il form e la manda in output
	 * @return void
	 */
	function showPage() {
		$this->addItem("$this->field_name point to...", "title");
		$this->addItem($this->form);
		$this->show();
	}

	//***************************************************************************
	function createForm() {
		$this->form = $this->getForm();
		$readonly = false;

		$ctrl = $this->form->addSelectTypeahead("dest_table", "Destination table", $readonly, !$readonly);
		$selected = null;
		foreach ($this->sessionData["tables"] as $idx => $table) {
			$ctrl->list[$idx] = $table["table_name"];
			$selected = $table["table_name"] == $this->sessionData["point_tos"][$this->field_name]["dest_table"] ? $idx : $selected;
		}
		$ctrl->value = $selected;

		$ctrl = $this->form->addSelect("dest_field", "Destination field", $readOnly, !$readonly);
		$ctrl->list = $this->rpc_getDestFields($selected);
		$ctrl->value = $this->sessionData["point_tos"][$this->field_name]["dest_field"];

		$this->form_submitButtons($this->form, $readonly, $this->sessionData["point_tos"][$this->field_name] ? !$readonly : false);
		$this->form->getInputValues();
	}

	//***************************************************************************
	function updateRecord() {
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);

		if (!$this->sessionData["tables"][$this->form->dest_table]) {
			$this->showMandatoryError();
		}
		
		$dest_table = $this->sessionData["tables"][$this->form->dest_table]["table_name"];
		$this->sessionData["point_tos"][$this->field_name]["dest_table"] = $dest_table;
		$this->sessionData["point_tos"][$this->field_name]["dest_field"] = $this->form->dest_field;

		file_put_contents("$this->directoryDoc/tables.cache/point_tos", serialize($this->sessionData["point_tos"]));

		$this->response();
	}

	//***************************************************************************
	function myDeleteRecord() {

		unset($this->sessionData["point_tos"][$this->field_name]);
		file_put_contents("$this->directoryDoc/tables.cache/point_tos", serialize($this->sessionData["point_tos"]));

		$this->response();
	}

	//*****************************************************************************

	/**
	 * @param type $id_question
	 * @return array
	 */
	function rpc_getDestFields($table_idx) {

		$retval = [];
		if ($this->sessionData["tables"][$table_idx]) {
			$dest_table = $this->sessionData["tables"][$table_idx]["table_name"];
			$sql = "SELECT COLUMN_NAME FROM ALL_TAB_COLUMNS WHERE TABLE_NAME = '$dest_table' ORDER BY COLUMN_NAME";
			foreach ($this->getRecordset($sql)->records as $record) {
				$retval[$record->COLUMN_NAME] = $record->COLUMN_NAME;
			}
		}

		return $retval;
	}

	//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
