//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe watextarea_ext
var waTextArea_ext = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waTextArea,

	//-------------------------------------------------------------------------
	// proprieta'
	tipo: 'textarea_ext',
	
	//-------------------------------------------------------------------------
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		
		var proxy = this;
		if (tinyMCE.initialized != true)
			{
			tinyMCE.init(
							{
							height : "200",
							convert_urls : false,
							relative_urls : false,
							document_base_url : "./",
							forced_root_block : false,
							force_br_newlines : true,
							force_p_newlines : false,
							selector : ".mceEditor",
							paste_data_images: true,
							plugins : "fullscreen, link, image, paste, textcolor, emoticons, table, code, media, template, hr",
							menu :	
								{ 
								edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
								insert : {title : 'Insert', items : 'link image media | hr'},
								format : {title : 'Format', items : 'bold italic underline strikethrough superscript subscript | formats removeformat'},
								table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'},
								tools  : {title : 'Tools' , items : 'fullscreen code'}
								},

							toolbar: "fontselect fontsizeselect bullist numlist outdent indent forecolor backcolor emoticons fullscreen",

							readonly : false,
							statusbar : false,
							setup: function(editor) 
								{
								editor.on("Init", function(editor) {proxy.setSelector();});
								}

							}			
						);
			tinyMCE.initialized = true;
			}
			
		},
				
	//-------------------------------------------------------------------------
	setSelector: function() 
		{
		this.selector = '#' + jQuery('#' + this.form.name + '_' + this.name).prev().prop("id");
		},
				
	//-------------------------------------------------------------------------
	/**
	* restituisce il  valore applicativo contenuto nel controllo
	*/
	get: function() 
		{
		tinyMCE.get(this.form.name + "_" + this.name).save();
		return this.obj.value;
		}
		

	}
);



