//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe watextarea_ext
var waGeolocalize = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: waControl,

	//-------------------------------------------------------------------------
	// proprieta'
	tipo: 'geolocalize',
	
	blob:	null,
	street:			null,
	house_number:	null,
	postal_code:	null,
	city:			null,
	province_short:	null,
	region_short:	null,
	country_short:	null,
	notes:			null,
	
	components:	null,
	
	//-------------------------------------------------------------------------
	initialize: function(form, name, value, visible, readOnly, mandatory) 
		{
		// definizione iniziale delle proprieta'
		this.parent(form, name, value, visible, readOnly, mandatory);
		
		this.selector = '#' + this.form.name + '_' + this.name + "_geolocalize_container";
		
		this.blob = this.form.obj.elements[this.name + "_blob"];
		this.street = this.form.obj.elements[this.name + "_street"];
		this.house_number = this.form.obj.elements[this.name + "_house_number"];
		this.postal_code = this.form.obj.elements[this.name + "_postal_code"];
		this.city = this.form.obj.elements[this.name + "_city"];
		this.province_short = this.form.obj.elements[this.name + "_province_short"];
		this.region_short = this.form.obj.elements[this.name + "_region_short"];
		this.country_short = this.form.obj.elements[this.name + "_country_short"];
		this.notes = this.form.obj.elements[this.name + "_notes"];
		
		if (this.blob.value) 
			{
			try 
				{
				this.normalizeAddress();
				}
			catch (e) {}
			}
		
		var types = "geocode";
		if (eval(this.form.name + "_" + this.name + "_city_only")) {
			types = "(cities)";
		}
		var options =	{types: [types]};
		var autocomplete = new google.maps.places.Autocomplete(this.obj, options);
	  
		// evento per cambio dell'autocomplete di google
		var proxy = this;
		google.maps.event.addListener
			(
			autocomplete, 
			'place_changed', 
			function() 
				{
				proxy.placeChanged(autocomplete.getPlace());
				}
			);
	
//		jQuery(this.house_number.id).on("change", function() {proxy.house_number_change()});
		jQuery("#" + this.form.name + "_" + this.name + "_street").on("change", function() {proxy.street_change()});
		jQuery("#" + this.form.name + "_" + this.name + "_house_number").on("change", function() {proxy.house_number_change()});
		jQuery("#" + this.form.name + "_" + this.name + "_notes").on("change", function() {proxy.notes_change()});
		jQuery("#" + this.form.name + "_" + this.name).on("change", function() {setTimeout(proxy.address_blur(), 20)});
		
		// trapping di invio per fare in modo che non vada alla riga successiva
		// del textarea
		this.obj.onkeypress = function (event) {proxy.trapCRLF(event);}
		},
				
	//-------------------------------------------------------------------------
	trapCRLF: function (event)
	    {
		event = event ? event : window.event;
		
		if (event.keyCode == 13)
			{
		    event.preventDefault();
			}
		},
		
	//-------------------------------------------------------------------------
	placeChanged: function(place)
		{
		// per default via civico e note sono disabilitati
		this.street.disabled = this.house_number.disabled = this.notes.disabled = true;
			
		if(!place)
			return;

		this.blob.value = JSON.stringify(place);
		this.blob2ctrls();
		
		// via civico e note sono abilitati solo se iol controllo è stato 
		// valorizzato
		this.street.disabled = this.house_number.disabled = this.notes.disabled = !this.obj.value;
		
		},
		
	//-------------------------------------------------------------------------
	address_blur: function()
		{
		if (!this.obj.value.trim())
			{
			this.reset();
			}
			
		},
		
	//-------------------------------------------------------------------------
	street_change: function()
		{
		this.specific_ctrl_change("route", "street");
		},
		
	//-------------------------------------------------------------------------
	house_number_change: function()
		{
		this.specific_ctrl_change("street_number", "house_number");
		},
		
	//-------------------------------------------------------------------------
	notes_change: function()
		{
		this.specific_ctrl_change("notes", "notes");
		},
		
	//-------------------------------------------------------------------------
	// gestion dell'evento di cambiamento di uno dei campi specifici (via, civico)
	specific_ctrl_change: function(google_name, ctrl_name)
		{
		if (!this.blob)
			return;
		
		this.components[ctrl_name] = this[ctrl_name].value;
		
		var address_info = JSON.parse(this.blob.value);

		var found = false;
		for (var i = 0; i < address_info.address_components.length; i++)
			{
			for (var li = 0; li < address_info.address_components[i].types.length; li++)
				{
				if (address_info.address_components[i].types[li] == google_name)
					{
					address_info.address_components[i].long_name = address_info.address_components[i].short_name = this.components[ctrl_name];
					found = true;
					break;
					}
				}
			if (found)
				break;
			}			
			
		if (!found)
			{
			var newElem = {long_name: this.components[ctrl_name], short_name: this.components[ctrl_name]};
			newElem.types = [google_name];
			address_info.address_components[address_info.address_components.length] = newElem;
			}
			
		this.blob.value = JSON.stringify(address_info);
		},
		
	//-------------------------------------------------------------------------
	/**
	* inserisce un valore applicativo nel controllo
	* 
	* @param {mixed} value valore da inserire nel controllo
	 * @memberof waControl
	*/
	set: function(value) 
		{
		this.obj.value = value.address;
		this.blob.value = value.blob;
		this.blob2ctrls();
		},
		
	//***************************************************************************
	/** 
	 * dati componenti dell'indirizzo tornati da googlemaps ritorna una
	 * struttura dati normalizzata (si spera)
	 * 
	 * @param array $address_components
	 * @return stdClass
	 */
		
	normalizeAddress: function() 
		{
		this.components = {};

		var pass = JSON.parse(this.blob.value).address_components;


		for (var i = 0; i < pass.length; i++)
			{
			for (var li = 0; li < pass[i].types.length; li++)
				{
				if (pass[i].types[li] == "street_number")
					{
					this.components.house_number = pass[i].long_name;
					break;
					}
				if (pass[i].types[li] == "route")
					{
					this.components.street = pass[i].long_name;
					break;
					}
				if (pass[i].types[li] == "locality")
					{
					this.components.locality = pass[i].long_name;
					break;
					}
				if (pass[i].types[li] == "postal_town")
					{
					this.components.postal_town = pass[i].long_name;
					break;
					}
				if (pass[i].types[li] == "administrative_area_level_3" && !this.components.city)
					{
					this.components.city = pass[i].long_name;
					break;
					}
				if (pass[i].types[li] == "administrative_area_level_2")
					{
					this.components.province = pass[i].long_name;
					this.components.province_short = pass[i].short_name;
					break;
					}
				if (pass[i].types[li] == "administrative_area_level_1")
					{
					this.components.region = pass[i].long_name;
					this.components.region_short = pass[i].short_name;
					break;
					}
				if (pass[i].types[li] == "country")
					{
					this.components.country = pass[i].long_name;
					this.components.country_short = pass[i].short_name;
					break;
					}
				if (pass[i].types[li] == "postal_code")
					{
					this.components.postal_code = pass[i].long_name;
					break;
					}
				}
			}


		// tenatitvi di determinare city (cambia a seconda della nazione)
		if (!this.components.city)
			{
			this.components.city = this.components.postal_town;
			}
		if (!this.components.city)
			{
			this.components.city = this.components.locality;
			}
		if (!this.components.city)
			{
			this.components.city = this.components.province;
			}
			
		// certe città straniere non hanno l'equivalente della provincia
		if (!this.components.province)
			{
			this.components.province = "--";
			this.components.province_short = "--";
			}
			
		},
		
	//-------------------------------------------------------------------------
	/**
	*/
	blob2ctrls: function() 
		{
		if (this.blob.value) 
			{
			try 
				{
				this.normalizeAddress();
				this.street.value = this.components.street ? this.components.street : "";
				this.house_number.value = this.components.house_number ? this.components.house_number : "";
				this.postal_code.value = this.components.postal_code ? this.components.postal_code : "";
				this.city.value = this.components.city ? this.components.city : "";
				this.province_short.value = this.components.province_short ? this.components.province_short : "";
				this.region_short.value = this.components.region_short ? this.components.region_short : "";
				this.country_short.value = this.components.country_short ? this.components.country_short : "";
				this.notes.value = this.components.notes ? this.components.notes : "";
				}
			catch (e) 
				{
				this.reset();
				}
			}
		else
			{
			this.reset();
			}
		},
		
	//-------------------------------------------------------------------------
	/**
	*/
	reset: function() 
		{
		this.components = null;
		this.obj.value = "";
		this.blob.value = "";
		this.street.value = "";
		this.house_number.value = "";
		this.postal_code.value = "";
		this.city.value = "";
		this.province_short.value = "";
		this.region_short.value = "";
		this.country_short.value = "";
		this.notes.value = "";

		// per default via civico e note sono disabilitati
		this.street.disabled = this.house_number.disabled = this.notes.disabled = true;
		},
		
	//-------------------------------------------------------------------------
	/**
	* restituisce il  valore applicativo contenuto nel controllo
	 * @memberof waControl
	*/
	get: function() 
		{
		if (!this.obj.value || !this.blob.value)
			return null;
		
		return {address: this.obj.value, blob: this.blob.value};
		},
		
	//-------------------------------------------------------------------------
	/**
	* a seconda dello stato /definisce la classe css di un controllo (visible, 
	* readOnly, mandatory) 
	 * @memberof waControl
	*/
	render: function() 
		{
		this.parent();
		this.street.disabled = this.house_number.disabled = this.notes.disabled = this.readOnly;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * verifica che un controllo sia valorizzato correttamente
	 * @return (boolean)
	 * @memberof waControl
	 */
	formatVerify: function() 
		{
		if (this.obj.value && !this.blob.value)
			{
			return false;
			}
			
		if (!this.obj.value && this.blob.value)
			{
			this.reset();
			}
			
		return true;
		},
		
	//-------------------------------------------------------------------------
	/**
	 * verifica se un controllo fisico appartiene al controllo applicativo
	* 
	* @param {object} obj oggetto input HTML 
	 * @return (boolean)
	 * @memberof waControl
	 */
	isMine: function (obj)
		{
		return obj.name == this.obj.name ||
				obj.name == this.street.name ||
				obj.name == this.house_number.name ||
				obj.name == this.postal_code.name ||
				obj.name == this.city.name ||
				obj.name == this.province_short.name ||
				obj.name == this.region_short.name ||
				obj.name == this.country_short.name ||
				obj.name == this.notes.name ||
				obj.name == this.blob.name;

		}
	
	}
);



