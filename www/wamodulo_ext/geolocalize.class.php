<?php
namespace waLibs;

//***************************************************************************
//****  classe waTextArea_ext *******************************************************
//***************************************************************************
/**
* waGeolocalize
*
* classe per la gestione di un controllo geolocalize.
*
*/
class waGeolocalize extends waControl
	{
	public $address;
	public $blob;
	
	/**
	* @ignore
	* @access protected
	*/
	var $type			= 'geolocalize';
	
	var $city_only		= false;
	var $country_only	= false;
	
	
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	* @ignore
	*/
	function get()
		{
		
		$value = new \stdClass();
		$address_field_name = $this->name;
		$blob_field_name = $this->name . "_blob";
		$value->address = $this->form->record->$address_field_name;
		$value->blob = $this->form->record->$blob_field_name;
		$value->city_only = $this->city_only;
		$value->country_only = $this->country_only;

		$this->value = json_encode($value);
		return parent::get();
		}
		
	//***************************************************************************
	/**
	 * salva sul campo del record il valore di input
	* @ignore
	*/	
	function input2record()
		{
		if (!$this->dbBound || !$this->form->record || $this->inputValue === null)
			{
			return;
			}
		$this->form->record->insertValue($this->name, $this->inputValue->address);
		$this->form->record->insertValue($this->name . "_blob", $this->inputValue->blob);
		}
	
	
	}	// fine classe waGeolocalize
//***************************************************************************
//******* fine della gnola **************************************************
//***************************************************************************



