<?php

//******************************************************************************
include "uqueryall.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends uqueryall {

    //*****************************************************************************
    function __construct() {
        parent::__construct();

        $response = new stdClass();
        $response->result = "";
        try {
            if ($this->sessionData["tablenames"][$_GET["tbl_name"]]) {
                $dbconn = $this->getDBConnection();
                if ($_GET["tbl_name"] == "CC_USER") {
                    $sql = "SELECT" .
                            " CC_CREDENTIAL.USERNAME," .
                            " CC_CONTACT.LASTNAME," .
                            " CC_CONTACT.FIRSTNAME" .
                            " FROM CCUSER.CC_USER" .
                            " INNER JOIN CCUSER.CC_CONTACT ON CC_USER.CONTACTID=CC_CONTACT.ID" .
                            " INNER JOIN CCUSER.CC_CREDENTIAL ON CC_USER.CREDENTIALID=CC_CREDENTIAL.ID";
                } elseif ($_GET["tbl_name"] == "CC_CONTACT") {
                    $sql = "SELECT" .
                            " CC_CONTACT.LASTNAME," .
                            " CC_CONTACT.FIRSTNAME" .
                            " FROM CCUSER.CC_CONTACT";
                } else {
                    $sql = "select * from CCUSER." . $_GET["tbl_name"];
                }
                $sql .= " WHERE $_GET[tbl_name].$_GET[id_name]=" . $dbconn->sqlString($_GET["id_value"]);

                $record = $this->getRecordset($sql, $dbconn, 1)->records[0];
                $this->addRow($record, "typecode", $response);
                $this->addRow($record, "code", $response);
                $this->addRow($record, "name", $response);
                $this->addRow($record, "description", $response);
                $this->addRow($record, "username", $response);
                $this->addRow($record, "firstname", $response);
                $this->addRow($record, "lastname", $response);
                $this->addRow($record, "claimnumber", $response);
            }
        } catch (Exception $ex) {
            
        }

        header("Access-Control-Allow-Origin: *");
        if ($_SERVER["REMOTE_ADDR"] == "127.0.0.1") {
            header("Content-Type: text/plain; charset=utf-8");
        } else {
            header("Content-Type: application/json; charset=utf-8");
        }

        exit(json_encode($response, defined("JSON_PRETTY_PRINT") ? JSON_PRETTY_PRINT : 0));
    }

    //*****************************************************************************
    function addRow(\waLibs\waRecord $record, $fieldName, &$response) {

        $ret = $response->result;
        if ($record->value($fieldName)) {
            $response->result .= ($response->result ? "<br/>" : "") . "$fieldName: " . $record->value($fieldName);
        }

        return $ret;
    }

    //*****************************************************************************
}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();
