<?php

include "doc_check.class.php";

//*****************************************************************************
class page extends doc_check {

    //**************************************************************************
    function __construct() {
        
        $this->page_title = "Kofax FAX check";
        parent::__construct();
    }
    
    //***************************************************************************
    function createForm() {

        $this->form = $this->getForm();

        $this->form->openTab("main", "Main");
            $this->form->addText("dest_db", "Destination database", true)->value = $this->user->env;
            $this->form->addInteger("depth_days", "Depth days")->value = 1;
        $this->form->closeTab();
        $this->form->openTab("kofax", "Share");
            $this->form->addText("kofax_host", "Mailbox", false, true)->value = $this->user->doc_check_kofax_host;
            $this->form->addText("kofax_username", "Username", false, true)->value = $this->user->doc_check_kofax_username;
            $this->form->addPassword("kofax_password", "Password", false, true)->value = $this->user->doc_check_kofax_password;
        $this->form->closeTab();

        $this->form_submitButtons($this->form, false, false, "Check");
        $this->form->getInputValues();
    }

    //***************************************************************************
    function check() {

        $this->checkMandatory($this->form);
        
        $reportList[] = "info_SX_DISPATCHER.log";
        for ($i = 1; $i < $this->form->depth_days; $i++) {
            $date = date("Y-m-d", mktime(0,0,0, date("n"), date("j") - $i, date("Y")));
            $reportList[] = "info_SX_DISPATCHER.log$date";
        }
        
        list($host, $port) = explode(":", $this->form->kofax_host);
        $username = str_replace("\\", "/", $this->form->kofax_username);
        // smbclient mget si pianta al primo errore, quindi se un file non esiste non 
        // vengono scaricati i restanti; usiamo get in ciclo
        // inoltre non c'è modo di inibire uno strano comportamento dello stderr, 
        // quindi dobbiamo usare ob per sopprimere output indesiderato
        ob_start();
        foreach ($reportList as $reportName) {
            $command = "smbclient" .
                        " //$host/capturesv" .
                        " -D /Unipol/SX/LOGS" .
                        ($port ? " -p $port" : "") .
                        " -U $username%'" . $this->form->kofax_password . "'" .
                        " -c 'lcd $this->directoryTmp;prompt;get $reportName'";
            $out = system($command, $result_code);
            if ($result_code != 0) {
                if (stripos($out, "NO_SUCH_FILE") || stripos($out, "NOT_FOUND")) {
                    continue;
                }
                else {
                    $this->showMessage("$this->page_title - Errore", "Errore durante la lettura dei file di log: $out", false, true);
                }
            }

        }
        ob_clean();
        
        $messages = [];
        foreach ($reportList as $reportName) {
            $file_name = "$this->directoryTmp/$reportName";
            if (!file_exists($file_name)) {
                $messages[] = "<hr>file $reportName non esistente<br/><br/>";
                continue;
            }           
        
            $messages[] = "<hr>Elaborazione file $reportName<br/><br/>";
            $lapMessages = [];
            $rowNr = 0;
            $chunkLimit = 100;
            $chunk = [];
            $fp = fopen($file_name, "rb");
            while ($row = fgets($fp)) {
                if (count($chunk) == $chunkLimit) {
                    $lapMessages = array_merge($lapMessages, $this->checkChunk(0, $chunk));
                    $chunk = [];
                }
                list ($rest, $batch_id) = explode("Open Module:Server  - Actual Batch_ID:", $row, 2);
                if ($batch_id) {
                    $rowNr++;
                    $barcode = "FAX-" . trim($batch_id) .  "-1";
                    $chunk[] = [$barcode];
                }
            }
            if (count($chunk)) {
                $lapMessages = array_merge($lapMessages, $this->checkChunk(0, $chunk));
            }
            if ($lapMessages) {
                $messages = array_merge($messages, $lapMessages);
            } else {
                $messages[] = "Nessun errore rilevato; processate $rowNr righe";
            }
            fclose($fp);
            unlink($file_name);
        }

        if ($messages) {
            $this->showMessage("$this->page_title - Esito rilevamento", implode("<br/>", $messages), false, true);
        } else {
            $this->showMessage("$this->page_title - Nessun report rilevato", "Nessun report rilevato", false, true);
        }

    }

//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
        