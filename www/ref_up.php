<?php

ini_set('max_execution_time', '600');

include "uqueryall.inc.php";

//*****************************************************************************
class page extends uqueryall {

    var $printed = [];

    //**************************************************************************
    function __construct() {
        parent::__construct();

        echo "<pre>";
        $table_name = $_GET["tbl_name"];
        $this->showRef($table_name);
        echo "</pre>";
        exit();
    }

    //*****************************************************************************

    /**
     * mostra
     * 
     * costruisce la pagina contenente il form e la manda in output
     * @return void
     */
    function showRef($table_name, $level = 0) {

        echo "\n" . str_repeat("\t", $level) . $table_name;
        if ($this->printed[$table_name]) {
            echo " *";
            return;
        }
        $this->printed[$table_name] = 1;
        if ($this->sessionData["constraints"]["parents"][$table_name]) {
            $constraints = &$this->sessionData["constraints"]["parents"][$table_name];
            usort($constraints, function ($a, $b) {
                return strcmp(str_replace("_", "A", $a->parent), str_replace("_", "A", $b->parent));
            });
            foreach ($constraints as $constraint) {
                $this->showRef($constraint->parent, $level + 1);
            }
        }
    }

    //*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
