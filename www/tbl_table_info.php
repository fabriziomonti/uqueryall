<?php
//******************************************************************************
include "uqueryall.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends uqueryall
	{
	var $tbl_name;
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();
		$this->tbl_name = $_GET["tbl_name"];

		$this->addItem($this->getMenu());
		$title = "Columns";
		if ($_GET["indexes"]) {
			$title = "Indexes";
		}
		elseif ($_GET["constraints"]) {
			$title = "Constraints";
		}
		elseif ($_GET["foreignkeys"]) {
			$title = "Foreign keys";
		}
		$this->addItem("table $this->tbl_name - $title", "title");
		$this->addItem($this->myGetForm());
		$this->addItem($this->myGetTable());
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function myGetTable()
		{
		// creazione della tabella
		$dbconn = $this->getDBConnection();
                $schema = $this->getCurrentSchema();
		$sql = "SELECT ROWNUM AS ID, ALL_TAB_COLUMNS.* FROM ALL_TAB_COLUMNS WHERE OWNER='$schema' AND TABLE_NAME = '$this->tbl_name' ORDER BY COLUMN_NAME";
		if ($_GET["indexes"]) {
			$sql = "SELECT * FROM ALL_INDEXES WHERE OWNER='$schema' AND TABLE_NAME = '$this->tbl_name'";
		}
		elseif ($_GET["constraints"]) {
			$sql = "SELECT * FROM ALL_CONSTRAINTS WHERE OWNER='$schema' AND TABLE_NAME = '$this->tbl_name'";
		}
		elseif ($_GET["foreignkeys"]) {
                    $schema = $this->getCurrentSchema();
			$sql = "SELECT a.column_name, c_pk.table_name" .
						" FROM all_cons_columns a" .
						" JOIN all_constraints c ON a.owner = c.owner AND a.constraint_name = c.constraint_name" .
						" JOIN all_constraints c_pk ON c.r_owner = c_pk.owner AND c.r_constraint_name = c_pk.constraint_name" .
					    " WHERE a.owner='$schema' " .
					    " AND c.constraint_type = 'R'" .
					    " AND a.table_name = '$this->tbl_name'" .
						" ORDER BY a.column_name";
		}
		
		$table = parent::getTable($sql);
		$table->listMaxRec = 0;
		$table->removeAction("New");
		$table->removeAction("Edit");
		$table->removeAction("Delete");
		$table->removeAction("All");
		$table->addAction("Data");
		if ($_GET["indexes"]) {
			$table->addAction("Columns");
			$table->addAction("Constraints");
			$table->addAction("Foreign_keys", false, "Foreign keys");
		}
		elseif ($_GET["constraints"]) {
			$table->addAction("Columns");
			$table->addAction("Indexes");
			$table->addAction("Foreign_keys", false, "Foreign keys");
		}
		elseif ($_GET["foreignkeys"]) {
			$table->addAction("Columns");
			$table->addAction("Constraints");
			$table->addAction("Indexes");
		}
		else {
			$table->addAction("Constraints");
			$table->addAction("Indexes");
			$table->addAction("Foreign_keys", false, "Foreign keys");
			$table->addAction("Point_to", true, "Point to...");
		}
		
		// colonne
		 $this->setTableColumns($table, $dbconn, $sql);
		
		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$table->loadRows()) {
			$this->showDBError($table->recordset->dbConnection);
		}

		return $table;
		}

	//*****************************************************************************

	/**
	 * @return waLibs\waForm
	 */
	function myGetForm() {
		
		$form = parent::getForm();
		
		$ctrl = $form->addSelectTypeahead("typeahead_table_name", "Table quick search", false);
		$ctrl->list = $this->sessionData["tablenames"];
		
		$form->getInputValues();
		if ($form->isToUpdate()) {
			$this->redirect("tbl_crud.php?tbl_name=" . $form->typeahead_table_name);
		}
		
		return $form;
		
		
	}

	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();