<?php

include "doc_check.class.php";

//*****************************************************************************
class page extends doc_check {


    //**************************************************************************
    function __construct() {
        
        $this->page_title = "Check FDMPosta EcmLoader results";
        $this->main_dir = "/InboundFlows/Filenet/Smistadoc/FDM/Posta";
        $this->log_name = "infoFDMPosta.log";
        
        parent::__construct();
    }
    
//*****************************************************************************
}

//*****************************************************************************
// istanzia la pagina
new page();
